package com.ezlcp.form.web.file;

import com.ezlcp.commons.model.FileModel;
import com.ezlcp.form.entity.File;
import jakarta.servlet.http.HttpServletResponse;


/**
 * @author Elwin ZHANG
 * @description: 文件操作接口<br />
 * @date 2022/5/16 10:14
 */
public interface IFileOperator {
    String getTitle();

    String getType();

    /**
     * 下载文件
     *
     * @param response   Http响应
     * @param sysFile    系统文件对象
     * @param isScale    是否缩放
     * @param isDownload 是否下载
     * @return
     */
    void downFile(HttpServletResponse response, File sysFile, boolean isScale, boolean isDownload);

    /**
     * 创建文件，传入扩展名和文件内容写入文件。
     *
     * @param bytes
     * @return
     */
    FileModel createFile(String fileName, byte[] bytes);

    /**
     * 删除文件。
     *
     * @param path
     * @return
     */
    int delFile(String path);

}