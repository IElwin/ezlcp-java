package com.ezlcp.form.web.file;

import com.ezlcp.form.entity.File;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Elwin ZHANG
 * @description: <br/>
 * @date 2022/5/16 9:57
 */
@Component
public class SysFileUtil {

    @Getter
    private static String baseUploadPath;

    @Value("${upload.path}")
    public void setBaseUploadPath(String path) {
        baseUploadPath = path;
    }
    /**
     * 替换扩展名
     * @param fileName
     * @param ext
     * @return
     */
    public static String replaceExt(String fileName,String ext){
        fileName =fileName.substring(0,fileName.lastIndexOf(".")) + "."+ ext;
        return  fileName;
    }

    public static String getDownloadName(File sysFile){
        String fileName=sysFile.getFileName();

        return  fileName;
    }

    /***
    * @description: 获取配置的上传目录根目录
    * @return java.lang.String
    * @author Elwin ZHANG
    * @date 2022/5/16 10:08
    */
    public static String getUploadPath(){
        return getBaseUploadPath();
    }

    /**
     * 获取文件路径。
     * @param sysFile 文件
     * @param isScale 是否缩放
     */
    public static String getFilePath(File sysFile, boolean isScale) throws Exception {
        String uploadPath=getUploadPath();
        String fullPath="";

        if (isScale){
            fullPath =  sysFile.getThumbnail();
        }else {
            fullPath = sysFile.getPath();
        }

        fullPath=uploadPath + fullPath;

        return fullPath;
    }


}
