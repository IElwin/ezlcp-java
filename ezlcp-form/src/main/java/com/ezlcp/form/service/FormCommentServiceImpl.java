
package com.ezlcp.form.service;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.service.impl.SuperServiceImpl;
import com.ezlcp.form.entity.FormComment;
import com.ezlcp.form.mapper.FormCommentMapper;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;

/**
 * [表单评论]业务服务类
 */
@Service
public class FormCommentServiceImpl extends SuperServiceImpl<FormCommentMapper, FormComment> implements BaseService<FormComment> {
    @Resource
    private FormCommentMapper formCommentMapper;

    @Override
    public BaseDao<FormComment> getRepository() {
        return formCommentMapper;
    }
}