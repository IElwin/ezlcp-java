package com.ezlcp.form.service;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.constant.Constants;
import com.ezlcp.commons.constant.StatusEnum;
import com.ezlcp.commons.service.impl.SuperServiceImpl;
import com.ezlcp.commons.tool.StringUtils;
import com.ezlcp.commons.utils.ContextUtil;
import com.ezlcp.form.entity.DicValue;
import com.ezlcp.form.mapper.DicValueMapper;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * [字典值]业务服务类
 */
@Service
public class DicValueServiceImpl extends SuperServiceImpl<DicValueMapper, DicValue> implements BaseService<DicValue> {
    private static final String COL_DIC_ID = "dic_id";
    private static final String COL_DIC_KEY = "dic_key";
    private static final String COL_IS_FIXED = "is_fixed";
    @Resource
    LogService logService;
    @Autowired
    private DicValueMapper dicValueMapper;

    /***
     * @description: 判断用户权限以及使用条件查询字典值列表
     * @param dicId 传入字典值id
     * @return java.util.List<com.ezlcp.user.entity.DicValue> 符合要求字典值列表
     * @author Weixuan LONG
     * @date 2022/5/11 18:01
     */
    public List<DicValue> getByDicId(String dicId) {
        QueryWrapper<DicValue> wrapper = new QueryWrapper<>();
        wrapper.eq(COL_DIC_ID, dicId);
        wrapper.lt(Constants.COL_STATUS, StatusEnum.deleted.getValue());
        String tenantId = ContextUtil.getCurrentTenantId();
        if (StringUtils.isNotEmpty(tenantId)) {
            wrapper.and(query -> query.eq(COL_IS_FIXED, 1).or().eq(Constants.COL_TENANT_ID, tenantId));
        } else {
            wrapper.isNull(Constants.COL_TENANT_ID);
        }
        wrapper.lambda().orderByAsc(DicValue::getSortNo);
        return dicValueMapper.selectList(wrapper);
    }

    /***
     * @description: 判断用户权限以及使用条件查询字典值列表
     * @param dicKey 传入字典值key
     * @return java.util.List<com.ezlcp.user.entity.DicValue> 符合要求字典值列表
     * @author Weixuan LONG
     * @date 2022/5/12 09:40
     */
    public List<DicValue> getByDicKey(String dicKey) {
        QueryWrapper<DicValue> wrapper = new QueryWrapper<>();
        wrapper.eq(COL_DIC_KEY, dicKey);
        wrapper.lt(Constants.COL_STATUS, StatusEnum.deleted.getValue());
        String tenantId = ContextUtil.getCurrentTenantId();
        if (StringUtils.isNotEmpty(tenantId)) {
            wrapper.and(query -> query.eq(COL_IS_FIXED, 1).or().eq(Constants.COL_TENANT_ID, tenantId));
        } else {
            wrapper.isNull(Constants.COL_TENANT_ID);
        }
        return this.findAll(wrapper);
    }

    /***
     * @description 根据词典Key和显示文字获取词典值
     * @param dickey key
     * @param showText 显示文字（默认为繁体中文）
     * @return com.ezlcp.user.entity.DicValue
     * @author Elwin ZHANG
     * @date 2022/8/15 14:44
     */
    public DicValue getByKeyAndText(String dickey, String showText) {
        QueryWrapper<DicValue> wrapper = new QueryWrapper<>();
        wrapper.eq(COL_DIC_KEY, dickey);
        wrapper.eq("show_text", showText);
        wrapper.lt(Constants.COL_STATUS, StatusEnum.deleted.getValue());
        String tenantId = ContextUtil.getCurrentTenantId();
        if (StringUtils.isNotEmpty(tenantId)) {
            wrapper.and(query -> query.eq(COL_IS_FIXED, 1).or().eq(Constants.COL_TENANT_ID, tenantId));
        } else {
            wrapper.isNull(Constants.COL_TENANT_ID);
        }
        var list = dicValueMapper.selectList(wrapper);
        if (list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    /***
     * @description: 新增或编辑字典值
     * @param dicValueList 字典值列表
     * @return java.lang.String
     * @author Weixuan LONG
     * @date 2022/5/30 9:22
     */
    @Transactional(rollbackFor = Exception.class)
    public String saveOrUpdateDicValue(List<DicValue> dicValueList) {
        String tenantId = ContextUtil.getCurrentTenantId();
        for (DicValue dicValue : dicValueList) {
            String valId = dicValue.getValId();
            String checkNullField = checkNullField(dicValue);
            if (checkNullField.length() > 0) {
                return checkNullField;
            }
            if (StringUtils.isEmpty(tenantId)) {
                return "common.insufficientPrivileges";
            }
            if (StringUtils.isNotEmpty(valId)) {
                // 编辑操作
                DicValue oldDicValue = this.getById(valId);
                if (ObjectUtil.isNull(oldDicValue.getTenantId())) {
                    continue;
                }
                if (!oldDicValue.getTenantId().equals(tenantId)) {
                    return "common.insufficientPrivileges";
                }
                dicValue.setSeq(oldDicValue.getSeq() + 1);
                dicValue.setIsFixed(Constants.SHORT0);
                this.updateById(dicValue);
                String detail = "old:" + JSON.toJSONString(oldDicValue) + "new:" + JSON.toJSONString(dicValue);
                logService.saveSystemLog("字典值", "updateDicValue", dicValue.getDicId(), detail);
            } else {
                // 新增操作
                dicValue.setTenantId(ContextUtil.getCurrentTenantId());
                dicValue.setEnText(StringUtils.defaultIfEmpty(dicValue.getEnText(), dicValue.getShowText()));
                dicValue.setHkText(StringUtils.defaultIfEmpty(dicValue.getHkText(), dicValue.getShowText()));
                dicValue.setIsFixed(Constants.SHORT0);
                dicValue.setStatus((short) StatusEnum.enable.getValue());
                this.insert(dicValue);
                logService.saveSystemLog("字典值", "addDicValue",
                        dicValue.getDicId(), JSON.toJSONString(dicValue));
            }
        }
        return "";
    }

    /***
     * @description: 判断传参是否为空
     * @param dicValue 传入字典值
     * @return java.lang.String 当字符串长度为0，则无误；否则有误
     * @author Weixuan LONG
     * @date 2022/5/11 17:49
     */
    public String checkNullField(DicValue dicValue) {
        String dicKey = dicValue.getDicKey();
        if (ObjectUtil.isEmpty(dicValue.getSortNo())) {
            return "dicValue.inputSort";
        }
        if (StringUtils.isBlank(dicKey)) {
            return "dicValue.inputDicKey";
        }
        if (StringUtils.isBlank(dicValue.getDicId())) {
            return "dicValue.dicId";
        }
        if (StringUtils.isBlank(dicValue.getShowText())) {
            return "dicValue.showText";
        }
        return "";
    }

    @Override
    public BaseDao<DicValue> getRepository() {
        return dicValueMapper;
    }
}