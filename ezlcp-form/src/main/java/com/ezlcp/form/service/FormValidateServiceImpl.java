
package com.ezlcp.form.service;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.service.impl.SuperServiceImpl;
import com.ezlcp.form.entity.FormValidate;
import com.ezlcp.form.mapper.FormValidateMapper;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;

/**
 * [表单的自定义校验]业务服务类
 */
@Service
public class FormValidateServiceImpl extends SuperServiceImpl<FormValidateMapper, FormValidate> implements BaseService<FormValidate> {
    @Resource
    private FormValidateMapper formValidateMapper;

    @Override
    public BaseDao<FormValidate> getRepository() {
        return formValidateMapper;
    }
}