
package com.ezlcp.form.service;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.service.impl.SuperServiceImpl;
import com.ezlcp.form.entity.FormButton;
import com.ezlcp.form.mapper.FormButtonMapper;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;

/**
 * [表单上的按钮]业务服务类
 */
@Service
public class FormButtonServiceImpl extends SuperServiceImpl<FormButtonMapper, FormButton> implements BaseService<FormButton> {
    @Resource
    private FormButtonMapper formButtonMapper;

    @Override
    public BaseDao<FormButton> getRepository() {
        return formButtonMapper;
    }
}