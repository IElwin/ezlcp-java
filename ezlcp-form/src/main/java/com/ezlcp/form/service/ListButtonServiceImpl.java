
package com.ezlcp.form.service;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.service.impl.SuperServiceImpl;
import com.ezlcp.form.entity.ListButton;
import com.ezlcp.form.mapper.ListButtonMapper;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;

/**
 * [数据列表上的按钮]业务服务类
 */
@Service
public class ListButtonServiceImpl extends SuperServiceImpl<ListButtonMapper, ListButton> implements BaseService<ListButton> {
    @Resource
    private ListButtonMapper listButtonMapper;

    @Override
    public BaseDao<ListButton> getRepository() {
        return listButtonMapper;
    }
}