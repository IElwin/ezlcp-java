
package com.ezlcp.form.service;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.service.impl.SuperServiceImpl;
import com.ezlcp.form.entity.ListCol;
import com.ezlcp.form.mapper.ListColMapper;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;

/**
 * [数据列表的列定义]业务服务类
 */
@Service
public class ListColServiceImpl extends SuperServiceImpl<ListColMapper, ListCol> implements BaseService<ListCol> {
    @Resource
    private ListColMapper listColMapper;

    @Override
    public BaseDao<ListCol> getRepository() {
        return listColMapper;
    }
}