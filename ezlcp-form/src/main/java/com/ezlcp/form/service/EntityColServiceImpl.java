
package com.ezlcp.form.service;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.service.impl.SuperServiceImpl;
import com.ezlcp.form.entity.EntityCol;
import com.ezlcp.form.mapper.EntityColMapper;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;

/**
 * [表的列定义]业务服务类
 */
@Service
public class EntityColServiceImpl extends SuperServiceImpl<EntityColMapper, EntityCol> implements BaseService<EntityCol> {
    @Resource
    private EntityColMapper entityColMapper;

    @Override
    public BaseDao<EntityCol> getRepository() {
        return entityColMapper;
    }
}