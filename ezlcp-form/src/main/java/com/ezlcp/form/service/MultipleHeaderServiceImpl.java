
package com.ezlcp.form.service;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.service.impl.SuperServiceImpl;
import com.ezlcp.form.entity.MultipleHeader;
import com.ezlcp.form.mapper.MultipleHeaderMapper;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;

/**
 * [列表或表单子表的多层表头配置]业务服务类
 */
@Service
public class MultipleHeaderServiceImpl extends SuperServiceImpl<MultipleHeaderMapper, MultipleHeader> implements BaseService<MultipleHeader> {
    @Resource
    private MultipleHeaderMapper multipleHeaderMapper;

    @Override
    public BaseDao<MultipleHeader> getRepository() {
        return multipleHeaderMapper;
    }
}