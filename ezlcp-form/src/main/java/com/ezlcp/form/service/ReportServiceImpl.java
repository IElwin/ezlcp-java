
package com.ezlcp.form.service;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.service.impl.SuperServiceImpl;
import com.ezlcp.form.entity.Report;
import com.ezlcp.form.mapper.ReportMapper;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;

/**
 * [报表或打印模板]业务服务类
 */
@Service
public class ReportServiceImpl extends SuperServiceImpl<ReportMapper, Report> implements BaseService<Report> {
    @Resource
    private ReportMapper reportMapper;

    @Override
    public BaseDao<Report> getRepository() {
        return reportMapper;
    }
}