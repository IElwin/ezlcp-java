
package com.ezlcp.form.service;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.service.impl.SuperServiceImpl;
import com.ezlcp.form.entity.File;
import com.ezlcp.form.mapper.FileMapper;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;

/**
 * [上传的文件统一保存在这张表中]业务服务类
 */
@Service
public class FileServiceImpl extends SuperServiceImpl<FileMapper, File> implements BaseService<File> {
    @Resource
    private FileMapper fileMapper;

    @Override
    public BaseDao<File> getRepository() {
        return fileMapper;
    }
}