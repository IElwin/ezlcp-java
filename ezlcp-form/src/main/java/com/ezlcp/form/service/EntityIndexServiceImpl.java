
package com.ezlcp.form.service;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.service.impl.SuperServiceImpl;
import com.ezlcp.form.entity.EntityIndex;
import com.ezlcp.form.mapper.EntityIndexMapper;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;

/**
 * [物理表的索引定义]业务服务类
 */
@Service
public class EntityIndexServiceImpl extends SuperServiceImpl<EntityIndexMapper, EntityIndex> implements BaseService<EntityIndex> {
    @Resource
    private EntityIndexMapper entityIndexMapper;

    @Override
    public BaseDao<EntityIndex> getRepository() {
        return entityIndexMapper;
    }
}