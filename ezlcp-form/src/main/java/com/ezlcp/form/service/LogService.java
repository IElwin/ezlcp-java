package com.ezlcp.form.service;

import com.ezlcp.commons.base.entity.IUser;
import com.ezlcp.commons.dto.SystemLog;
import com.ezlcp.commons.feign.LogClient;
import com.ezlcp.commons.feign.UserClient;
import com.ezlcp.commons.tool.IdGenerator;
import com.ezlcp.commons.tool.StringUtils;
import com.ezlcp.commons.utils.ContextUtil;
import com.ezlcp.commons.utils.RequestUtil;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * @author Elwin ZHANG
 * @description: 日志服务 <br/>
 * @date 2023/1/16 17:04
 */
@Service
public class LogService {
    @Resource
    private LogClient logClient;

    @Resource
    private UserClient menuClient;

    /***
     * @description: 写操作日志
     * @param module 模块名，一般对应控制器名称
     * @param action 操作，一般对应接口方法
     * @param recordId 单个记录ID，增删改的记录ID
     * @param detail 明细：新增或逻辑删除时可以不传，修改时传旧、新记录的JSON字符串，物理删除时传旧记录的JSON；其他操作可以传一些备注信息
     * @author Elwin ZHANG
     * @date 2022/5/18 10:01
     */
    public void saveSystemLog(String module, String action, String recordId, String detail) {
        SystemLog log = new SystemLog();
        log.setLogId(IdGenerator.getIdStr());
        IUser user = ContextUtil.getCurrentUser();
        //记录当前用户信息
        if (user != null) {
            log.setUserId(user.getUserId());
            log.setUserName(user.getFullName() + "(" + user.getAccount() + ")");
            log.setTenantId(user.getTenantId());
        }
        //记录IP，请求页面信息
        try {
            HttpServletRequest request = getCurRequest();
            String ipAddr = RequestUtil.getIpAddress(request);
            log.setIpAddr(ipAddr);
            //前端域名
            String host = request.getHeader("Origin");
            if (StringUtils.isEmpty(host)) {
                host = request.getHeader("Referer");
            }
            if (StringUtils.isEmpty(host)) {
                host = "";
            } else if (host.endsWith("/")) {
                host = host.substring(0, host.length() - 1);
            }
            //前端路由
            String pathName = request.getHeader("PathName");
            if (pathName == null) {
                pathName = "";
            }
            String path = host + pathName;
            if (path.length() > 100) {
                path = path.substring(0, 99);
            }
            log.setRefPage(path);
            //通过路由找到菜单名
            if(StringUtils.isNotEmpty(pathName)) {
                String menuName = menuClient.getMenuName(pathName);
                log.setMenuName(menuName);
            }
        } catch (Exception ee) {

        }
        //记录传入的信息
        log.setAction(action);
        log.setDetail(detail);
        log.setModule(module);
        log.setRecordId(recordId);
        logClient.saveSystemLog(log);
    }

    /***
     * @description: 获取当前上下文Request对象
     * @return javax.servlet.http.HttpServletRequest
     * @author Elwin ZHANG
     * @date 2022/5/18 9:49
     */
    public HttpServletRequest getCurRequest() {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (servletRequestAttributes != null) {
            return servletRequestAttributes.getRequest();
        }
        return null;
    }
}
