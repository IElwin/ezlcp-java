
package com.ezlcp.form.service;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.service.impl.SuperServiceImpl;
import com.ezlcp.form.entity.QueryPlan;
import com.ezlcp.form.mapper.QueryPlanMapper;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;

/**
 * [数据列表的查询方案]业务服务类
 */
@Service
public class QueryPlanServiceImpl extends SuperServiceImpl<QueryPlanMapper, QueryPlan> implements BaseService<QueryPlan> {
    @Resource
    private QueryPlanMapper queryPlanMapper;

    @Override
    public BaseDao<QueryPlan> getRepository() {
        return queryPlanMapper;
    }
}