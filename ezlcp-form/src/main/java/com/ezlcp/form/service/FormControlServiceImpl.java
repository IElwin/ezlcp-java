
package com.ezlcp.form.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.service.impl.SuperServiceImpl;
import com.ezlcp.form.entity.FormControl;
import com.ezlcp.form.mapper.FormControlMapper;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;

/**
 * [表单的控件]业务服务类
 */
@Service
public class FormControlServiceImpl extends SuperServiceImpl<FormControlMapper, FormControl> implements BaseService<FormControl> {
    @Resource
    private FormControlMapper formControlMapper;

    /***
    * 删除某表单关联的全部控件
    * @param formId 表单ID
    */
    public void delByFormId(String formId){
        LambdaQueryWrapper<FormControl> wrapper = new LambdaQueryWrapper();
        wrapper.eq(FormControl::getFormId,formId);
        this.formControlMapper.delete(wrapper);
    }
    @Override
    public BaseDao<FormControl> getRepository() {
        return formControlMapper;
    }
}