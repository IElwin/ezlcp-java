
package com.ezlcp.form.service;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.service.impl.SuperServiceImpl;
import com.ezlcp.form.entity.SubTableCol;
import com.ezlcp.form.mapper.SubTableColMapper;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;

/**
 * [表单子表列设置]业务服务类
 */
@Service
public class SubTableColServiceImpl extends SuperServiceImpl<SubTableColMapper, SubTableCol> implements BaseService<SubTableCol> {
    @Resource
    private SubTableColMapper subTableColMapper;

    @Override
    public BaseDao<SubTableCol> getRepository() {
        return subTableColMapper;
    }
}