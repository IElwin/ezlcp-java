
package com.ezlcp.form.service;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.service.impl.SuperServiceImpl;
import com.ezlcp.form.entity.FormEvent;
import com.ezlcp.form.mapper.FormEventMapper;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;

/**
 * [表单字段控件事件]业务服务类
 */
@Service
public class FormEventServiceImpl extends SuperServiceImpl<FormEventMapper, FormEvent> implements BaseService<FormEvent> {
    @Resource
    private FormEventMapper formEventMapper;

    @Override
    public BaseDao<FormEvent> getRepository() {
        return formEventMapper;
    }
}