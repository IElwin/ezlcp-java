package com.ezlcp.form.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.form.entity.SerialNo;
import org.apache.ibatis.annotations.Mapper;

/**
 * 單據編號，系统初始化公司ID为空的记录，每个公司创建后从初始记录复制数据库访问层
 */
@Mapper
public interface SerialNoMapper extends BaseDao<SerialNo> {
}