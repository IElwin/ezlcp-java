package com.ezlcp.form.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.form.entity.Form;
import org.apache.ibatis.annotations.Mapper;

/**
 * 表单（主表）定义数据库访问层
 */
@Mapper
public interface FormMapper extends BaseDao<Form> {
}