package com.ezlcp.form.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.form.entity.EntityCol;
import org.apache.ibatis.annotations.Mapper;

/**
 * 表的列定义数据库访问层
 */
@Mapper
public interface EntityColMapper extends BaseDao<EntityCol> {
}