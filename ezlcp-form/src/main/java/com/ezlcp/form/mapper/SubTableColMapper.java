package com.ezlcp.form.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.form.entity.SubTableCol;
import org.apache.ibatis.annotations.Mapper;

/**
 * 表单子表列设置数据库访问层
 */
@Mapper
public interface SubTableColMapper extends BaseDao<SubTableCol> {
}