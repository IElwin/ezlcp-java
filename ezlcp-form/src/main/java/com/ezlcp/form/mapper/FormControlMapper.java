package com.ezlcp.form.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.form.entity.FormControl;
import org.apache.ibatis.annotations.Mapper;

/**
 * 表单的控件数据库访问层
 */
@Mapper
public interface FormControlMapper extends BaseDao<FormControl> {
}