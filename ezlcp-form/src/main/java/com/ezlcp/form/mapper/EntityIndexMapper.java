package com.ezlcp.form.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.form.entity.EntityIndex;
import org.apache.ibatis.annotations.Mapper;

/**
 * 物理表的索引定义数据库访问层
 */
@Mapper
public interface EntityIndexMapper extends BaseDao<EntityIndex> {
}