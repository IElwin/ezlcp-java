package com.ezlcp.form.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.form.entity.FormButton;
import org.apache.ibatis.annotations.Mapper;

/**
 * 表单上的按钮数据库访问层
 */
@Mapper
public interface FormButtonMapper extends BaseDao<FormButton> {
}