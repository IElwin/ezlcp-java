package com.ezlcp.form.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.form.entity.ListButton;
import org.apache.ibatis.annotations.Mapper;

/**
 * 数据列表上的按钮数据库访问层
 */
@Mapper
public interface ListButtonMapper extends BaseDao<ListButton> {
}