package com.ezlcp.form.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.form.entity.Dictionary;
import org.apache.ibatis.annotations.Mapper;

/**
 * 数据字典数据库访问层
 */
@Mapper
public interface DictionaryMapper extends BaseDao<Dictionary> {
}