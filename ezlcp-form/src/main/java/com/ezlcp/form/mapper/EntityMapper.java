package com.ezlcp.form.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.form.entity.Entity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 数据库中表的定义数据库访问层
 */
@Mapper
public interface EntityMapper extends BaseDao<Entity> {
}