package com.ezlcp.form.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.form.entity.FormEvent;
import org.apache.ibatis.annotations.Mapper;

/**
 * 表单字段控件事件数据库访问层
 */
@Mapper
public interface FormEventMapper extends BaseDao<FormEvent> {
}