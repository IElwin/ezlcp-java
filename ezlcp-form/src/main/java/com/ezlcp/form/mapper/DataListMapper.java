package com.ezlcp.form.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.form.entity.DataList;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.poi.ss.formula.functions.T;

import java.util.Map;

/**
 * 查询列表定义数据库访问层
 */
@Mapper
public interface DataListMapper extends BaseDao<DataList> {


}