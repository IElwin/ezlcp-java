package com.ezlcp.form.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.form.entity.Module;
import org.apache.ibatis.annotations.Mapper;

/**
 * 对创建的实体，表单，流程进行分组数据库访问层
 */
@Mapper
public interface ModuleMapper extends BaseDao<Module> {
}