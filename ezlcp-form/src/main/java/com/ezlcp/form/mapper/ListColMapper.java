package com.ezlcp.form.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.form.entity.ListCol;
import org.apache.ibatis.annotations.Mapper;

/**
 * 数据列表的列定义数据库访问层
 */
@Mapper
public interface ListColMapper extends BaseDao<ListCol> {
}