package com.ezlcp.form.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.form.entity.FormComment;
import org.apache.ibatis.annotations.Mapper;

/**
 * 表单评论数据库访问层
 */
@Mapper
public interface FormCommentMapper extends BaseDao<FormComment> {
}