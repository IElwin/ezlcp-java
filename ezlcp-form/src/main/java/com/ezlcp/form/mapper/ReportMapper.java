package com.ezlcp.form.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.form.entity.Report;
import org.apache.ibatis.annotations.Mapper;

/**
 * 报表或打印模板数据库访问层
 */
@Mapper
public interface ReportMapper extends BaseDao<Report> {
}