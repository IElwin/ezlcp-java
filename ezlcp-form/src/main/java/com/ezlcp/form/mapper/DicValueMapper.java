package com.ezlcp.form.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.form.entity.DicValue;
import org.apache.ibatis.annotations.Mapper;

/**
 * 字典值数据库访问层
 */
@Mapper
public interface DicValueMapper extends BaseDao<DicValue> {
}