package com.ezlcp.form.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.form.entity.MultipleHeader;
import org.apache.ibatis.annotations.Mapper;

/**
 * 列表或表单子表的多层表头配置数据库访问层
 */
@Mapper
public interface MultipleHeaderMapper extends BaseDao<MultipleHeader> {
}