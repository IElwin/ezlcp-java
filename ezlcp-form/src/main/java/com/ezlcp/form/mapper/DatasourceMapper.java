package com.ezlcp.form.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.commons.dto.Datasource;
import org.apache.ibatis.annotations.Mapper;

/**
 * 自定义的数据库连接数据库访问层
 */
@Mapper
public interface DatasourceMapper extends BaseDao<Datasource> {
}