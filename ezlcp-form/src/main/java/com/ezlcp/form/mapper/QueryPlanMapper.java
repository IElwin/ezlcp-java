package com.ezlcp.form.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.form.entity.QueryPlan;
import org.apache.ibatis.annotations.Mapper;

/**
 * 数据列表的查询方案数据库访问层
 */
@Mapper
public interface QueryPlanMapper extends BaseDao<QueryPlan> {
}