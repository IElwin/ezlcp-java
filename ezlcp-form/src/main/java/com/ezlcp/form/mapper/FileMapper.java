package com.ezlcp.form.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.form.entity.File;
import org.apache.ibatis.annotations.Mapper;

/**
 * 上传的文件统一保存在这张表中数据库访问层
 */
@Mapper
public interface FileMapper extends BaseDao<File> {
}