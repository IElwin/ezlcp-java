package com.ezlcp.form.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.form.entity.FormValidate;
import org.apache.ibatis.annotations.Mapper;

/**
 * 表单的自定义校验数据库访问层
 */
@Mapper
public interface FormValidateMapper extends BaseDao<FormValidate> {
}