
package com.ezlcp.form.controller;

import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.form.entity.Report;
import com.ezlcp.form.service.ReportServiceImpl;
import com.ezlcp.form.web.BaseController;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/ezlcp/form/report")
@Tag(name = "报表或打印模板")
public class ReportController extends BaseController<Report> {
    @Resource
    ReportServiceImpl reportService;

    @Override
    public BaseService getBaseService() {
        return reportService;
    }

    @Override
    public String getComment() {
        return "报表或打印模板";
    }
}