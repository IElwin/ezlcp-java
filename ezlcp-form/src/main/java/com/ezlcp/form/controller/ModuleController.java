package com.ezlcp.form.controller;

import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.base.entity.JsonPageResult;
import com.ezlcp.commons.base.entity.JsonResult;
import com.ezlcp.commons.base.search.QueryFilter;
import com.ezlcp.commons.constant.Constants;
import com.ezlcp.commons.tool.StringUtils;
import com.ezlcp.commons.utils.ContextUtil;
import com.ezlcp.form.entity.Module;
import com.ezlcp.form.service.ModuleServiceImpl;
import com.ezlcp.form.web.BaseController;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/ezlcp/form/module")
@Tag(name = "对创建的实体，表单，流程进行分组")
public class ModuleController extends BaseController<Module> {
    @Resource
    ModuleServiceImpl moduleService;

    @Operation(summary = "f.可选择的父级节点列表,如果moduleId不存在可以传NULL")
    @PostMapping("/selectParent")
    public JsonResult selectParent(@Parameter(description = "当前模块Id") @RequestParam(value = "moduleId", required = false) String moduleId) {
        JsonResult result = JsonResult.getSuccessResult("common.handleSuccess");
        var data = moduleService.selectParent(moduleId);
        result.setData(data);
        return result;
    }

    @Operation(summary = "根据主键ID删除记录", description = "删除一条记录}")
    @PostMapping("del")
    @Override
    public JsonResult del(@RequestParam(value = "id") String id) {
        if (StringUtils.isEmpty(id)) {
            return JsonResult.Fail("common.paramError");
        }
        moduleService.deleteModule(id);
        JsonResult result = JsonResult.Success("common.delSuccess");
        result.setShow(false);
        logService.saveSystemLog(getComment(), "delete", id, "");
        return result;
    }

    @Override
    public JsonResult save(@Validated @RequestBody Module entity, BindingResult validResult) throws Exception {
        if (entity == null || StringUtils.isEmpty(entity.getModuleName())) {
            return JsonResult.Fail("common.paramError");
        }
        String pkId = entity.getPkId();
        //新增
        if (StringUtils.isEmpty(pkId)) {
            entity.setSeq(1);
            entity.setTenantId(ContextUtil.getCurrentTenantId());
        } else {
            entity.setSeq(entity.getSeq() + 1);
        }
        //处理父节点相关信息
        String parentId = entity.getParentId();
        if (StringUtils.isEmpty(parentId)) {
            entity.setLevel(Constants.SHORT1);
        } else {
            //获取父节点信息
            var parent = moduleService.getById(parentId);
            if (parent == null) {
                return JsonResult.Fail("common.paramError");
            }
            int level = parent.getLevel() + 1;
            entity.setLevel((short) level);
        }
        return super.save(entity, validResult);
    }

    @Override
    public JsonPageResult getAll() throws Exception {
        return JsonPageResult.getFail("common.apiClose");
    }

    @Operation(summary = "选择模块的下拉框数据来源")
    @GetMapping("/selectModule")
    public  JsonResult selectModule(){
        JsonResult result = JsonResult.getSuccessResult("common.handleSuccess");
        var data = moduleService.selectModule();
        result.setData(data);
        return result;
    }

    @Override
    protected void handleFilter(QueryFilter filter) {
        String tenantId = ContextUtil.getCurrentTenantId();
        if (StringUtils.isNotEmpty(tenantId)) {
            filter.addQueryParam("Q_tenant_id_S_EQ", tenantId);
        } else {
            filter.addQueryParam("Q_tenant_id_S_ISNULL", "1");
        }
    }

    @Override
    public BaseService getBaseService() {
        return moduleService;
    }

    @Override
    public String getComment() {
        return "模块";
    }
}