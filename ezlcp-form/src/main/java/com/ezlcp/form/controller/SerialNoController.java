package com.ezlcp.form.controller;

import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.base.entity.JsonPageResult;
import com.ezlcp.commons.base.entity.JsonResult;
import com.ezlcp.commons.base.search.QueryFilter;
import com.ezlcp.commons.tool.StringUtils;
import com.ezlcp.commons.utils.ContextUtil;
import com.ezlcp.form.entity.SerialNo;
import com.ezlcp.form.service.SerialNoServiceImpl;
import com.ezlcp.form.web.BaseController;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/ezlcp/form/serialNo")
@Tag(name = "流水单（单据编号）管理")
public class SerialNoController extends BaseController<SerialNo> {
    @Autowired
    SerialNoServiceImpl serialNoService;

    @Override
    protected void handleFilter(QueryFilter filter) {
        String tenantId = ContextUtil.getCurrentTenantId();
        if (StringUtils.isNotEmpty(tenantId)) {
            filter.addQueryParam("Q_sn.tenant_id_S_EQ", tenantId);
        }
    }

    @Operation(summary = "c.获取规则的详细介绍")
    @GetMapping(value = "getRuleHelp")
    public JsonResult getRuleHelp() {
        String help = "{yyyy}:4位数的年份，如2022年，\n" + "{yy}:两位数的年份，如22年；\n" +
                "{MM}:两位数的月份，如01月，\n" + "{mm}:正常的月份，前面不补0，如1月；\n" +
                "{DD}:两位数的日，如08日，\n" + "{dd}:正常的日，前面不补0，如8日；\n" +
                "{NO}:指定长度的编号，如0002，\n" + "{no}:正常编号，前面不补0，如2；最多只能有一个；\n" +
                "{yyyyMM}:固定6位长度的年月，如202203；\n" +
                "{yyyyMMDD}:固定8位长度的年月日，如20220606。\n";
        String help2 = "{yyyy}:4位数的年份，如2022年，\n<br>" + "{yy}:两位数的年份，如22年；\n<br>" +
                "{MM}:两位数的月份，如01月，\n<br>" + "{mm}:正常的月份，前面不补0，如1月；\n<br>" +
                "{DD}:两位数的日，如08日，\n<br>" + "{dd}:正常的日，前面不补0，如8日；\n<br>" +
                "{NO}:指定长度的编号，如0002，\n<br>" + "{no}:正常编号，前面不补0，如2；最多只能有一个；\n<br>" +
                "{yyyyMM}:固定6位长度的年月，如202203；\n<br>" +
                "{yyyyMMDD}:固定8位长度的年月日，如20220606。\n<br>";
        return JsonResult.getSuccessResult(help2, help);
    }

    @Operation(summary = "d.校验规则", description = "传入实体对象")
    @PostMapping(value = "validRule")
    public JsonResult validRule(@RequestBody SerialNo serialNo) {
        if (serialNo == null) {
            return JsonResult.Fail("common.paramError");
        }
        return serialNoService.validRule(serialNo);
    }

    @Operation(summary = "e.单条记录保存")
    @Override
    @PostMapping("/save")
    public JsonResult save(@RequestBody SerialNo entity, BindingResult validResult) throws Exception {
        if (entity == null) {
            return JsonResult.Fail("common.paramError");
        }
        JsonResult result = serialNoService.validRule(entity);
        if (!result.isSuccess()) {
            return result;
        }
        String pkId = entity.getPkId();
        //新增
        if (StringUtils.isEmpty(pkId)) {
            entity.setSeq(1);
            entity.setCurrentVal(0l);
            entity.setTenantId(ContextUtil.getCurrentTenantId());
        } else {
            entity.setSeq(entity.getSeq() + 1);
            entity.setCurrentVal(null);
        }
        int resetType = serialNoService.getResetType(entity.getRules());
        entity.setResetType((short) resetType);
        return super.save(entity, validResult);
    }

    @Operation(summary = "根据主键ID删除记录", description = "删除一条记录}")
    @PostMapping("del")
    @Override
    public JsonResult del(@RequestParam(value = "id") String id) {
        if (StringUtils.isEmpty(id)) {
            return JsonResult.Fail("common.paramError");
        }
        //TODO 判断是否被自定义表单引用
        serialNoService.deleteSerialNo(id);
        JsonResult result = JsonResult.Success("common.delSuccess");
        logService.saveSystemLog(getComment(), "delete", id, "");
        return result;
    }

    @Override
    public JsonPageResult getAll() throws Exception {
        return JsonPageResult.getFail("common.apiClose");
    }

    @Override
    public BaseService getBaseService() {
        return serialNoService;
    }

    @Override
    public String getComment() {
        return "单据流水号";
    }
}