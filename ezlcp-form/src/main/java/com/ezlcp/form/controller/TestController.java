package com.ezlcp.form.controller;

import com.ezlcp.commons.base.entity.JsonResult;
import com.ezlcp.commons.tool.DateUtils;
import com.ezlcp.commons.tool.StringUtils;
import com.ezlcp.commons.utils.DruidUtils;
import com.ezlcp.form.db.DBHelper;
import com.github.drinkjava2.jdialects.Dialect;
import com.github.drinkjava2.jdialects.model.TableModel;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.SneakyThrows;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Elwin ZHANG
 * @description: <br/>
 * @date 2023/7/3 14:34
 */
@RestController
@RequestMapping("/ezlcp/form/test")
@Tag(name = "测试控制器")
public class TestController {

    @Operation(summary = "表结构定义")
    @PostMapping("getColumns")
    @SneakyThrows
    public JsonResult getColumns() {
        String dsId = "1675805988146180097";
        var client = DBHelper.getDbClient(dsId);
        String tableName = "t_abcdefg";
        var data = client.getTableModel(tableName);
        JsonResult result = JsonResult.getSuccessResult("common.handleSuccess");
        return result;
    }

    @SneakyThrows
    @Operation(summary = "测试创建表,事务")
    @PostMapping("createTable")
    public JsonResult createTable(@Parameter(description = "表名") @RequestParam("tableName") String tableName,
                                  @Parameter(description = "dsId") @RequestParam(value = "dsId", required = false) String dsId) {
        if (StringUtils.isEmpty(dsId)) {
            dsId = "1664472893795082241";
        }
        var ds = DruidUtils.getDataSource(dsId);
        var template = new JdbcTemplate(ds);
        //手动事务
        var txManager=new DataSourceTransactionManager(ds);
        DefaultTransactionDefinition transDef = new DefaultTransactionDefinition(); // 定义事务属性
        transDef.setPropagationBehavior(DefaultTransactionDefinition.PROPAGATION_REQUIRED); // 设置传播行为属性
        TransactionStatus status = txManager.getTransaction(transDef); // 获得事务状态
        try {
            TableModel t = new TableModel(tableName);
            t.comment("测试创建的表");
            t.column("id").VARCHAR(64).pkey().comment("主键");
            t.column("name").VARCHAR(50).comment("姓名");
            t.column("age").INTEGER().comment("年龄");
            t.column("create_time").TIMESTAMP().comment("创建时间");
            t.column("create_by").VARCHAR(64).comment("创建人");
            Dialect dialect = Dialect.guessDialect(ds);
            String[] ddlArray = dialect.toDropAndCreateDDL(t);
            for (String ddl : ddlArray) {
                template.execute(ddl);
            }
            String now = DateUtils.getTime();
            String sql = "insert into " + tableName + " (id,name,age,create_time) values('101','李明',18,'" + now + "')";
            template.execute(sql);
            int j=1/0;
            sql = "insert into " + tableName + " (id,name,age,create_time) values('102','李明2',38,'" + now + "')";
            template.execute(sql);
            sql = "select * from " + tableName;
            JsonResult result = JsonResult.getSuccessResult("common.handleSuccess");
            var data = template.queryForList(sql);
            result.setData(data);
            txManager.commit(status);// 提交
            return result;
        } catch (Exception e) {
            txManager.rollback(status);// 回滚
            return JsonResult.Fail(e.getMessage());
        }
    }
}
