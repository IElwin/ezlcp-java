package com.ezlcp.form.controller;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson2.JSON;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.base.entity.JsonPageResult;
import com.ezlcp.commons.base.entity.JsonResult;
import com.ezlcp.commons.constant.StatusEnum;
import com.ezlcp.commons.utils.ContextUtil;
import com.ezlcp.form.entity.DicValue;
import com.ezlcp.form.service.DicValueServiceImpl;
import com.ezlcp.form.web.BaseController;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/ezlcp/form/dicValue")
@Tag(name = "字典值")
public class DicValueController extends BaseController<DicValue> {
    @Autowired
    DicValueServiceImpl dicValueService;


    @Operation(summary = "根据字典id查询字典值")
    @GetMapping("/getDicValueByDicId")
    public JsonResult getDicValueByDicId(@Parameter(description = "字典值关联字典id") @RequestParam(value = "dicId") String dicId) {
        return JsonResult.getSuccessResult(dicValueService.getByDicId(dicId), "common.selSuccess");
    }

    @Operation(summary = "根据字典key查询字典值")
    @GetMapping("/getDicValueByDicKey")
    public JsonResult getDicValueByDicKey(@Parameter(description = "字典值关联字典key") @RequestParam String dicKey) {
        return JsonResult.getSuccessResult(dicValueService.getByDicKey(dicKey));
    }

    @Operation(summary = "新增编辑字典值")
    @PostMapping("/saveOrUpdateDicValue")
    public JsonResult saveOrUpdateDicValue(@Parameter(description = "字典值信息") @RequestBody List<DicValue> dicValueList) {
        Set<String> set = dicValueList.stream().map(DicValue::getShowText).collect(Collectors.toSet());
        if (set.size() != dicValueList.size()) {
            return JsonResult.getFailResult("dicValue.addElementReuse");
        }
        String msg = dicValueService.saveOrUpdateDicValue(dicValueList);
        if (msg.length() > 0) {
            return JsonResult.getFailResult(msg);
        }
        return JsonResult.getSuccessResult("common.handleSuccess");
    }

    @Operation(summary = "删除字典值")
    @DeleteMapping("/deleteDicValue/{valId}")
    public JsonResult deleteDicValue(@Parameter(description = "字典值id") @PathVariable(name = "valId") String valId) {
        DicValue oldDicValue = dicValueService.getById(valId);
        if (ObjectUtil.isNull(oldDicValue.getTenantId())) {
            return JsonResult.Fail("common.noHandle");
        }
        if (!oldDicValue.getTenantId().equals(ContextUtil.getCurrentTenantId())) {
            return JsonResult.Fail("common.noHandle");
        }
        DicValue dicValue = new DicValue();
        dicValue.setValId(valId);
        dicValue.setStatus((short) StatusEnum.deleted.getValue());
        dicValue.setSeq(oldDicValue.getSeq() + 1);
        boolean deleteStatus = dicValueService.updateById(dicValue);
        String detail = "old:" + JSON.toJSONString(oldDicValue) + "new:" + JSON.toJSONString(dicValue);
        logService.saveSystemLog(getComment(), "deleteDicValue", valId, detail);
        return deleteStatus ? JsonResult.getSuccessResult("common.delSuccess") : JsonResult.getFailResult("common.delFail");
    }

    @Override
    @Operation(summary = "【已关闭】根据主键ID删除记录", description = "根据主键ID删除记录,parameters is {ids:'1,2'}")
    public JsonResult del(String ids) {
        return null;
    }

    @Override
    @Operation(summary = "【已关闭】查询当前业务表的所有数据", description = "查询当前实体表的所有数据。")
    public JsonPageResult getAll() {
        return null;
    }

    @Override
    @Operation(summary = "【已关闭】保存业务数据记录", description = "根据提交的业务JSON数据保存业务数据记录")
    public JsonResult save(DicValue entity, BindingResult validResult) {
        return null;
    }

    @Override
    @Operation(summary = "【已关闭】根据主键IDS查询业务数据详细信息", description = "根据主键IDS查询业务数据详细信息")
    public JsonResult<DicValue> getByIds(String ids) {
        return null;
    }

    @Override
    public BaseService getBaseService() {
        return dicValueService;
    }

    @Override
    public String getComment() {
        return "字典值";
    }
}