
package com.ezlcp.form.controller;

import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.form.entity.QueryPlan;
import com.ezlcp.form.service.QueryPlanServiceImpl;
import com.ezlcp.form.web.BaseController;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/ezlcp/form/queryPlan")
@Tag(name = "数据列表的查询方案")
public class QueryPlanController extends BaseController<QueryPlan> {
    @Resource
    QueryPlanServiceImpl queryPlanService;

    @Override
    public BaseService getBaseService() {
        return queryPlanService;
    }

    @Override
    public String getComment() {
        return "数据列表的查询方案";
    }
}