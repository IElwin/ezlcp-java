package com.ezlcp.form.db;

/**
 * @author Elwin ZHANG
 * @description: 物理表状态枚举 <br/>
 * @date 2023/7/7 10:09
 */
public enum TableStatusEnum {
    /**
     * 不存在
     */
    NotExist(0),
    /**
     * 存在，无数据
     */
    Empty(1),
    /**
     * 有数据
     */
    HasData(2);

    private int value;

    TableStatusEnum(int value) {
        this.value = value;
    }

    /***
     * @description 获取整型的值
     */
    public int getValue() {
        return value;
    }

    /***
     * @description 获取字符串类型的值
     */
    public String getCode() {
        return value + "";
    }
}
