package com.ezlcp.form.db;

import com.ezlcp.commons.utils.DruidUtils;

/**
 * @author Elwin ZHANG
 * @description: 自定义数据源相关类 <br/>
 * @date 2023/7/13 15:53
 */
public class DBHelper {

    private static final String TABLE_NAME = "TABLE_NAME";

    /***
     * @description 根据数据源ID，获取数据库操作类
     * @param dsId 数据源ID
     * @return com.ezlcp.form.db.IDbClient
     * @author Elwin ZHANG
     * @date 2023/7/13 15:56
     */
    public static IDbClient getDbClient(String dsId) {
        var ds = DruidUtils.getDataSource(dsId);
        if (ds == null) {
            //数据库连接失败
            throw new RuntimeException("datasource.connFail");
        }
        //var dbType=ds.getDbType();
        //根据dbType判断使用哪个操作类，目前统一使用默认实现类
        return new DBClient(ds);
    }

}
