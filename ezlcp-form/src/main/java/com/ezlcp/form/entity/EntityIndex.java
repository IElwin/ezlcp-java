package com.ezlcp.form.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ezlcp.commons.base.entity.BaseExtEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

/**
 * <pre>
 *
 * 描述：物理表的索引定义实体类定义
 * 表:f_entity_index
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-03-28 10:42:47
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "f_entity_index")
@Schema(description = "实体索引")
public class EntityIndex extends BaseExtEntity<java.lang.String> {
    @Schema(description = "ID")
    @TableId(value = "id", type = IdType.INPUT)
	private String id;
    @Schema(description = "所属实体")
    @TableField(value = "ent_id", jdbcType = JdbcType.VARCHAR)
    private String entId;
    @Schema(description = "一个或多个字段")
    @TableField(value = "fields", jdbcType = JdbcType.VARCHAR)
    private String fields;
    @Schema(description = "索引名（数据库对象）")
    @TableField(value = "index_name", jdbcType = JdbcType.VARCHAR)
    private String indexName;
    @Schema(description = "是否唯一")
    @TableField(value = "is_unique", jdbcType = JdbcType.NUMERIC)
    private Short isUnique;
    @Schema(description = "修改次数")
    @TableField(value = "seq", jdbcType = JdbcType.NUMERIC)
    private Integer seq;
    @Schema(description = "0草稿1已生效4删除")
    @TableField(value = "status", jdbcType = JdbcType.NUMERIC)
    private Short status;

    @Schema(description = "公司ID")
    @TableField(value = "tenant_id", jdbcType = JdbcType.VARCHAR)
    private String tenantId;

    @JsonCreator
    public EntityIndex() {
    }

    @Override
    public String getPkId() {
        return id;
    }

    @Override
    public void setPkId(String pkId) {
        this.id = pkId;
    }
}