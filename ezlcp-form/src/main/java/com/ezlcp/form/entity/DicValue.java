package com.ezlcp.form.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ezlcp.commons.base.entity.BaseExtEntity;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

/**
 * <pre>
 *
 * 描述：字典值实体类定义
 * 表:s_dic_value
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-01-04 15:12:48
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "f_dic_value")
@Schema(description ="字典值/下拉项")
public class DicValue extends BaseExtEntity<java.lang.String> {
    @Schema(description = "记录ID")
    @TableId(value = "val_id", type = IdType.INPUT)
	private String valId;
    @Schema(description = "字典ID")
    @TableField(value = "dic_id", jdbcType = JdbcType.VARCHAR)
    private String dicId;
    @Schema(description = "关键字")
    @TableField(value = "dic_key", jdbcType = JdbcType.VARCHAR)
    private String dicKey;
    @Schema(description = "保存值")
    @TableField(value = "save_val", jdbcType = JdbcType.VARCHAR)
    private String saveVal;
    @Schema(description = "显示文本")
    @TableField(value = "show_text", jdbcType = JdbcType.VARCHAR)
    private String showText;
    @Schema(description = "繁体中文")
    @TableField(value = "hk_text", jdbcType = JdbcType.VARCHAR)
    private String hkText;
    @Schema(description = "英文")
    @TableField(value = "en_text", jdbcType = JdbcType.VARCHAR)
    private String enText;
    @Schema(description = "显示顺序")
    @TableField(value = "sort_no", jdbcType = JdbcType.NUMERIC)
    private Integer sortNo;
    @Schema(description = "系统内置")
    @TableField(value = "is_fixed", jdbcType = JdbcType.NUMERIC)
    private Short isFixed;
    @Schema(description = "状态")
    @TableField(value = "status", jdbcType = JdbcType.NUMERIC)
    private Short status;
    @Schema(description = "修改次数")
    @TableField(value = "seq", jdbcType = JdbcType.NUMERIC)
    private Integer seq;
    @Schema(description = "公司ID")
    @TableField(value = "tenant_id", jdbcType = JdbcType.VARCHAR)
    private String tenantId;
    @JsonCreator
    public DicValue() {
    }

    @Override
    public String getPkId() {
        return valId;
    }

    @Override
    public void setPkId(String pkId) {
        this.valId = pkId;
    }
}