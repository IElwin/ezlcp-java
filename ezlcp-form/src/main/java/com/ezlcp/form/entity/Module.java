package com.ezlcp.form.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ezlcp.commons.base.entity.BaseExtEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

/**
 * <pre>
 *
 * 描述：对创建的实体，表单，流程进行分组实体类定义
 * 表:f_module
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-03-28 10:42:47
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "f_module")
@Schema(description = "模块")
public class Module extends BaseExtEntity<java.lang.String> {
    @Schema(description = "模块ID")
    @TableId(value = "module_id", type = IdType.INPUT)
	private String moduleId;
    @Schema(description = "英文名称")
    @TableField(value = "en_name", jdbcType = JdbcType.VARCHAR)
    private String enName;
    @Schema(description = "繁体中文名称")
    @TableField(value = "hk_name", jdbcType = JdbcType.VARCHAR)
    private String hkName;
    @Schema(description = "名称")
    @TableField(value = "module_name", jdbcType = JdbcType.VARCHAR)
    private String moduleName;
    @Schema(description = "父模块ID")
    @TableField(value = "parent_id", jdbcType = JdbcType.VARCHAR)
    private String parentId;
    @Schema(description = "备注")
    @TableField(value = "remark", jdbcType = JdbcType.VARCHAR)
    private String remark;
    @Schema(description = "修改次数")
    @TableField(value = "seq", jdbcType = JdbcType.NUMERIC)
    private Integer seq;
    @Schema(description = "0停用1启用4删除")
    @TableField(value = "status", jdbcType = JdbcType.NUMERIC)
    private Short status;
    @Schema(description = "排序")
    @TableField(value = "sort", jdbcType = JdbcType.NUMERIC)
    private Integer sort;
    @Schema(description = "等级")
    @TableField(value = "level", jdbcType = JdbcType.NUMERIC)
    private Short level;
    @Schema(description = "公司ID")
    @TableField(value = "tenant_id", jdbcType = JdbcType.VARCHAR)
    private String tenantId;
    @JsonCreator
    public Module() {
    }

    @Override
    public String getPkId() {
        return moduleId;
    }

    @Override
    public void setPkId(String pkId) {
        this.moduleId = pkId;
    }
}