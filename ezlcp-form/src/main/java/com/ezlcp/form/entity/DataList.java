package com.ezlcp.form.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ezlcp.commons.base.entity.BaseExtEntity;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

import java.util.List;

/**
 * <pre>
 *
 * 描述：查询列表定义实体类定义
 * 表:f_data_list
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-03-28 10:42:46
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "f_data_list")
@Schema(description = "数据列表")
public class DataList extends BaseExtEntity<java.lang.String> {
    @Schema(description = "ID")
    @TableId(value = "id", type = IdType.INPUT)
    private String id;
    @Schema(description = "允许多选")
    @TableField(value = "can_select_more", jdbcType = JdbcType.NUMERIC)
    private Short canSelectMore;
    @Schema(description = "单击行事件")
    @TableField(value = "click_event", jdbcType = JdbcType.VARCHAR)
    private String clickEvent;
    @Schema(description = "来源ID")
    @TableField(value = "copy_from", jdbcType = JdbcType.VARCHAR)
    private String copyFrom;
    @Schema(description = "双击行事件")
    @TableField(value = "dbl_click_event", jdbcType = JdbcType.VARCHAR)
    private String dblClickEvent;
    @Schema(description = "实体ID")
    @TableField(value = "ent_id", jdbcType = JdbcType.VARCHAR)
    private String entId;
    @Schema(description = "英文名称")
    @TableField(value = "en_name", jdbcType = JdbcType.VARCHAR)
    private String enName;
    @Schema(description = "1PC2移动")
    @TableField(value = "end_type", jdbcType = JdbcType.NUMERIC)
    private Short endType;
    @Schema(description = "繁体中文名称")
    @TableField(value = "hk_name", jdbcType = JdbcType.VARCHAR)
    private String hkName;
    @Schema(description = "图标")
    @TableField(value = "icon", jdbcType = JdbcType.VARCHAR)
    private String icon;
    @Schema(description = "系统内置")
    @TableField(value = "is_build_in", jdbcType = JdbcType.NUMERIC)
    private Short isBuildIn;
    @Schema(description = "是否懒加载")
    @TableField(value = "is_lazy", jdbcType = JdbcType.NUMERIC)
    private Short isLazy;
    @Schema(description = "是否分页")
    @TableField(value = "is_paging", jdbcType = JdbcType.NUMERIC)
    private Short isPaging;
    @Schema(description = "行高")
    @TableField(value = "line_height", jdbcType = JdbcType.VARCHAR)
    private String lineHeight;
    @Schema(description = "0表格1看板2相册3主从表4树表5组合表6图表7甘特图8日历")
    @TableField(value = "list_type", jdbcType = JdbcType.NUMERIC)
    private Short listType;
    @Schema(description = "移动表单")
    @TableField(value = "mb_form_id", jdbcType = JdbcType.VARCHAR)
    private String mbFormId;
    @Schema(description = "移动表单-只读")
    @TableField(value = "mb_form_read_id", jdbcType = JdbcType.VARCHAR)
    private String mbFormReadId;
    @Schema(description = "所属模块")
    @TableField(value = "module_id", jdbcType = JdbcType.VARCHAR)
    private String moduleId;
    @Schema(description = "列表名称")
    @TableField(value = "name", jdbcType = JdbcType.VARCHAR)
    private String name;
    @Schema(description = "每页行数")
    @TableField(value = "page_size", jdbcType = JdbcType.NUMERIC)
    private int pageSize;
    @Schema(description = "PC表单")
    @TableField(value = "pc_form_id", jdbcType = JdbcType.VARCHAR)
    private String pcFormId;
    @Schema(description = "PC表单-只读")
    @TableField(value = "pc_form_read_id", jdbcType = JdbcType.VARCHAR)
    private String pcFormReadId;
    @Schema(description = "备注")
    @TableField(value = "remark", jdbcType = JdbcType.VARCHAR)
    private String remark;
    @Schema(description = "修改次数")
    @TableField(value = "seq", jdbcType = JdbcType.NUMERIC)
    private Integer seq;
    @Schema(description = "更多配置")
    @TableField(value = "settings", jdbcType = JdbcType.VARCHAR)
    private String settings;
    @Schema(description = "显示高级查询")
    @TableField(value = "show_high_filter", jdbcType = JdbcType.NUMERIC)
    private Short showHighFilter;
    @Schema(description = "显示查询方案")
    @TableField(value = "show_query_plan", jdbcType = JdbcType.NUMERIC)
    private Short showQueryPlan;
    @Schema(description = "显示序号列")
    @TableField(value = "show_serial_no", jdbcType = JdbcType.NUMERIC)
    private Short showSerialNo;
    @Schema(description = "删除后执行SQL")
    @TableField(value = "sql_after_del", jdbcType = JdbcType.VARCHAR)
    private String sqlAfterDel;
    @Schema(description = "删除前校验SQL")
    @TableField(value = "sql_before_del", jdbcType = JdbcType.VARCHAR)
    private String sqlBeforeDel;
    @Schema(description = "关联子句")
    @TableField(value = "sql_join", jdbcType = JdbcType.VARCHAR)
    private String sqlJoin;
    @Schema(description = "排序子句")
    @TableField(value = "sql_oder_by", jdbcType = JdbcType.VARCHAR)
    private String sqlOderBy;
    @Schema(description = "固定查询条件")
    @TableField(value = "sql_where", jdbcType = JdbcType.VARCHAR)
    private String sqlWhere;
    @Schema(description = "0草稿1发布4作废")
    @TableField(value = "status", jdbcType = JdbcType.NUMERIC)
    private Short status;
    @Schema(description = "版本")
    @TableField(value = "ver", jdbcType = JdbcType.NUMERIC)
    private Integer ver;
    @Schema(description = "公司ID")
    @TableField(value = "tenant_id", jdbcType = JdbcType.VARCHAR)
    private String tenantId;
    @Schema(description = "前置SQL")
    @TableField(value = "before_sql", jdbcType = JdbcType.VARCHAR)
    private String beforeSql;

    @Schema(description = "模块名")
    @TableField(exist = false)
    private String moduleName;
    @Schema(description = "模块繁体名")
    @TableField(exist = false)
    private String hkModuleName;
    @TableField(exist = false)
    @Schema(description = "模块英文名")
    private String enModuleName;

    @TableField(exist = false)
    @Schema(description = "实体名")
    private String entName;
    @TableField(exist = false)
    @Schema(description = "实体繁体名")
    private String hkEntName;
    @TableField(exist = false)
    @Schema(description = "实体英文名")
    private String enEntName;

    @TableField(exist = false)
    @Schema(description = "主实体表名")
    private String mainTableName;

    @TableField(exist = false)
    @Schema(description = "数据列表列信息")
    private List<ListCol> cols;

    @JsonCreator
    public DataList() {
    }

    @Override
    public String getPkId() {
        return id;
    }

    @Override
    public void setPkId(String pkId) {
        this.id = pkId;
    }
}