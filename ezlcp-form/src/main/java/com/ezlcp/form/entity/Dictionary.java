package com.ezlcp.form.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ezlcp.commons.base.entity.BaseExtEntity;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

import java.util.List;

/**
 * <pre>
 *
 * 描述：数据字典实体类定义
 * 表:s_dictionary
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-01-04 15:12:02
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "f_dictionary")
@Schema(description ="字典/下拉框")
public class Dictionary extends BaseExtEntity<java.lang.String> {
    @Schema(description = "字典ID")
    @TableId(value = "dic_id", type = IdType.INPUT)
	private String dicId;
    @Schema(description = "关键字")
    @TableField(value = "dic_key", jdbcType = JdbcType.VARCHAR)
    private String dicKey;
    @Schema(description = "名称")
    @TableField(value = "dic_name", jdbcType = JdbcType.VARCHAR)
    private String dicName;
    @Schema(description = "繁体中文名")
    @TableField(value = "hk_name", jdbcType = JdbcType.VARCHAR)
    private String hkName;
    @Schema(description = "英文名称")
    @TableField(value = "en_name", jdbcType = JdbcType.VARCHAR)
    private String enName;
    @Schema(description = "显示顺序")
    @TableField(value = "sort_no", jdbcType = JdbcType.NUMERIC)
    private Integer sortNo;
    @Schema(description = "系统内置")
    @TableField(value = "is_fixed", jdbcType = JdbcType.SMALLINT)
    private Short isFixed;
    @Schema(description = "状态")
    @TableField(value = "status", jdbcType = JdbcType.SMALLINT)
    private Short status;
    @Schema(description = "所属模块")
    @TableField(value = "module_id", jdbcType = JdbcType.VARCHAR)
    private String moduleId;
    @Schema(description = "修改次数")
    @TableField(value = "seq", jdbcType = JdbcType.NUMERIC)
    private Integer seq;
    @Schema(description = "公司ID")
    @TableField(value = "tenant_id", jdbcType = JdbcType.VARCHAR)
    private String tenantId;
    @Schema(description = "关联字典值列表")
    @TableField(exist = false)
    private List<DicValue> dicValueList;
    @Schema(description = "模块名")
    @TableField(exist = false)
    private String moduleName;
    @Schema(description = "模块繁体名")
    @TableField(exist = false)
    private String hkModuleName;
    @TableField(exist = false)
    @Schema(description = "模块英文名")
    private String enModuleName;

    /**
    * 状态词典ID
    */
    public  static final String STATUS_DIC_ID="1";

    @JsonCreator
    public Dictionary() {
    }

    @Override
    public String getPkId() {
        return dicId;
    }

    @Override
    public void setPkId(String pkId) {
        this.dicId = pkId;
    }
}