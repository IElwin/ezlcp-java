package com.ezlcp.form.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ezlcp.commons.base.entity.BaseExtEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

/**
 * <pre>
 *
 * 描述：数据列表的列定义实体类定义
 * 表:f_list_col
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-03-28 10:42:47
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "f_list_col")
@Schema(description = "列表列")
public class ListCol extends BaseExtEntity<java.lang.String> {
    @Schema(description = "ID")
    @TableId(value = "id", type = IdType.INPUT)
	private String id;
    @Schema(description = "背景色")
    @TableField(value = "bg_color", jdbcType = JdbcType.VARCHAR)
    private String bgColor;
    @Schema(description = "显示顺序")
    @TableField(value = "col_order", jdbcType = JdbcType.NUMERIC)
    private Short colOrder;
    @Schema(description = "列类型")
    @TableField(value = "col_type", jdbcType = JdbcType.VARCHAR)
    private String colType;
    @Schema(description = "列宽")
    @TableField(value = "col_width", jdbcType = JdbcType.VARCHAR)
    private String colWidth;
    @Schema(description = "数据格式")
    @TableField(value = "data_format", jdbcType = JdbcType.VARCHAR)
    private String dataFormat;
    @Schema(description = "实体列ID")
    @TableField(value = "entity_col_id", jdbcType = JdbcType.VARCHAR)
    private String entityColId;
    @Schema(description = "物理字段名")
    @TableField(value = "field_name", jdbcType = JdbcType.VARCHAR)
    private String fieldName;
    @Schema(description = "字段别名")
    @TableField(value = "field_alias", jdbcType = JdbcType.VARCHAR)
    private String fieldAlias;
    @Schema(description = "字体颜色")
    @TableField(value = "font_color", jdbcType = JdbcType.VARCHAR)
    private String fontColor;
    @Schema(description = "字体大小")
    @TableField(value = "font_size", jdbcType = JdbcType.VARCHAR)
    private String fontSize;
    @Schema(description = "英文列标题")
    @TableField(value = "header_en", jdbcType = JdbcType.VARCHAR)
    private String headerEn;
    @Schema(description = "繁体列标题")
    @TableField(value = "header_hk", jdbcType = JdbcType.VARCHAR)
    private String headerHk;
    @Schema(description = "列标题")
    @TableField(value = "header_text", jdbcType = JdbcType.VARCHAR)
    private String headerText;
    @Schema(description = "是否导出")
    @TableField(value = "is_export", jdbcType = JdbcType.NUMERIC)
    private Short isExport;
    @Schema(description = "是否过滤")
    @TableField(value = "is_filter", jdbcType = JdbcType.NUMERIC)
    private Short isFilter;
    @Schema(description = "是否冻结")
    @TableField(value = "is_fixed", jdbcType = JdbcType.NUMERIC)
    private Short isFixed;
    @Schema(description = "是否关联字段")
    @TableField(value = "is_fk", jdbcType = JdbcType.NUMERIC)
    private Short isFk;
    @Schema(description = "显示为链接")
    @TableField(value = "is_link", jdbcType = JdbcType.NUMERIC)
    private Short isLink;
    @Schema(description = "可排序")
    @TableField(value = "is_sort", jdbcType = JdbcType.NUMERIC)
    private Short isSort;
    @Schema(description = "链接地址")
    @TableField(value = "link_url", jdbcType = JdbcType.VARCHAR)
    private String linkUrl;
    @Schema(description = "是否移动端显示")
    @TableField(value = "is_mobile", jdbcType = JdbcType.NUMERIC)
    private Short isMobile;
    @Schema(description = "数据字典ID")
    @TableField(value = "dict_id", jdbcType = JdbcType.VARCHAR)
    private String dictId;
    @Schema(description = "列表ID")
    @TableField(value = "list_id", jdbcType = JdbcType.VARCHAR)
    private String listId;
    @Schema(description = "其它配置")
    @TableField(value = "others", jdbcType = JdbcType.VARCHAR)
    private String others;
    @Schema(description = "父列头")
    @TableField(value = "parent_header_id", jdbcType = JdbcType.VARCHAR)
    private String parentHeaderId;
    @Schema(description = "修改次数")
    @TableField(value = "seq", jdbcType = JdbcType.NUMERIC)
    private Integer seq;
    @Schema(description = "统计函数")
    @TableField(value = "stat_func", jdbcType = JdbcType.VARCHAR)
    private String statFunc;
    @Schema(description = "表名或别名")
    @TableField(value = "table_alias", jdbcType = JdbcType.VARCHAR)
    private String tableAlias;
    @Schema(description = "0左，1中，2右")
    @TableField(value = "text_align", jdbcType = JdbcType.NUMERIC)
    private Short textAlign;
    @Schema(description = "公司ID")
    @TableField(value = "tenant_id", jdbcType = JdbcType.VARCHAR)
    private String tenantId;

    @JsonCreator
    public ListCol() {
    }

    @Override
    public String getPkId() {
        return id;
    }

    @Override
    public void setPkId(String pkId) {
        this.id = pkId;
    }
}