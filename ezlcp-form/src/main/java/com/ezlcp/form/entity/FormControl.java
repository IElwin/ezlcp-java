package com.ezlcp.form.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ezlcp.commons.base.entity.BaseExtEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

/**
 * <pre>
 *
 * 描述：表单的控件实体类定义
 * 表:f_form_control
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-03-28 10:42:47
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "f_form_control")
@Schema(description = "表单控件")
public class FormControl extends BaseExtEntity<java.lang.String> {
    @Schema(description = "控件ID")
    @TableId(value = "control_id", type = IdType.INPUT)
	private String controlId;
    @Schema(description = "样式")
    @TableField(value = "style", jdbcType = JdbcType.VARCHAR)
    private String style;
    @Schema(description = "关联实体列")
    @TableField(value = "col_id", jdbcType = JdbcType.VARCHAR)
    private String colId;
    @Schema(description = "显示顺序")
    @TableField(value = "col_order", jdbcType = JdbcType.NUMERIC)
    private Short colOrder;
    @Schema(description = "所占列数")
    @TableField(value = "col_spans", jdbcType = JdbcType.NUMERIC)
    private Short colSpans;
    @Schema(description = "控件名称")
    @TableField(value = "control_name", jdbcType = JdbcType.VARCHAR)
    private String controlName;
    @Schema(description = "控件类型")
    @TableField(value = "control_type", jdbcType = JdbcType.VARCHAR)
    private String controlType;
    @Schema(description = "默认值或表达式")
    @TableField(value = "default_val", jdbcType = JdbcType.VARCHAR)
    private String defaultVal;
    @Schema(description = "审核后可修改")
    @TableField(value = "edit_after_approve", jdbcType = JdbcType.NUMERIC)
    private Short editAfterApprove;
    @Schema(description = "扩展代码")
    @TableField(value = "ext_html", jdbcType = JdbcType.VARCHAR)
    private String extHtml;
    @Schema(description = "其他设置")
    @TableField(value = "ext_settings", jdbcType = JdbcType.VARCHAR)
    private String extSettings;
    @Schema(description = "所属表单")
    @TableField(value = "form_id", jdbcType = JdbcType.VARCHAR)
    private String formId;
    @Schema(description = "隐藏条件")
    @TableField(value = "hide_cond", jdbcType = JdbcType.VARCHAR)
    private String hideCond;
    @Schema(description = "input/dict/expression/auto")
    @TableField(value = "input_source", jdbcType = JdbcType.VARCHAR)
    private String inputSource;
    @Schema(description = "输入来源过滤条件")
    @TableField(value = "source_filter", jdbcType = JdbcType.VARCHAR)
    private String sourceFilter;
    @Schema(description = "是否导出")
    @TableField(value = "is_export", jdbcType = JdbcType.NUMERIC)
    private Short isExport;
    @Schema(description = "是否隐藏")
    @TableField(value = "is_hide", jdbcType = JdbcType.NUMERIC)
    private Short isHide;
    @Schema(description = "是否只读")
    @TableField(value = "is_readonly", jdbcType = JdbcType.NUMERIC)
    private Short isReadonly;
    @Schema(description = "是否必填")
    @TableField(value = "is_required", jdbcType = JdbcType.NUMERIC)
    private Short isRequired;
    @Schema(description = "英文标签")
    @TableField(value = "label_en", jdbcType = JdbcType.VARCHAR)
    private String labelEn;
    @Schema(description = "繁体标签")
    @TableField(value = "label_hk", jdbcType = JdbcType.VARCHAR)
    private String labelHk;
    @Schema(description = "标签文字")
    @TableField(value = "label_text", jdbcType = JdbcType.VARCHAR)
    private String labelText;
    @Schema(description = "占位符")
    @TableField(value = "placeholder", jdbcType = JdbcType.VARCHAR)
    private String placeholder;
    @Schema(description = "只读条件")
    @TableField(value = "readonly_cond", jdbcType = JdbcType.VARCHAR)
    private String readonlyCond;
    @Schema(description = "必填条件")
    @TableField(value = "required_cond", jdbcType = JdbcType.VARCHAR)
    private String requiredCond;
    @Schema(description = "计算字段（SQL表达式）")
    @TableField(value = "sql_expression", jdbcType = JdbcType.VARCHAR)
    private String sqlExpression;
    @Schema(description = "是否子表")
    @TableField(value = "sub_table", jdbcType = JdbcType.NUMERIC)
    private Short subTable;
    @Schema(description = "浮动提示")
    @TableField(value = "title", jdbcType = JdbcType.VARCHAR)
    private String title;
    @Schema(description = "字段最大输入长度")
    @TableField(value = "max_length", jdbcType = JdbcType.INTEGER)
    private Integer maxLength;

    @Schema(description = "公司ID")
    @TableField(value = "tenant_id", jdbcType = JdbcType.VARCHAR)
    private String tenantId;
    @JsonCreator
    public FormControl() {
    }

    @Override
    public String getPkId() {
        return controlId;
    }

    @Override
    public void setPkId(String pkId) {
        this.controlId = pkId;
    }
}