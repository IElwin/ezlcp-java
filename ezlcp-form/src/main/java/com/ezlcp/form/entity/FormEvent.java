package com.ezlcp.form.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ezlcp.commons.base.entity.BaseExtEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

/**
 * <pre>
 *
 * 描述：表单字段控件事件实体类定义
 * 表:f_form_event
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-03-28 10:42:47
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "f_form_event")
@Schema(description = "表单事件")
public class FormEvent extends BaseExtEntity<java.lang.String> {
    @Schema(description = "ID")
    @TableId(value = "id", type = IdType.INPUT)
	private String id;
    @Schema(description = "调用方法")
    @TableField(value = "call_func", jdbcType = JdbcType.VARCHAR)
    private String callFunc;
    @Schema(description = "控件ID")
    @TableField(value = "control_id", jdbcType = JdbcType.VARCHAR)
    private String controlId;
    @Schema(description = "事件性名")
    @TableField(value = "event_name", jdbcType = JdbcType.VARCHAR)
    private String eventName;
    @Schema(description = "表单ID")
    @TableField(value = "fm_id", jdbcType = JdbcType.VARCHAR)
    private String fmId;
    @Schema(description = "是否子表控件")
    @TableField(value = "is_sub_table", jdbcType = JdbcType.NUMERIC)
    private Short isSubTable;
    @Schema(description = "参数列表（,分开）")
    @TableField(value = "parameters", jdbcType = JdbcType.VARCHAR)
    private String parameters;
    @Schema(description = "修改次数")
    @TableField(value = "seq", jdbcType = JdbcType.NUMERIC)
    private Integer seq;

    @JsonCreator
    public FormEvent() {
    }

    @Override
    public String getPkId() {
        return id;
    }

    @Override
    public void setPkId(String pkId) {
        this.id = pkId;
    }
}