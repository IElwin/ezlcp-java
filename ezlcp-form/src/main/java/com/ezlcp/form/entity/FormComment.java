package com.ezlcp.form.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ezlcp.commons.base.entity.BaseExtEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

/**
 * <pre>
 *
 * 描述：表单评论实体类定义
 * 表:f_form_comment
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-03-28 10:42:47
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "f_form_comment")
@Schema(description = "表单评论")
public class FormComment extends BaseExtEntity<java.lang.String> {
    @Schema(description = "ID")
    @TableId(value = "id", type = IdType.INPUT)
	private String id;
    @Schema(description = "附件内容")
    @TableField(value = "attachments", jdbcType = JdbcType.VARCHAR)
    private String attachments;
    @Schema(description = "内容")
    @TableField(value = "content", jdbcType = JdbcType.VARCHAR)
    private String content;
    @Schema(description = "所属表单")
    @TableField(value = "form_id", jdbcType = JdbcType.VARCHAR)
    private String formId;
    @Schema(description = "被回复的ID")
    @TableField(value = "reply_id", jdbcType = JdbcType.VARCHAR)
    private String replyId;
    @Schema(description = "修改次数")
    @TableField(value = "seq", jdbcType = JdbcType.NUMERIC)
    private Integer seq;
    @Schema(description = "0停用1启用4删除")
    @TableField(value = "status", jdbcType = JdbcType.NUMERIC)
    private Short status;

    @JsonCreator
    public FormComment() {
    }

    @Override
    public String getPkId() {
        return id;
    }

    @Override
    public void setPkId(String pkId) {
        this.id = pkId;
    }
}