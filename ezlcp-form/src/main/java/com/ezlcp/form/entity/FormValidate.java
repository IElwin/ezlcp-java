package com.ezlcp.form.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ezlcp.commons.base.entity.BaseExtEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

/**
 * <pre>
 *
 * 描述：表单的自定义校验实体类定义
 * 表:f_form_validate
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-03-28 10:42:47
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "f_form_validate")
@Schema(description = "表单校验")
public class FormValidate extends BaseExtEntity<java.lang.String> {
    @Schema(description = "ID")
    @TableId(value = "id", type = IdType.INPUT)
	private String id;
    @Schema(description = "英文信息")
    @TableField(value = "en_message", jdbcType = JdbcType.VARCHAR)
    private String enMessage;
    @Schema(description = "1前端2后端")
    @TableField(value = "end_type", jdbcType = JdbcType.NUMERIC)
    private Short endType;
    @Schema(description = "所属表单")
    @TableField(value = "form_id", jdbcType = JdbcType.VARCHAR)
    private String formId;
    @Schema(description = "繁体中文信息")
    @TableField(value = "hk_message", jdbcType = JdbcType.VARCHAR)
    private String hkMessage;
    @Schema(description = "失败时是否中断")
    @TableField(value = "is_break", jdbcType = JdbcType.NUMERIC)
    private Short isBreak;
    @Schema(description = "左表达式")
    @TableField(value = "left_expr", jdbcType = JdbcType.VARCHAR)
    private String leftExpr;
    @Schema(description = "提示信息")
    @TableField(value = "message", jdbcType = JdbcType.VARCHAR)
    private String message;
    @Schema(description = "运算符")
    @TableField(value = "operator", jdbcType = JdbcType.VARCHAR)
    private String operator;
    @Schema(description = "前置条件")
    @TableField(value = "pre_cond", jdbcType = JdbcType.VARCHAR)
    private String preCond;
    @Schema(description = "右表达式")
    @TableField(value = "right_expr", jdbcType = JdbcType.VARCHAR)
    private String rightExpr;
    @Schema(description = "修改次数")
    @TableField(value = "seq", jdbcType = JdbcType.NUMERIC)
    private Integer seq;
    @Schema(description = "顺序")
    @TableField(value = "show_order", jdbcType = JdbcType.NUMERIC)
    private Short showOrder;
    @Schema(description = "表名或子表名")
    @TableField(value = "table_name", jdbcType = JdbcType.VARCHAR)
    private String tableName;
    @Schema(description = "场景（1保存前2加载时4审核8反审")
    @TableField(value = "use_at", jdbcType = JdbcType.NUMERIC)
    private Short useAt;

    @JsonCreator
    public FormValidate() {
    }

    @Override
    public String getPkId() {
        return id;
    }

    @Override
    public void setPkId(String pkId) {
        this.id = pkId;
    }
}