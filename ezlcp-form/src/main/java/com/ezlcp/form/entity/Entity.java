package com.ezlcp.form.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ezlcp.commons.base.entity.BaseExtEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

import java.util.List;

/**
 * <pre>
 *
 * 描述：数据库中表的定义实体类定义
 * 表:f_entity
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-03-28 10:42:47
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "f_entity")
@Schema(description = "实体")
public class Entity extends BaseExtEntity<java.lang.String> {
    @Schema(description = "实体ID")
    @TableId(value = "ent_id", type = IdType.INPUT)
	private String entId;
    @Schema(description = "数据源ID")
    @TableField(value = "ds_id", jdbcType = JdbcType.VARCHAR)
    private String dsId;
    @Schema(description = "英文名称")
    @TableField(value = "en_name", jdbcType = JdbcType.VARCHAR)
    private String enName;
    @Schema(description = "实体名")
    @TableField(value = "ent_name", jdbcType = JdbcType.VARCHAR)
    private String entName;
    @Schema(description = "日志生成：1新增2修改4删除")
    @TableField(value = "gen_log", jdbcType = JdbcType.NUMERIC)
    private Short genLog;
    @Schema(description = "繁体中文名称")
    @TableField(value = "hk_name", jdbcType = JdbcType.VARCHAR)
    private String hkName;
    @Schema(description = "1系统内置")
    @TableField(value = "is_build_in", jdbcType = JdbcType.NUMERIC)
    private Short isBuildIn;
    @Schema(description = "是否生成表")
    @TableField(value = "is_gen_db", jdbcType = JdbcType.NUMERIC)
    private Short isGenDb;
    @Schema(description = "仅逻辑删除")
    @TableField(value = "is_logic_del", jdbcType = JdbcType.NUMERIC)
    private Short isLogicDel;
    @Schema(description = "所属模块")
    @TableField(value = "module_id", jdbcType = JdbcType.VARCHAR)
    private String moduleId;
    @Schema(description = "备注")
    @TableField(value = "remark", jdbcType = JdbcType.VARCHAR)
    private String remark;
    @Schema(description = "修改次数")
    @TableField(value = "seq", jdbcType = JdbcType.NUMERIC)
    private Integer seq;
    @Schema(description = "0草稿1发布4作废")
    @TableField(value = "status", jdbcType = JdbcType.NUMERIC)
    private Short status;
    @Schema(description = "物理表名")
    @TableField(value = "table_name", jdbcType = JdbcType.VARCHAR)
    private String tableName;

    @Schema(description = "从数据库反向生成")
    @TableField(value = "is_from_db", jdbcType = JdbcType.NUMERIC)
    private Short isFromDb;
    @Schema(description = "其他信息")
    @TableField(value = "others", jdbcType = JdbcType.VARCHAR)
    private String others;
    @Schema(description = "公司ID")
    @TableField(value = "tenant_id", jdbcType = JdbcType.VARCHAR)
    private String tenantId;

    @Schema(description = "模块名")
    @TableField(exist = false)
    private String moduleName;
    @Schema(description = "模块繁体名")
    @TableField(exist = false)
    private String hkModuleName;
    @TableField(exist = false)
    @Schema(description = "模块英文名")
    private String enModuleName;

    @TableField(exist = false)
    @Schema(description = "数据源名")
    private String dsName;
    @TableField(exist = false)
    @Schema(description = "数据源标识")
    private String identifier;

    @TableField(exist = false)
    @Schema(description = "实体字段列表")
    private List<EntityCol> cols;

    @TableField(exist = false)
    @Schema(description = "实体索引列表")
    private List<EntityIndex> indexes;
    @JsonCreator
    public Entity() {
    }

    @Override
    public String getPkId() {
        return entId;
    }

    @Override
    public void setPkId(String pkId) {
        this.entId = pkId;
    }
}