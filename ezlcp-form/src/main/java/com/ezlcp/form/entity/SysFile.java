package com.ezlcp.form.entity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Elwin ZHANG
 * @description: 上传文件类 <br/>
 * @date 2023/3/21 14:42
 */
@Setter
@Getter
@Schema(description = "文件类")
public class SysFile extends File{

}
