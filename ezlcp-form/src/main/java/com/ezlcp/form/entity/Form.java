package com.ezlcp.form.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ezlcp.commons.base.entity.BaseExtEntity;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 *
 * 描述：表单（主表）定义实体类定义
 * 表:f_form
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-03-28 10:42:47
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "f_form")
@Schema(description = "表单")
public class Form extends BaseExtEntity<java.lang.String> {
    public static final short TYPE_PC = 1;
    public static final short TYPE_H5 = 2;

    @Schema(description = "表单ID")
    @TableId(value = "fm_id", type = IdType.INPUT)
    private String fmId;
    @Schema(description = "按钮定义")
    @TableField(value = "buttons", jdbcType = JdbcType.VARCHAR)
    private String buttons;
    @Schema(description = "页面内容")
    @TableField(value = "content", jdbcType = JdbcType.VARCHAR)
    private String content;
    @Schema(description = "页面临时内容（针对已发布的表单要重新发布后才生效）")
    @TableField(value = "temp_content", jdbcType = JdbcType.VARCHAR)
    private String tempContent;
    @Schema(description = "内容手工修改过")
    @TableField(value = "content_modified", jdbcType = JdbcType.NUMERIC)
    private Short contentModified;
    @Schema(description = "来源ID")
    @TableField(value = "copy_from", jdbcType = JdbcType.VARCHAR)
    private String copyFrom;
    @Schema(description = "修改时限")
    @TableField(value = "edit_limit_minutes", jdbcType = JdbcType.NUMERIC)
    private Integer editLimitMinutes;
    @Schema(description = "英文名称")
    @TableField(value = "en_name", jdbcType = JdbcType.VARCHAR)
    private String enName;
    @Schema(description = "关联实体ID")
    @TableField(value = "ent_id", jdbcType = JdbcType.VARCHAR)
    private String entId;
    @Schema(description = "每行字段数")
    @TableField(value = "fm_cols", jdbcType = JdbcType.NUMERIC)
    private Short fmCols;
    @Schema(description = "表单名（标题）")
    @TableField(value = "fm_name", jdbcType = JdbcType.VARCHAR)
    private String fmName;
    @Schema(description = "1PC 2移动")
    @TableField(value = "fm_type", jdbcType = JdbcType.NUMERIC)
    private Short fmType;
    @Schema(description = "字体大小")
    @TableField(value = "font_size", jdbcType = JdbcType.VARCHAR)
    private String fontSize;
    @Schema(description = "高度")
    @TableField(value = "height", jdbcType = JdbcType.NUMERIC)
    private Short height;
    @Schema(description = "繁体中文名称")
    @TableField(value = "hk_name", jdbcType = JdbcType.VARCHAR)
    private String hkName;
    @Schema(description = "图标")
    @TableField(value = "icon", jdbcType = JdbcType.VARCHAR)
    private String icon;
    @Schema(description = "1系统内置")
    @TableField(value = "is_build_in", jdbcType = JdbcType.NUMERIC)
    private Short isBuildIn;
    @Schema(description = "是否只读")
    @TableField(value = "is_readonly", jdbcType = JdbcType.NUMERIC)
    private Short isReadonly;
    @Schema(description = "0不显示1左对齐2右对齐4换行")
    @TableField(value = "label_style", jdbcType = JdbcType.NUMERIC)
    private Short labelStyle;
    @Schema(description = "标签宽度")
    @TableField(value = "label_width", jdbcType = JdbcType.VARCHAR)
    private String labelWidth;
    @Schema(description = "所属模块")
    @TableField(value = "module_id", jdbcType = JdbcType.VARCHAR)
    private String moduleId;
    @Schema(description = "备注")
    @TableField(value = "remark", jdbcType = JdbcType.VARCHAR)
    private String remark;
    @Schema(description = "字段间隙")
    @TableField(value = "row_gutter", jdbcType = JdbcType.INTEGER)
    private Integer rowGutter;
    @Schema(description = "修改次数")
    @TableField(value = "seq", jdbcType = JdbcType.NUMERIC)
    private Integer seq;
    @Schema(description = "0草稿1发布4作废")
    @TableField(value = "status", jdbcType = JdbcType.NUMERIC)
    private Short status;
    @Schema(description = "子表数量")
    @TableField(value = "sub_tables", jdbcType = JdbcType.NUMERIC)
    private Short subTables;
    @Schema(description = "版本")
    @TableField(value = "ver", jdbcType = JdbcType.NUMERIC)
    private Integer ver;
    @Schema(description = "页宽")
    @TableField(value = "width", jdbcType = JdbcType.NUMERIC)
    private Short width;
    @Schema(description = "公司ID")
    @TableField(value = "tenant_id", jdbcType = JdbcType.VARCHAR)
    private String tenantId;

    @Schema(description = "是否重新生成控件")
    @TableField(exist = false)
    private boolean retrieve;
    @Schema(description = "模块名")
    @TableField(exist = false)
    private String moduleName;
    @Schema(description = "模块繁体名")
    @TableField(exist = false)
    private String hkModuleName;
    @TableField(exist = false)
    @Schema(description = "模块英文名")
    private String enModuleName;
    @Schema(description = "实体名")
    @TableField(exist = false)
    private String entityName;
    @Schema(description = "实体繁体名")
    @TableField(exist = false)
    private String hkEntityName;
    @TableField(exist = false)
    @Schema(description = "实体英文名")
    private String enEntityName;

    @TableField(exist = false)
    @Schema(description = "表单控件列表")
    private List<FormControl> controls;

    @JsonCreator
    public Form() {
    }

    /***
     * @description 是否为PC表单
     */
    public boolean isPC() {
        return fmType == TYPE_PC;
    }

    @Override
    public String getPkId() {
        return fmId;
    }

    @Override
    public void setPkId(String pkId) {
        this.fmId = pkId;
    }
}