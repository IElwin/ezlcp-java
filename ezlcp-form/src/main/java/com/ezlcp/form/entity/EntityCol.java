package com.ezlcp.form.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ezlcp.commons.base.entity.BaseExtEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

/**
 * <pre>
 *
 * 描述：表的列定义实体类定义
 * 表:f_entity_col
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-03-28 10:42:47
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "f_entity_col")
@Schema(description = "实体列")
public class EntityCol extends BaseExtEntity<java.lang.String> {
    @Schema(description = "列ID")
    @TableId(value = "col_id", type = IdType.INPUT)
	private String colId;
    @Schema(description = "可搜索")
    @TableField(value = "can_search", jdbcType = JdbcType.NUMERIC)
    private Short canSearch;
    @Schema(description = "字度长位")
    @TableField(value = "col_length", jdbcType = JdbcType.NUMERIC)
    private Short colLength;
    @Schema(description = "显示顺序")
    @TableField(value = "col_order", jdbcType = JdbcType.NUMERIC)
    private Short colOrder;
    @Schema(description = "小数位长度")
    @TableField(value = "col_prec", jdbcType = JdbcType.NUMERIC)
    private Short colPrec;
    @Schema(description = "字段类型")
    @TableField(value = "col_type", jdbcType = JdbcType.VARCHAR)
    private String colType;
    @Schema(description = "默认值或表达式")
    @TableField(value = "default_val", jdbcType = JdbcType.VARCHAR)
    private String defaultVal;
    @Schema(description = "英文名称")
    @TableField(value = "en_name", jdbcType = JdbcType.VARCHAR)
    private String enName;
    @Schema(description = "所属实体")
    @TableField(value = "ent_id", jdbcType = JdbcType.VARCHAR)
    private String entId;
    @Schema(description = "数据库字段名")
    @TableField(value = "field_name", jdbcType = JdbcType.VARCHAR)
    private String fieldName;
    @Schema(description = "繁体中文名称")
    @TableField(value = "hk_name", jdbcType = JdbcType.VARCHAR)
    private String hkName;
    @Schema(description = "1系统内置")
    @TableField(value = "is_build_in", jdbcType = JdbcType.NUMERIC)
    private Short isBuildIn;
    @Schema(description = "是否隐藏")
    @TableField(value = "is_hide", jdbcType = JdbcType.NUMERIC)
    private Short isHide;
    @Schema(description = "是否主键")
    @TableField(value = "is_pk", jdbcType = JdbcType.NUMERIC)
    private Short isPk;
    @Schema(description = "是否必填")
    @TableField(value = "is_required", jdbcType = JdbcType.NUMERIC)
    private Short isRequired;
    @Schema(description = "是否唯一")
    @TableField(value = "is_unique", jdbcType = JdbcType.NUMERIC)
    private Short isUnique;
    @Schema(description = "重要列")
    @TableField(value = "main_col", jdbcType = JdbcType.NUMERIC)
    private Short mainCol;
    @Schema(description = "附加属性")
    @TableField(value = "others", jdbcType = JdbcType.VARCHAR)
    private String others;
    @Schema(description = "组件类型")
    @TableField(value = "control_type", jdbcType = JdbcType.VARCHAR)
    private String controlType;
    @Schema(description = "引用实体ID")
    @TableField(value = "ref_ent_id", jdbcType = JdbcType.VARCHAR)
    private String refEntId;
    @Schema(description = "引用单据编号ID")
    @TableField(value = "ref_serial_no", jdbcType = JdbcType.VARCHAR)
    private String refSerialNo;
    @Schema(description = "数据字典ID")
    @TableField(value = "dict_id", jdbcType = JdbcType.VARCHAR)
    private String dictId;
    @Schema(description = "备注")
    @TableField(value = "remark", jdbcType = JdbcType.VARCHAR)
    private String remark;
    @Schema(description = "修改次数")
    @TableField(value = "seq", jdbcType = JdbcType.NUMERIC)
    private Integer seq;
    @Schema(description = "列名（显示）")
    @TableField(value = "show_name", jdbcType = JdbcType.VARCHAR)
    private String showName;
    @Schema(description = "公司ID")
    @TableField(value = "tenant_id", jdbcType = JdbcType.VARCHAR)
    private String tenantId;

    @Schema(description = "引用实体名")
    @TableField(exist = false)
    private String refEntName;
    @Schema(description = "引用实体繁体名")
    @TableField(exist = false)
    private String refHkName;
    @Schema(description = "引用实体英文名")
    @TableField(exist = false)
    private String refEnName;

    @Schema(description = "单据编号名称")
    @TableField(exist = false)
    private String refOrderName;
    @Schema(description = "单据编号繁体名")
    @TableField(exist = false)
    private String refHkOrderName;
    @Schema(description = "单据编号英文名")
    @TableField(exist = false)
    private String refEnOrderName;

    @Schema(description = "数据字典名称")
    @TableField(exist = false)
    private String dictName;
    @Schema(description = "数据字典繁体名")
    @TableField(exist = false)
    private String dictHkName;
    @Schema(description = "数据字典英文名")
    @TableField(exist = false)
    private String dictEnName;

    @JsonCreator
    public EntityCol() {
    }

    @Override
    public String getPkId() {
        return colId;
    }

    @Override
    public void setPkId(String pkId) {
        this.colId = pkId;
    }
}