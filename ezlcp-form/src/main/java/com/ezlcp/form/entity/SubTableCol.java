package com.ezlcp.form.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ezlcp.commons.base.entity.BaseExtEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

/**
 * <pre>
 *
 * 描述：表单子表列设置实体类定义
 * 表:f_sub_table_col
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-03-28 10:42:47
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "f_sub_table_col")
@Schema(description = "子表列")
public class SubTableCol extends BaseExtEntity<java.lang.String> {
    @Schema(description = "ID")
    @TableId(value = "id", type = IdType.INPUT)
	private String id;
    @Schema(description = "背景色")
    @TableField(value = "bg_color", jdbcType = JdbcType.VARCHAR)
    private String bgColor;
    @Schema(description = "关联实体列")
    @TableField(value = "col_id", jdbcType = JdbcType.VARCHAR)
    private String colId;
    @Schema(description = "显示顺序")
    @TableField(value = "col_order", jdbcType = JdbcType.NUMERIC)
    private Short colOrder;
    @Schema(description = "控件名称")
    @TableField(value = "control_name", jdbcType = JdbcType.VARCHAR)
    private String controlName;
    @Schema(description = "默认值或表达式")
    @TableField(value = "default_val", jdbcType = JdbcType.VARCHAR)
    private String defaultVal;
    @Schema(description = "审核后可修改")
    @TableField(value = "edit_after_approve", jdbcType = JdbcType.NUMERIC)
    private Short editAfterApprove;
    @Schema(description = "扩展代码")
    @TableField(value = "ext_html", jdbcType = JdbcType.VARCHAR)
    private String extHtml;
    @Schema(description = "其他设置")
    @TableField(value = "ext_settings", jdbcType = JdbcType.VARCHAR)
    private String extSettings;
    @Schema(description = "字体颜色")
    @TableField(value = "font_color", jdbcType = JdbcType.VARCHAR)
    private String fontColor;
    @Schema(description = "字体大小")
    @TableField(value = "font_size", jdbcType = JdbcType.VARCHAR)
    private String fontSize;
    @Schema(description = "所属表单")
    @TableField(value = "form_id", jdbcType = JdbcType.VARCHAR)
    private String formId;
    @Schema(description = "英文列标题")
    @TableField(value = "header_en", jdbcType = JdbcType.VARCHAR)
    private String headerEn;
    @Schema(description = "繁体列标题")
    @TableField(value = "header_hk", jdbcType = JdbcType.VARCHAR)
    private String headerHk;
    @Schema(description = "标签文字")
    @TableField(value = "header_text", jdbcType = JdbcType.VARCHAR)
    private String headerText;
    @Schema(description = "隐藏条件")
    @TableField(value = "hide_cond", jdbcType = JdbcType.VARCHAR)
    private String hideCond;
    @Schema(description = "input/curUserId/curDate/now/calculate")
    @TableField(value = "input_source", jdbcType = JdbcType.VARCHAR)
    private String inputSource;
    @Schema(description = "是否导出")
    @TableField(value = "is_export", jdbcType = JdbcType.NUMERIC)
    private Short isExport;
    @Schema(description = "是否隐藏")
    @TableField(value = "is_hide", jdbcType = JdbcType.NUMERIC)
    private Short isHide;
    @Schema(description = "是否只读")
    @TableField(value = "is_readonly", jdbcType = JdbcType.NUMERIC)
    private Short isReadonly;
    @Schema(description = "是否必填")
    @TableField(value = "is_required", jdbcType = JdbcType.NUMERIC)
    private Short isRequired;
    @Schema(description = "是否汇总")
    @TableField(value = "is_sum", jdbcType = JdbcType.NUMERIC)
    private Short isSum;
    @Schema(description = "父列头")
    @TableField(value = "parent_header_id", jdbcType = JdbcType.VARCHAR)
    private String parentHeaderId;
    @Schema(description = "只读条件")
    @TableField(value = "readonly_cond", jdbcType = JdbcType.VARCHAR)
    private String readonlyCond;
    @Schema(description = "必填条件")
    @TableField(value = "required_cond", jdbcType = JdbcType.VARCHAR)
    private String requiredCond;
    @Schema(description = "修改次数")
    @TableField(value = "seq", jdbcType = JdbcType.NUMERIC)
    private Integer seq;
    @Schema(description = "计算字段（SQL表达式）")
    @TableField(value = "sql_expression", jdbcType = JdbcType.VARCHAR)
    private String sqlExpression;
    @Schema(description = "子表ID")
    @TableField(value = "sub_table_id", jdbcType = JdbcType.VARCHAR)
    private String subTableId;

    @JsonCreator
    public SubTableCol() {
    }

    @Override
    public String getPkId() {
        return id;
    }

    @Override
    public void setPkId(String pkId) {
        this.id = pkId;
    }
}