package com.ezlcp.form.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ezlcp.commons.base.entity.BaseExtEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

/**
 * <pre>
 *
 * 描述：数据列表的查询方案实体类定义
 * 表:f_query_plan
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-03-28 10:42:47
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "f_query_plan")
@Schema(description = "查询方案")
public class QueryPlan extends BaseExtEntity<java.lang.String> {
    @Schema(description = "ID")
    @TableId(value = "id", type = IdType.INPUT)
	private String id;
    @Schema(description = "条件配置")
    @TableField(value = "conditions", jdbcType = JdbcType.VARCHAR)
    private String conditions;
    @Schema(description = "英文名称")
    @TableField(value = "en_name", jdbcType = JdbcType.VARCHAR)
    private String enName;
    @Schema(description = "繁体中文名称")
    @TableField(value = "hk_name", jdbcType = JdbcType.VARCHAR)
    private String hkName;
    @Schema(description = "系统内置")
    @TableField(value = "is_build_in", jdbcType = JdbcType.NUMERIC)
    private Short isBuildIn;
    @Schema(description = "是否默认")
    @TableField(value = "is_default", jdbcType = JdbcType.NUMERIC)
    private Short isDefault;
    @Schema(description = "列表")
    @TableField(value = "list_id", jdbcType = JdbcType.VARCHAR)
    private String listId;
    @Schema(description = "方案名称")
    @TableField(value = "plan_name", jdbcType = JdbcType.VARCHAR)
    private String planName;
    @Schema(description = "修改次数")
    @TableField(value = "seq", jdbcType = JdbcType.NUMERIC)
    private Integer seq;
    @Schema(description = "顺序")
    @TableField(value = "show_order", jdbcType = JdbcType.NUMERIC)
    private Short showOrder;

    @JsonCreator
    public QueryPlan() {
    }

    @Override
    public String getPkId() {
        return id;
    }

    @Override
    public void setPkId(String pkId) {
        this.id = pkId;
    }
}