package com.ezlcp.form.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ezlcp.commons.base.entity.BaseExtEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

/**
 * <pre>
 *
 * 描述：列表或表单子表的多层表头配置实体类定义
 * 表:f_multiple_header
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-03-28 10:42:47
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "f_multiple_header")
@Schema(description = "多表头")
public class MultipleHeader extends BaseExtEntity<java.lang.String> {
    @Schema(description = "ID")
    @TableId(value = "id", type = IdType.INPUT)
	private String id;
    @Schema(description = "英文名称")
    @TableField(value = "en_name", jdbcType = JdbcType.VARCHAR)
    private String enName;
    @Schema(description = "名称")
    @TableField(value = "head_name", jdbcType = JdbcType.VARCHAR)
    private String headName;
    @Schema(description = "繁体中文名称")
    @TableField(value = "hk_name", jdbcType = JdbcType.VARCHAR)
    private String hkName;
    @Schema(description = "是否子表")
    @TableField(value = "is_sub_table", jdbcType = JdbcType.NUMERIC)
    private Short isSubTable;
    @Schema(description = "层级")
    @TableField(value = "level", jdbcType = JdbcType.NUMERIC)
    private Short level;
    @Schema(description = "所属模块")
    @TableField(value = "module_id", jdbcType = JdbcType.VARCHAR)
    private String moduleId;
    @Schema(description = "父表头")
    @TableField(value = "parent_id", jdbcType = JdbcType.VARCHAR)
    private String parentId;
    @Schema(description = "列表或子表ID")
    @TableField(value = "relate_id", jdbcType = JdbcType.VARCHAR)
    private String relateId;
    @Schema(description = "修改次数")
    @TableField(value = "seq", jdbcType = JdbcType.NUMERIC)
    private Integer seq;

    @JsonCreator
    public MultipleHeader() {
    }

    @Override
    public String getPkId() {
        return id;
    }

    @Override
    public void setPkId(String pkId) {
        this.id = pkId;
    }
}