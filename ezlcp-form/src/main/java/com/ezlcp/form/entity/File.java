package com.ezlcp.form.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ezlcp.commons.base.entity.BaseExtEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

/**
 * <pre>
 *
 * 描述：上传的文件统一保存在这张表中实体类定义
 * 表:f_file
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-03-28 10:42:47
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "f_file")
@Schema(description = "文件或附件")
public class File extends BaseExtEntity<java.lang.String> {
    @Schema(description = "ID")
    @TableId(value = "id", type = IdType.INPUT)
	private String id;
    @Schema(description = "文件扩展名")
    @TableField(value = "ext_name", jdbcType = JdbcType.VARCHAR)
    private String extName;
    @Schema(description = "原文件名")
    @TableField(value = "file_name", jdbcType = JdbcType.VARCHAR)
    private String fileName;
    @Schema(description = "媒体类型")
    @TableField(value = "media_type", jdbcType = JdbcType.VARCHAR)
    private String mediaType;
    @Schema(description = "新文件名")
    @TableField(value = "new_name", jdbcType = JdbcType.VARCHAR)
    private String newName;
    @Schema(description = "保存路径")
    @TableField(value = "path", jdbcType = JdbcType.VARCHAR)
    private String path;
    @Schema(description = "备注")
    @TableField(value = "remark", jdbcType = JdbcType.VARCHAR)
    private String remark;
    @Schema(description = "状态")
    @TableField(value = "status", jdbcType = JdbcType.NUMERIC)
    private Short status;
    @Schema(description = "缩略图路径")
    @TableField(value = "thumbnail", jdbcType = JdbcType.VARCHAR)
    private String thumbnail;
    @Schema(description = "文件大小")
    @TableField(value = "total_bytes", jdbcType = JdbcType.NUMERIC)
    private Long totalBytes;

    @TableField(exist = false)
    @Schema(description = "文件内容")
    private byte[] fileContent;
    @TableField(exist = false)
    @Schema(description = "时间戳")
    private String timestamp;

    @JsonCreator
    public File() {
    }

    @Override
    public String getPkId() {
        return id;
    }

    @Override
    public void setPkId(String pkId) {
        this.id = pkId;
    }
}