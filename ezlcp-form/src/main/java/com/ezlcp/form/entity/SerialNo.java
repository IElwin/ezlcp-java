package com.ezlcp.form.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ezlcp.commons.base.entity.BaseExtEntity;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

/**
 * <pre>
 *
 * 描述：單據編號，系统初始化公司ID为空的记录，每个公司创建后从初始记录复制实体类定义
 * 表:s_serial_no
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-01-04 15:18:48
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "f_serial_no")
@Schema(description ="流水号/单号")
public class SerialNo extends BaseExtEntity<java.lang.String> {
    @Schema(description = "主键")
    @TableId(value = "id", type = IdType.INPUT)
	private String id;
    @Schema(description = "标识符")
    @TableField(value = "identifier", jdbcType = JdbcType.VARCHAR)
    private String identifier;
    @Schema(description = "单据名称")
    @TableField(value = "order_name", jdbcType = JdbcType.VARCHAR)
    private String orderName;
    @Schema(description = "英文名称")
    @TableField(value = "en_name", jdbcType = JdbcType.VARCHAR)
    private String enName;
    @Schema(description = "繁体中文名")
    @TableField(value = "hk_name", jdbcType = JdbcType.VARCHAR)
    private String hkName;
    @Schema(description = "规则表达式")
    @TableField(value = "rules", jdbcType = JdbcType.VARCHAR)
    private String rules;
    @Schema(description = "初始值")
    @TableField(value = "init_val", jdbcType = JdbcType.NUMERIC)
    private Short initVal;
    @Schema(description = "步长")
    @TableField(value = "step", jdbcType = JdbcType.NUMERIC)
    private Short step;
    @Schema(description = "当前值")
    @TableField(value = "current_val", jdbcType = JdbcType.NUMERIC)
    private Long currentVal;
    @Schema(description = "数值长度")
    @TableField(value = "no_len", jdbcType = JdbcType.NUMERIC)
    private Short noLen;
    @Schema(description = "0不重置1每天2每月4每年")
    @TableField(value = "reset_type", jdbcType = JdbcType.NUMERIC)
    private Short resetType;
    @Schema(description = "备注")
    @TableField(value = "remark", jdbcType = JdbcType.VARCHAR)
    private String remark;
    @Schema(description = "所属模块")
    @TableField(value = "module_id", jdbcType = JdbcType.VARCHAR)
    private String moduleId;
    @Schema(description = "修改次数")
    @TableField(value = "seq", jdbcType = JdbcType.NUMERIC)
    private Integer seq;
    @Schema(description = "公司ID")
    @TableField(value = "tenant_id", jdbcType = JdbcType.VARCHAR)
    private String tenantId;
    @Schema(description = "状态")
    @TableField(value = "status", jdbcType = JdbcType.SMALLINT)
    private Short status;
    @Schema(description = "模块名")
    @TableField(exist = false)
    private String moduleName;
    @Schema(description = "模块繁体名")
    @TableField(exist = false)
    private String hkModuleName;
    @TableField(exist = false)
    @Schema(description = "模块英文名")
    private String enModuleName;
    @JsonCreator
    public SerialNo() {
    }

    @Override
    public String getPkId() {
        return id;
    }

    @Override
    public void setPkId(String pkId) {
        this.id = pkId;
    }
}