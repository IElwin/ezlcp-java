package com.ezlcp.form;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author Elwin ZHANG
 * @description: 表单模块启动<br/>
 * @date 2022/12/26 10:51
 */
@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = {"com.ezlcp"})
@EnableFeignClients(basePackages = {"com.ezlcp"})
@EnableScheduling
public class EzlcpFormApplication {
    public static void main(String[] args) {
        SpringApplication.run(EzlcpFormApplication.class, args);
    }
}
