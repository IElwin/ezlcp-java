package com.ezlcp.user.controller;

import com.alibaba.cloud.commons.lang.StringUtils;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.exception.NacosException;
import com.ezlcp.commons.base.entity.JsonResult;
import com.ezlcp.commons.dto.LoginLog;
import com.ezlcp.commons.listener.NacosDataSourceListener;
import com.ezlcp.commons.model.SysUser;
import com.ezlcp.commons.utils.DruidUtils;
import com.ezlcp.commons.utils.NacosUtils;
import com.ezlcp.commons.utils.TokenUtil;
import com.ezlcp.user.service.LogService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;


/**
 * @author Elwin ZHANG
 * @description: 测试控制器<br />
 * @date 2022/12/16 17:24
 */
@RestController
@RefreshScope
@RequestMapping("/ezlcp/user/test")
@Tag(name = "测试控制器", description = "你懂的！")
public class TestController {
    //nacos中的配置项，在user-*.properties中
    @Value("${default_lang:zh_CN}")
    private String defaultLang;

    @Lazy
    @Resource
    private NacosUtils nacosUtils;

    @Resource
    private LogService logService;

    /***
     * @description 返回默认语言的配置
     * @return java.lang.String
     * @author Elwin ZHANG
     * @date 2022/12/16 17:45
     */
    @GetMapping("/defaultLang")
    public String get() {
        return defaultLang;
    }

    @Operation(summary = "你好", description = "如果不传入名称，默认显示一个名字")
    @Parameter(name = "name", description = "名字，默认为Obama")
    @GetMapping("/hello")
    public Object hello(@RequestParam(required = false) String name) {
        //无参数
        if (StringUtils.isEmpty(name)) {
            return "Hello,Obama!";
        }
        return "Hello," + name;
    }

    @GetMapping("/nacos/get")
    public Object nacosGet() throws NacosException {
        ConfigService service = nacosUtils.getConfigService("");
        String result = service.getConfig("abc.test", "", 2000);
        nacosUtils.closeService(service);
        return result;
    }

    @GetMapping("/nacos/set")
    public Object nacosSet() throws NacosException {
        ConfigService service = nacosUtils.getConfigService("");
        String content = "中文abc" + Math.random();
        service.publishConfig("abc.test", "", content);
        nacosUtils.closeService(service);
        return content;
    }

    @GetMapping("/nacos/del")
    public Object nacosDel() throws NacosException {
        ConfigService service = nacosUtils.getConfigService("");
        boolean result = service.removeConfig("abc.test", "");
        nacosUtils.closeService(service);
        return result;
    }

    @GetMapping("/redis")
    public Object redis() {
        SysUser user = new SysUser();
        user.setAccount("ttt");
        user.setUserId("222");
        user.setFullName("elwin Lee");
        TokenUtil.newTokenAndCache(user);
        return JsonResult.Success("redis写入成功");
    }

    @Operation(summary = "测试异常")
    @GetMapping("exception")
    public Object exception() {
        int j = 0;
        return 5 / j;
    }

    @Operation(summary = "测试数据源")
    @GetMapping("datasource")
    public JsonResult datasource() {
        var ds = NacosDataSourceListener.getDataSource("1664472893795082241");
        try {
            JdbcTemplate jdbcTemplate = new JdbcTemplate(ds);
            String sql = "SELECT * FROM user WHERE id>198";
            var result = jdbcTemplate.queryForList(sql);
            var name = result.get(0).get("name");
            var result2 = JsonResult.Success(name.toString());
            result2.setData(ds.getName());
            return result2;
        } catch (Exception e) {
            e.printStackTrace();
            return JsonResult.Fail(e.getMessage());
        }
    }

    @Operation(summary = "测试默认数据源")
    @GetMapping("defaultDatasource")
    public JsonResult defaultDatasource() {
        var ds = DruidUtils.getDataSource("default");
        JdbcTemplate jdbcTemplate = new JdbcTemplate(ds);
        String sql = "SELECT 1";
        var value = jdbcTemplate.queryForObject(sql, String.class);
        var result = JsonResult.Success(value);
        result.setData(ds.getName());
        return result;
    }

    @Operation(summary = "登录日志写入", description = "测试feign调用")
    @PostMapping("log")
    public Object loginLog(@RequestBody LoginLog log) {
        logService.saveLoginLog(log);
        return "成功！";
    }
}
