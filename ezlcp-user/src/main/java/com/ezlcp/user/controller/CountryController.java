
package com.ezlcp.user.controller;

import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.base.entity.JsonResult;
import com.ezlcp.user.entity.Country;
import com.ezlcp.user.service.CountryServiceImpl;
import com.ezlcp.user.web.BaseController;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/ezlcp/user/country")
@Tag(name = "国家")
public class CountryController extends BaseController<Country> {
    @Autowired
    CountryServiceImpl countryService;

    @Override
    public BaseService getBaseService() {
        return countryService;
    }

    @Override
    public String getComment() {
        return "国家";
    }

    @Override
    public JsonResult del(String ids) {
        return JsonResult.Fail("接口已关闭");
    }

    @Override
    public JsonResult save(Country entity, BindingResult validResult) throws Exception {
        return JsonResult.Fail("接口已关闭");
    }
}