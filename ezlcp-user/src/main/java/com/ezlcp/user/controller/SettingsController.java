
package com.ezlcp.user.controller;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson2.JSON;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.base.entity.JsonPageResult;
import com.ezlcp.commons.base.entity.JsonResult;
import com.ezlcp.commons.base.search.QueryFilter;
import com.ezlcp.commons.tool.StringUtils;
import com.ezlcp.commons.utils.ContextUtil;
import com.ezlcp.user.entity.Settings;
import com.ezlcp.user.service.SettingsServiceImpl;
import com.ezlcp.user.web.BaseController;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import jakarta.annotation.Resource;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/ezlcp/user/settings")
@Tag(name = "租户配置")
public class SettingsController extends BaseController<Settings> {
    @Resource
    SettingsServiceImpl settingsService;


    @Operation(summary = "根據公司tenant_id查詢系统预设值")
    @PostMapping("selectByTenantId")
    public JsonResult selectByTenantId(@Parameter(description= "tenantId") @RequestParam(value = "tenantId",required = false) String tenantId) {
        if (StringUtils.isEmpty(tenantId)) {
            return JsonResult.getFailResult("company.tenantIdNotNull");
        }
        Settings Settings = settingsService.selectByTenantId(tenantId);
        JsonResult result = JsonResult.Success("common.returnDataSuccess");
        result.setData(Settings);
        return result;
    }

    @Operation(summary="a.获取公司系统预设值")
    @GetMapping(value = "getSettingsTenant")
    public JsonResult getSettingsTenant() {
        JsonResult result = JsonResult.Success("common.handleSuccess");
        Settings Settings = settingsService.getCurSetting();
        result.setData(Settings);
        return result;
    }

    @Operation(summary = "修改系统预设值")
    @PutMapping("/updateSettings")
    public JsonResult updateSettings(@Parameter(description= "系统预设值") @RequestBody Settings Settings) {
        if (ObjectUtil.isEmpty(Settings) || StringUtils.isEmpty(Settings.getId())) {
            return JsonResult.Fail("common.paramError");
        }
        Settings oldSettings = settingsService.getById(Settings.getId());
        if (StringUtils.compare(oldSettings.getTenantId(),ContextUtil.getCurrentTenantId())!=0) {
            return JsonResult.Fail("Settings.noOperate");
        }
        Settings.setSeq(oldSettings.getSeq() + 1);
        Boolean updateStatus = settingsService.updateSettings(Settings);
        String detail = "old:" + JSON.toJSONString(oldSettings) + ",new:" + JSON.toJSONString(Settings);
        logService.saveSystemLog(getComment(), "updateSettings", Settings.getPkId(), JSON.toJSONString(detail));
        return updateStatus ? JsonResult.Success("common.handleSuccess") : JsonResult.Fail();
    }

    @Override
    @Operation(summary = "【已关闭】根据主键ID删除记录", description = "根据主键ID删除记录,parameters is {ids:'1,2'}")
    public JsonResult del(String ids) {
        return null;
    }

    @Override
    @Operation(summary = "【已关闭】查询当前业务表的所有数据", description = "查询当前实体表的所有数据。")
    public JsonPageResult getAll() {
        return null;
    }

    @Override
    @Operation(summary = "【已关闭】保存业务数据记录", description = "根据提交的业务JSON数据保存业务数据记录")
    public JsonResult save(Settings entity, BindingResult validResult) {
        return null;
    }

    @Override
    @Operation(summary = "【已关闭】根据主键IDS查询业务数据详细信息", description = "根据主键IDS查询业务数据详细信息")
    public JsonResult<Settings> getByIds(String ids) {
        return null;
    }
    
    @Override
    public BaseService getBaseService() {
        return settingsService;
    }

    @Override
    public String getComment() {
        return "租户配置";
    }
}