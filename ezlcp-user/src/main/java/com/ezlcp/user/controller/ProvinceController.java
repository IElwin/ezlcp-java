
package com.ezlcp.user.controller;

import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.base.entity.JsonResult;
import com.ezlcp.user.entity.Country;
import com.ezlcp.user.entity.Province;
import com.ezlcp.user.service.ProvinceServiceImpl;
import com.ezlcp.user.web.BaseController;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/ezlcp/user/province")
@Tag(name = "省份")
public class ProvinceController extends BaseController<Province> {
    @Autowired
    ProvinceServiceImpl provinceService;

    @Override
    public BaseService getBaseService() {
        return provinceService;
    }

    @Override
    public String getComment() {
        return "省份";
    }

    @Override
    public JsonResult del(String ids) {
        return JsonResult.Fail("接口已关闭");
    }
    @Override
    public JsonResult save(Province entity, BindingResult validResult) throws Exception {
        return JsonResult.Fail("接口已关闭");
    }
}