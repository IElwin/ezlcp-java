package com.ezlcp.user.controller;

import com.alibaba.fastjson2.JSONObject;
import com.ezlcp.commons.base.entity.JsonResult;
import com.ezlcp.commons.tool.ChineseUtil;
import com.ezlcp.commons.tool.StringUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author Elwin ZHANG
 * @description: 工具类接口<br />
 * @date 2023/3/20 17:23
 */
@RestController
@RequestMapping("/ezlcp/user/tools")
@Tag(name = "工具类接口")
public class ToolsController {

    @Operation(summary = "将中文字符串转换为繁体中文")
    @PostMapping("/toZhHK")
    public JsonResult toZhHK(@Parameter(description = "中文字符串") @RequestParam(value = "text") String text) {
        JsonResult result = JsonResult.Success("common.handleSuccess");
        String data = "";
        if (StringUtils.isNotEmpty(text)) {
            data = ChineseUtil.toTraditional(text);
        }
        result.setData(data);
        return result;
    }

    @Operation(summary = "将中文字符串转换为简体中文")
    @PostMapping("/toZhCN")
    public JsonResult toZhCN(@Parameter(description = "中文字符串") @RequestParam(value = "text") String text) {
        JsonResult result = JsonResult.Success("common.handleSuccess");
        String data = "";
        if (StringUtils.isNotEmpty(text)) {
            data = ChineseUtil.toSimplified(text);
        }
        result.setData(data);
        return result;
    }

    @Operation(summary = "获取中文字符串的拼音首字母")
    @PostMapping("/getFirstLetters")
    public JsonResult getFirstLetters(@Parameter(description = "中文字符串") @RequestParam(value = "text") String text) {
        JsonResult result = JsonResult.Success("common.handleSuccess");
        String data = "";
        if (StringUtils.isNotEmpty(text)) {
            data = ChineseUtil.toFirstLetters(text);
        }
        result.setData(data);
        return result;
    }

    @Operation(summary = "获取中文字符串的拼音格式，默认用空格分隔")
    @PostMapping("/getPinyin")
    public JsonResult getPinyin(@Parameter(description = "中文字符串") @RequestParam(value = "text") String text,
                                @Parameter(description = "分隔符，默认空格") @RequestParam(value = "split", required = false) String split,
                                @Parameter(description = "最大字符串长度，默认50") @RequestParam(value = "maxLength", required = false) Integer maxLength) {
        JsonResult result = JsonResult.Success("common.handleSuccess");
        if (maxLength == null) {
            maxLength = 50;
        }
        String pinyin = "";
        if (StringUtils.isNotEmpty(text)) {
            pinyin = ChineseUtil.toPinyinNoTone(text, split);
            //长度限制
            if (pinyin.length() > maxLength) {
                pinyin = pinyin.substring(0, maxLength);
            }
        }
        result.setData(pinyin);
        return result;
    }

    @Operation(summary = "获取简体中文以及中文拼音，拼音默认用空格分隔")
    @PostMapping("/getZhCNAndPinyin")
    public JsonResult getZhCNAndPinyin(@Parameter(description = "中文字符串") @RequestParam(value = "text") String text,
                                       @Parameter(description = "分隔符，默认空格") @RequestParam(value = "split", required = false) String split,
                                       @Parameter(description = "最大字符串长度，默认50") @RequestParam(value = "maxLength", required = false) Integer maxLength) {
        JsonResult result = JsonResult.Success("common.handleSuccess");
        if (maxLength == null) {
            maxLength = 50;
        }
        JSONObject data = new JSONObject();
        if (StringUtils.isNotEmpty(text)) {
            String zhCN = ChineseUtil.toSimplified(text);
            String pinyin = ChineseUtil.toPinyinNoTone(text, split);
            //长度限制
            if (pinyin.length() > maxLength) {
                pinyin = pinyin.substring(0, maxLength);
            }
            if (zhCN.length() > maxLength) {
                zhCN = zhCN.substring(0, maxLength);
            }
            data.put("pinyin", pinyin);
            data.put("zhCN", zhCN);
        }
        result.setData(data);
        return result;
    }

    @Operation(summary = "获取繁体中文以及中文拼音，拼音默认用空格分隔")
    @PostMapping("/getZhHKAndPinyin")
    public JsonResult getZhHKAndPinyin(@Parameter(description = "中文字符串") @RequestParam(value = "text") String text,
                                       @Parameter(description = "分隔符，默认空格") @RequestParam(value = "split", required = false) String split,
                                       @Parameter(description = "最大字符串长度，默认50") @RequestParam(value = "maxLength", required = false) Integer maxLength) {
        JsonResult result = JsonResult.Success("common.handleSuccess");
        if (maxLength == null) {
            maxLength = 50;
        }
        JSONObject data = new JSONObject();
        if (StringUtils.isNotEmpty(text)) {
            String pinyin = ChineseUtil.toPinyinNoTone(text, split);
            String zhHK = ChineseUtil.toTraditional(text);
            //长度限制
            if (pinyin.length() > maxLength) {
                pinyin = pinyin.substring(0, maxLength);
            }
            if (zhHK.length() > maxLength) {
                zhHK = zhHK.substring(0, maxLength);
            }
            data.put("pinyin", pinyin);
            data.put("zhHK", zhHK);
        }
        result.setData(data);
        return result;
    }
}
