
package com.ezlcp.user.controller;

import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.base.entity.JsonResult;
import com.ezlcp.user.entity.District;
import com.ezlcp.user.service.DistrictServiceImpl;
import com.ezlcp.user.web.BaseController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/ezlcp/user/district")
@Tag(name = "区/县")
public class DistrictController extends BaseController<District> {
    @Autowired
    DistrictServiceImpl districtService;

    @Operation(summary = "查找某城市下的区和县")
    @PostMapping("/getByCity")
    public JsonResult getByCity(@Parameter(description = "城市ID") @RequestParam String cityId){
        JsonResult result = JsonResult.Success("common.delSuccess");
        var data=districtService.getByCity(cityId);
        result.setData(data);
        return result;
    }
    @Override
    public JsonResult del(String ids) {
        return JsonResult.Fail("接口已关闭");
    }

    @Override
    public JsonResult save(District entity, BindingResult validResult) throws Exception {
        return JsonResult.Fail("接口已关闭");
    }

    @Override
    public BaseService getBaseService() {
        return districtService;
    }

    @Override
    public String getComment() {
        return "区/县";
    }
}