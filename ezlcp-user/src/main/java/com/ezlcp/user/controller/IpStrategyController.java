package com.ezlcp.user.controller;

import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.base.entity.JsonPageResult;
import com.ezlcp.commons.base.entity.JsonResult;
import com.ezlcp.commons.tool.StringUtils;
import com.ezlcp.commons.utils.ContextUtil;
import com.ezlcp.user.entity.IpStrategy;
import com.ezlcp.user.service.IpStrategyServiceImpl;
import com.ezlcp.user.web.BaseController;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/ezlcp/user/ipStrategy")
@Tag(name = "IP黑名单或白名单控制")
public class IpStrategyController extends BaseController<IpStrategy> {
    @Resource
    IpStrategyServiceImpl ipStrategyService;

    @Override
    public BaseService getBaseService() {
        return ipStrategyService;
    }

    @Override
    @PostMapping("/save")
    public JsonResult save(@RequestBody IpStrategy entity, BindingResult validResult) throws Exception {
        if (entity == null) {
            return JsonResult.Fail("common.paramError");
        }
        if(StringUtils.isEmpty(entity.getId())){
            entity.setTenantId(ContextUtil.getCurrentTenantId());
        }
        return super.save(entity, validResult);
    }

    @Override
    public JsonPageResult getAll() throws Exception {
        return JsonPageResult.getFail("common.apiClose");
    }

    @Override
    public String getComment() {
        return "IP黑名单或白名单控制";
    }
}