
package com.ezlcp.user.controller;

import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.base.entity.JsonResult;
import com.ezlcp.user.entity.City;
import com.ezlcp.user.service.CityServiceImpl;
import com.ezlcp.user.web.BaseController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/ezlcp/user/city")
@Tag(name = "城市")
public class CityController extends BaseController<City> {
    @Autowired
    CityServiceImpl cityService;

    @Override
    public BaseService getBaseService() {
        return cityService;
    }


    @Operation(summary = "查找某省的所有城市")
    @PostMapping("/getByProvince")
    public JsonResult getByProvince(@Parameter(description = "省ID") @RequestParam String provinceId){
        JsonResult result = JsonResult.Success("common.delSuccess");
        var data=cityService.getByProvince(provinceId);
        result.setData(data);
        return result;
    }

    @Override
    public JsonResult del(String ids) {
        return JsonResult.Fail("接口已关闭");
    }

    @Override
    public JsonResult save(City entity, BindingResult validResult) throws Exception {
        return JsonResult.Fail("接口已关闭");
    }

    @Override
    public String getComment() {
        return "城市";
    }

}