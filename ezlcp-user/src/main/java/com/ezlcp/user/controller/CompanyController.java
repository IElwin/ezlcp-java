
package com.ezlcp.user.controller;

import com.alibaba.fastjson2.JSON;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.base.entity.JsonPageResult;
import com.ezlcp.commons.base.entity.JsonResult;
import com.ezlcp.commons.base.search.QueryFilter;
import com.ezlcp.commons.utils.ContextUtil;
import com.ezlcp.user.entity.Company;
import com.ezlcp.user.entity.User;
import com.ezlcp.commons.constant.StatusEnum;
import com.ezlcp.user.service.CompanyServiceImpl;
import com.ezlcp.user.service.UserServiceImpl;
import com.ezlcp.user.web.BaseController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/ezlcp/user/company")
@Tag(name = "即租户信息")
public class CompanyController extends BaseController<Company> {
    @Autowired
    CompanyServiceImpl companyService;

    @Lazy
    @Resource
    UserServiceImpl userService;

    @Override
    protected void handleFilter(QueryFilter filter) {
        String tenantId = ContextUtil.getCurrentTenantId();
        if (StringUtils.isNotEmpty(tenantId)) {
            filter.addQueryParam("Q_c.tenant_id_S_EQ", tenantId);
        }
    }

    @Operation(summary = "修改公司资料")
    @PostMapping("updateCompany")
    public JsonResult updateCompany(@RequestBody Company company) {
        if (StringUtils.isEmpty(company.getName())) {
            return JsonResult.getFailResult("company.inputNameChi");
        }
        if (StringUtils.isEmpty(company.getEnName())) {
            return JsonResult.getFailResult("company.inputNameEn");
        }
        if (StringUtils.isEmpty(company.getLicenseNo())) {
            return JsonResult.getFailResult("company.inputOrderNo");
        }
        Company oldCompany = companyService.getById(company.getTenantId());
        if (!oldCompany.getSeq().equals(company.getSeq())) {
            return JsonResult.Fail("common.dataChange");
        }
        company.setSeq(oldCompany.getSeq() + 1);
        User user = userService.getById(company.getUserId());
        user.setName(company.getName());
        user.setSeq(user.getSeq() + 1);
        userService.updateById(user);
        //记录日志
        String detail = "old:" + JSON.toJSONString(oldCompany) + ",new:" + JSON.toJSONString(company);
        boolean updateStatus = companyService.updateById(company);
        logService.saveSystemLog(getComment(), "updateCompany", company.getPkId(), detail);
        return updateStatus ? JsonResult.Success("common.updSuccess") : JsonResult.Fail("common.updFail");
    }

    @Override
    @Operation(summary = "根据主键ID删除记录", description = "根据主键ID删除记录")
    @PostMapping("del")
    public JsonResult del(@Parameter(description= "租户ID") @RequestParam(value = "tenantId") String tenantId) {
        if (StringUtils.isEmpty(tenantId)) {
            return JsonResult.getFailResult("company.tenantIdNotNull");
        }
        Company company = companyService.getById(tenantId);
        company.setStatus(StatusEnum.deleted.name());
        company.setSeq(company.getSeq() + 1);
        companyService.update(company);
        User user = userService.getById(company.getUserId());
        user.setStatus(StatusEnum.deleted.name());
        user.setSeq(user.getSeq() + 1);
        userService.update(user);
        logService.saveSystemLog(getComment(), "delete", company.getPkId(), null);
        logService.saveSystemLog(getComment(), "delete", user.getPkId(), null);
        return JsonResult.Success("common.delSuccess");
    }

    @Operation(summary = "根據公司tenant_id查詢")
    @PostMapping("selectByTenantId")
    public JsonResult selectByTenantId(@Parameter(description= "租户ID") @RequestParam(value = "tenantId") String tenantId) {
        if (StringUtils.isEmpty(tenantId)) {
            return JsonResult.getFailResult("company.tenantIdNotNull");
        }
        return JsonResult.getSuccessResult(companyService.selectByTenantId(tenantId), "common.returnDataSuccess");
    }

    @Override
    @Operation(summary = "【已关闭】查询当前业务表的所有数据", description = "查询当前实体表的所有数据。")
    public JsonPageResult getAll() {
        return null;
    }

    @Override
    @Operation(summary = "【已关闭】保存业务数据记录", description = "根据提交的业务JSON数据保存业务数据记录")
    public JsonResult save(Company entity, BindingResult validResult) {
        return null;
    }

    @Override
    @Operation(summary = "【已关闭】根据主键IDS查询业务数据详细信息", description = "根据主键IDS查询业务数据详细信息")
    public JsonResult<Company> getByIds(String ids) {
        return null;
    }

    @Override
    public BaseService getBaseService() {
        return companyService;
    }

    @Override
    public String getComment() {
        return "即租户信息";
    }
}