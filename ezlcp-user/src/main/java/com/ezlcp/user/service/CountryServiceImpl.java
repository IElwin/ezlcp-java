
package com.ezlcp.user.service;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.service.impl.SuperServiceImpl;
import com.ezlcp.user.entity.Country;
import com.ezlcp.user.mapper.CountryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

 

/**
 * [s_country]业务服务类
 */
@Service
public class CountryServiceImpl extends SuperServiceImpl<CountryMapper, Country> implements BaseService<Country> {
    @Autowired
    private CountryMapper countryMapper;

    @Override
    public BaseDao<Country> getRepository() {
        return countryMapper;
    }
}