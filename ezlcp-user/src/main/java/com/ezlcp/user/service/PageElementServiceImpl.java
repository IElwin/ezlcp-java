
package com.ezlcp.user.service;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.service.impl.SuperServiceImpl;
import com.ezlcp.user.entity.PageElement;
import com.ezlcp.user.mapper.PageElementMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

 

/**
 * [頁面功能表]业务服务类
 */
@Service
public class PageElementServiceImpl extends SuperServiceImpl<PageElementMapper, PageElement> implements BaseService<PageElement> {
    @Autowired
    private PageElementMapper pageElementMapper;

    @Override
    public BaseDao<PageElement> getRepository() {
        return pageElementMapper;
    }
}