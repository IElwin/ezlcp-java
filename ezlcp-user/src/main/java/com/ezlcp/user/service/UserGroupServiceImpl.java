
package com.ezlcp.user.service;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.service.impl.SuperServiceImpl;
import com.ezlcp.user.entity.UserGroup;
import com.ezlcp.user.mapper.UserGroupMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

 

/**
 * [用户与角色关联表，多对多]业务服务类
 */
@Service
public class UserGroupServiceImpl extends SuperServiceImpl<UserGroupMapper, UserGroup> implements BaseService<UserGroup> {
    @Autowired
    private UserGroupMapper userGroupMapper;

    @Override
    public BaseDao<UserGroup> getRepository() {
        return userGroupMapper;
    }
}