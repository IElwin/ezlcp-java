
package com.ezlcp.user.service;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.service.impl.SuperServiceImpl;
import com.ezlcp.user.entity.GroupMenu;
import com.ezlcp.user.mapper.GroupMenuMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

 

/**
 * [群组关联菜单及元素，多对多]业务服务类
 */
@Service
public class GroupMenuServiceImpl extends SuperServiceImpl<GroupMenuMapper, GroupMenu> implements BaseService<GroupMenu> {
    @Autowired
    private GroupMenuMapper groupMenuMapper;

    @Override
    public BaseDao<GroupMenu> getRepository() {
        return groupMenuMapper;
    }
}