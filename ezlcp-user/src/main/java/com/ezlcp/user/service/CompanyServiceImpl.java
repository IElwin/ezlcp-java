
package com.ezlcp.user.service;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.service.impl.SuperServiceImpl;
import com.ezlcp.user.entity.Company;
import com.ezlcp.user.mapper.CompanyMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

 

/**
 * [即租户信息]业务服务类
 */
@Service
public class CompanyServiceImpl extends SuperServiceImpl<CompanyMapper, Company> implements BaseService<Company> {
    @Autowired
    private CompanyMapper companyMapper;

    /**
     * @param tenantId 公司ID
     * @return 所需公司信息
     * @description: 根據公司tenant_id查詢
     * @author ou zhen
     * @date 2022/5/9 10:12
     */
    public Company selectByTenantId(String tenantId) {
        return companyMapper.selectByTenantId(tenantId);
    }

    @Override
    public BaseDao<Company> getRepository() {
        return companyMapper;
    }
}