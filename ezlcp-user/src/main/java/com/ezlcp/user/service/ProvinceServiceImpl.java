
package com.ezlcp.user.service;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.service.impl.SuperServiceImpl;
import com.ezlcp.user.entity.Province;
import com.ezlcp.user.mapper.ProvinceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

 

/**
 * [s_province]业务服务类
 */
@Service
public class ProvinceServiceImpl extends SuperServiceImpl<ProvinceMapper, Province> implements BaseService<Province> {
    @Autowired
    private ProvinceMapper provinceMapper;

    @Override
    public BaseDao<Province> getRepository() {
        return provinceMapper;
    }
}