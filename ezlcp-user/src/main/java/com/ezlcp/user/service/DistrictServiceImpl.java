
package com.ezlcp.user.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.service.impl.SuperServiceImpl;
import com.ezlcp.user.entity.City;
import com.ezlcp.user.entity.District;
import com.ezlcp.user.mapper.DistrictMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * [s_district]业务服务类
 */
@Service
public class DistrictServiceImpl extends SuperServiceImpl<DistrictMapper, District> implements BaseService<District> {
    @Autowired
    private DistrictMapper districtMapper;

    @Override
    public BaseDao<District> getRepository() {
        return districtMapper;
    }


    /***
     * @description  查找某个城市下面的区或县
     * @param city 城市ID
     * @return java.util.List<com.ezlcp.user.entity.District>
     * @author Elwin ZHANG
     * @date 2023/2/8 11:57
     */
    public List<District> getByCity(String city){
        QueryWrapper wrapper=new QueryWrapper();
        wrapper.eq("city",city);
        return districtMapper.selectList(wrapper);
    }
}