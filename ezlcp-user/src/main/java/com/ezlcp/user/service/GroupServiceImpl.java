package com.ezlcp.user.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.constant.GroupTypeEnum;
import com.ezlcp.commons.constant.StatusEnum;
import com.ezlcp.commons.service.impl.SuperServiceImpl;
import com.ezlcp.commons.tool.IdGenerator;
import com.ezlcp.commons.tool.StringUtils;
import com.ezlcp.commons.utils.ContextUtil;
import static com.ezlcp.commons.constant.Constants.*;
import com.ezlcp.user.entity.Group;
import com.ezlcp.user.entity.UserGroup;
import com.ezlcp.user.mapper.GroupMapper;
import com.ezlcp.user.mapper.UserGroupMapper;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * [用户组、群组、部门、分公司等人员集合]业务服务类
 */
@Service
public class GroupServiceImpl extends SuperServiceImpl<GroupMapper, Group> implements BaseService<Group> {
    @Resource
    private GroupMapper groupMapper;
    @Resource
    private UserGroupMapper userGroupMapper;

    /**
     * @description: 覆盖基类方法
     * @author Elwin ZHANG
     * @date 2022/6/7 13:52
     */
    @Override
    public List<Group> getAll() {
        QueryWrapper<Group> wrapper = new QueryWrapper<>();
        String tenantId = ContextUtil.getCurrentTenantId();
        if (StringUtils.isEmpty(tenantId)) {
            wrapper.and(query -> query.eq(COL_TENANT_ID, "").or().isNull(COL_TENANT_ID));
        } else {
            wrapper.eq(COL_TENANT_ID, tenantId);
        }
        wrapper.eq(COL_STATUS, StatusEnum.enable.getCode());
        return groupMapper.selectList(wrapper);
    }

    /***
     * @description 查询本租户相关的用户组
     * @param groupId 用户组ID
     * @return com.ezlcp.user.entity.Group
     * @author Elwin ZHANG
     * @date 2023/3/16 17:03
     */
    public Group getByValidId(String groupId) {
        QueryWrapper<Group> wrapper = new QueryWrapper<>();
        wrapper.eq(COL_GROUP_ID, groupId);
        String tenantId = ContextUtil.getCurrentTenantId();
        if (StringUtils.isEmpty(tenantId)) {
            wrapper.and(query -> query.eq(COL_TENANT_ID, "").or().isNull(COL_TENANT_ID));
        } else {
            wrapper.eq(COL_TENANT_ID, tenantId);
        }
        wrapper.eq(COL_STATUS, StatusEnum.enable.getCode());
        return groupMapper.selectOne(wrapper);
    }

    /***
     * @description 根据当前组的类型返回本企业可选择的父节点
     * @param groupId 当前组ID
     * @param groupType 组类型
     * @return java.util.List<com.ezlcp.user.entity.Group>
     * @author Elwin ZHANG
     * @date 2023/3/17 13:44
     */
    public List<Group> selectParentGroups(String groupId, GroupTypeEnum groupType) {
        QueryWrapper<Group> wrapper = new QueryWrapper<>();
        String tenantId = ContextUtil.getCurrentTenantId();
        if (StringUtils.isEmpty(tenantId)) {
            wrapper.and(query -> query.eq(COL_TENANT_ID, "").or().isNull(COL_TENANT_ID));
        } else {
            wrapper.eq(COL_TENANT_ID, tenantId);
        }

        //用户组/角色/分公司的父节点只能是同类型的其他记录
        if (GroupTypeEnum.UserGroup.equals(groupType) || GroupTypeEnum.Role.equals(groupType) ||GroupTypeEnum.Branch.equals(groupType) ) {
            wrapper.eq(COL_GROUP_TYPE, groupType.getId());
        }else if(GroupTypeEnum.Department.equals(groupType) ){
            //部门的父节点可以为分公司
            wrapper.in(COL_GROUP_TYPE,GroupTypeEnum.Branch.getId(),GroupTypeEnum.Department.getId());
        }else if(GroupTypeEnum.Position.equals(groupType) ){
            //岗位的父节点可以为分公司,部门
            wrapper.in(COL_GROUP_TYPE,GroupTypeEnum.Branch.getId(),GroupTypeEnum.Department.getId(),GroupTypeEnum.Position.getId());
        }
        wrapper.eq(COL_STATUS, StatusEnum.enable.getCode());
        if(StringUtils.isNotEmpty(groupId)) {
            wrapper.ne(COL_GROUP_ID, groupId);
            wrapper.ne(COL_PARENT_ID,groupId);
        }
        wrapper.orderByAsc("level","sort");
        return groupMapper.selectList(wrapper);
    }

    /***
     * @description: 查询某用户的所有用户租
     * @param userId 用户ID
     * @return java.util.List<com.ezlcp.user.entity.group>
     * @author Elwin ZHANG
     * @date 2022/5/10 14:18
     */
    public List<Group> getUserGroups(String userId) {
        return groupMapper.getByUserId(userId);
    }

    /***
     * @description: 删除某用户的所有用户组
     * @param userId 用户ID
     * @author Elwin ZHANG
     * @date 2022/5/10 14:53
     */
    public void deleteUserGroups(String userId) {
        List<Group> list = getUserGroups(userId);
        if (list == null || list.size() == 0) {
            return;
        }
        //循环删除用户关联的所有
        for (Group group : list) {
            deleteUserGroup(userId, group.getGroupId());
        }
    }

    /***
     * @description: 批量为用户关联用户组
     * @param userId 用户ID
     * @param groupIds 用户组ID列表
     * @author Elwin ZHANG
     * @date 2022/5/10 16:01
     */
    public void addUserGroups(String userId, String groupIds) {
        String[] groups = groupIds.replace("'", "").split(",");
        //循环ID数组新增
        for (String groupId : groups) {
            addUserGroup(userId, groupId);
        }
    }

    /***
     * @description: 先清空该用户的用户组，再批量为用户关联新用户组
     * @param userId 用户ID
     * @param groupIds 用户组ID列表
     * @author Elwin ZHANG
     * @date 2022/5/10 16:01
     */
    @Transactional(rollbackFor = Exception.class)
    public void editUserGroups(String userId, String groupIds) {
        QueryWrapper<UserGroup> wrapper = new QueryWrapper<>();
        wrapper.eq(COL_USER_ID, userId);
        userGroupMapper.delete(wrapper);
        addUserGroups(userId, groupIds);
    }

    /***
     * @description: 增加用户与一个用户组的关联
     * @param userId 用户ID
     * @param groupId 用户组ID
     * @author Elwin ZHANG
     * @date 2022/5/10 15:54
     */
    public void addUserGroup(String userId, String groupId) {
        UserGroup ur = new UserGroup();
        ur.setUserId(userId);
        ur.setGroupId(groupId);
        ur.setId(IdGenerator.getIdStr());
        ur.setTenantId(ContextUtil.getCurrentTenantId());
        userGroupMapper.insert(ur);
    }

    /**
     * @param groupId 用户组ID
     * @description: 删除一个用户组
     * @author Elwin ZHANG
     * @date 2022/6/7 16:30
     */
    public void deleteGroup(String groupId) {
        Group group = groupMapper.selectById(groupId);
        if (group == null) {
            return;
        }
        group.setStatus((short) StatusEnum.deleted.getValue());
        group.setSeq(group.getSeq() + 1);
        groupMapper.updateById(group);
    }

    /***
     * @description: 删除用户与用户组的关系
     * @param userId 用户ID
     * @param groupId 用户组ID
     * @author Elwin ZHANG
     * @date 2022/5/10 15:42
     */
    public void deleteUserGroup(String userId, String groupId) {
        QueryWrapper<UserGroup> wrapper = new QueryWrapper<>();
        wrapper.eq(COL_USER_ID, userId);
        wrapper.eq(COL_GROUP_ID, groupId);
        userGroupMapper.delete(wrapper);
    }

    @Override
    public BaseDao<Group> getRepository() {
        return groupMapper;
    }
}