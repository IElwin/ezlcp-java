package com.ezlcp.user.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.constant.Constants;
import com.ezlcp.commons.service.impl.SuperServiceImpl;
import com.ezlcp.user.entity.IpStrategy;
import com.ezlcp.user.mapper.IpStrategyMapper;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * [IP黑名单或白名单控制]业务服务类
 */
@Service
public class IpStrategyServiceImpl extends SuperServiceImpl<IpStrategyMapper, IpStrategy> implements BaseService<IpStrategy> {
    @Resource
    private IpStrategyMapper ipStrategyMapper;

    /***
     * @description 查找某个公司的IP策略配置
     * @param tenantId 公司ID
     * @param type IP策略类型：1黑名单，2白名单; 0为全部
     * @return java.util.List<String>
     * @author Elwin ZHANG
     * @date 2023/1/29 17:30
     */
    public List<String> selectByTenantAndType(String tenantId, int type) {
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq(Constants.COL_TENANT_ID, tenantId);
        if (type > 0) {
            wrapper.eq("type", type);
        }
        List<IpStrategy> list = ipStrategyMapper.selectList(wrapper);
        if (list == null || list.isEmpty()) {
            return new ArrayList<String>();
        }
        return list.stream().map(IpStrategy::getIpAddr).collect(Collectors.toList());
    }


    @Override
    public BaseDao<IpStrategy> getRepository() {
        return ipStrategyMapper;
    }
}