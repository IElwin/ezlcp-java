package com.ezlcp.user.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.constant.Constants;
import com.ezlcp.commons.service.impl.SuperServiceImpl;
import com.ezlcp.commons.tool.StringUtils;
import com.ezlcp.commons.utils.ContextUtil;
import com.ezlcp.user.entity.TenantMenu;
import com.ezlcp.user.mapper.TenantMenuMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * [公司关联菜单]业务服务类
 */
@Service
public class TenantMenuServiceImpl extends SuperServiceImpl<TenantMenuMapper, TenantMenu> implements BaseService<TenantMenu> {
    @Autowired
    private TenantMenuMapper tenantMenuMapper;

    @Override
    public BaseDao<TenantMenu> getRepository() {
        return tenantMenuMapper;
    }

    /***
     * @description 判断路由是否唯一
     * @param url 路由
     * @param id 菜单ID
     * @return boolean
     * @author Elwin ZHANG
     * @date 2023/4/7 11:01
     */
    public boolean isUrlUnique(String url, String id) {
        QueryWrapper wrapper = new QueryWrapper();
        String tenantId = ContextUtil.getCurrentTenantId();
        if (StringUtils.isEmpty(tenantId)) {
            wrapper.isNull(Constants.COL_TENANT_ID);
        } else {
            wrapper.eq(Constants.COL_TENANT_ID, tenantId);
        }
        wrapper.eq("url", url);
        if (StringUtils.isNotEmpty(id)) {
            wrapper.ne("id", id);
        }
        var cnt = this.tenantMenuMapper.selectCount(wrapper);
        return cnt <= 0;
    }
}