
package com.ezlcp.user.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.service.impl.SuperServiceImpl;
import com.ezlcp.user.entity.City;
import com.ezlcp.user.mapper.CityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * [s_city]业务服务类
 */
@Service
public class CityServiceImpl extends SuperServiceImpl<CityMapper, City> implements BaseService<City> {
    @Autowired
    private CityMapper cityMapper;

    @Override
    public BaseDao<City> getRepository() {
        return cityMapper;
    }

    /***
    * @description  查找某个省下面的城市
    * @param province 省ID
    * @return java.util.List<com.ezlcp.user.entity.City>
    * @author Elwin ZHANG
    * @date 2023/2/8 11:57
    */
    public List<City> getByProvince(String province){
        QueryWrapper wrapper=new QueryWrapper();
        wrapper.eq("province",province);
        return cityMapper.selectList(wrapper);
    }
}