package com.ezlcp.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ezlcp.commons.base.entity.BaseExtEntity;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

/**
 * <pre>
 *
 * 描述：s_country实体类定义
 * 表:s_country
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-01-04 15:11:12
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "s_country")
@Schema(description ="国家")
public class Country extends BaseExtEntity<java.lang.String> {
    @Schema(description = "id")
    @TableId(value = "id", type = IdType.INPUT)
	private Integer id;
    @Schema(description = "name_")
    @TableField(value = "name_", jdbcType = JdbcType.VARCHAR)
    private String name;
    @Schema(description = "en_name")
    @TableField(value = "en_name", jdbcType = JdbcType.VARCHAR)
    private String enName;
    @Schema(description = "fullname")
    @TableField(value = "fullname", jdbcType = JdbcType.VARCHAR)
    private String fullname;
    @Schema(description = "alpha2")
    @TableField(value = "alpha2", jdbcType = JdbcType.VARCHAR)
    private String alpha2;
    @Schema(description = "alpha3")
    @TableField(value = "alpha3", jdbcType = JdbcType.VARCHAR)
    private String alpha3;
    @Schema(description = "no_")
    @TableField(value = "no_", jdbcType = JdbcType.NUMERIC)
    private Integer no;

    @JsonCreator
    public Country() {
    }

    @Override
    public String getPkId() {
        return id.toString();
    }

    @Override
    public void setPkId(String pkId) {
        this.id = Integer.parseInt(pkId);
    }
}