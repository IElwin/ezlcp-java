package com.ezlcp.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ezlcp.commons.base.entity.BaseExtEntity;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

/**
 * <pre>
 *
 * 描述：群组关联菜单及元素，多对多实体类定义
 * 表:s_group_menu
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-01-04 15:18:02
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "s_group_menu")
@Schema(description ="用户组关联的菜单")
public class GroupMenu extends BaseExtEntity<java.lang.String> {
    @Schema(description = "记录ID")
    @TableId(value = "id", type = IdType.INPUT)
	private String id;
    @Schema(description = "用户组ID")
    @TableField(value = "group_id", jdbcType = JdbcType.VARCHAR)
    private String groupId;
    @Schema(description = "菜单ID")
    @TableField(value = "menu_id", jdbcType = JdbcType.VARCHAR)
    private String menuId;
    @Schema(description = "是否有元素")
    @TableField(value = "has_element", jdbcType = JdbcType.NUMERIC)
    private Short hasElement;
    @Schema(description = "元素权限,json")
    @TableField(value = "elements", jdbcType = JdbcType.VARCHAR)
    private String elements;
    @Schema(description = "公司ID")
    @TableField(value = "tenant_id", jdbcType = JdbcType.VARCHAR)
    private String tenantId;
    @JsonCreator
    public GroupMenu() {
    }

    @Override
    public String getPkId() {
        return id;
    }

    @Override
    public void setPkId(String pkId) {
        this.id = pkId;
    }
}