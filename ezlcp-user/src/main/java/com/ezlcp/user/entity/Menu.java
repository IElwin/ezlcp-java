package com.ezlcp.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ezlcp.commons.base.entity.BaseExtEntity;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

/**
 * <pre>
 *
 * 描述：系统菜单表实体类定义
 * 表:s_menu
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-01-04 15:14:59
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "s_menu")
@Schema(description ="菜单")
public class Menu extends BaseExtEntity<java.lang.String> {
    @Schema(description = "菜单ID")
    @TableId(value = "menu_id", type = IdType.INPUT)
	private String menuId;
    @Schema(description = "菜单名称")
    @TableField(value = "menu_name", jdbcType = JdbcType.VARCHAR)
    private String menuName;
    @Schema(description = "繁体中文名称")
    @TableField(value = "hk_name", jdbcType = JdbcType.VARCHAR)
    private String hkName;
    @Schema(description = "英文名称")
    @TableField(value = "en_name", jdbcType = JdbcType.VARCHAR)
    private String enName;
    @Schema(description = "上级菜单id")
    @TableField(value = "parent_id", jdbcType = JdbcType.VARCHAR)
    private String parentId;
    @Schema(description = "访问路由")
    @TableField(value = "url", jdbcType = JdbcType.VARCHAR)
    private String url;
    @Schema(description = "vue页面")
    @TableField(value = "component", jdbcType = JdbcType.VARCHAR)
    private String component;
    @Schema(description = "参数")
    @TableField(value = "params", jdbcType = JdbcType.VARCHAR)
    private String params;
    @Schema(description = "默认子路由")
    @TableField(value = "nav_url", jdbcType = JdbcType.VARCHAR)
    private String navUrl;
    @Schema(description = "排序")
    @TableField(value = "sort", jdbcType = JdbcType.NUMERIC)
    private Integer sort;
    @Schema(description = "图标")
    @TableField(value = "icon", jdbcType = JdbcType.VARCHAR)
    private String icon;
    @Schema(description = "页面等级")
    @TableField(value = "page_level", jdbcType = JdbcType.NUMERIC)
    private Short pageLevel;
    @Schema(description = "其他属性")
    @TableField(value = "other", jdbcType = JdbcType.VARCHAR)
    private String other;
    @Schema(description = "纯路由,菜单不显示")
    @TableField(value = "hidden", jdbcType = JdbcType.TINYINT)
    private Short hidden;
    @Schema(description = "1租户0平台4删除")
    @TableField(value = "status", jdbcType = JdbcType.NUMERIC)
    private Short status;
    @Schema(description = "路径")
    @TableField(value = "parent_path", jdbcType = JdbcType.VARCHAR)
    private String parentPath;
    @Schema(description = "是否一级菜单直接跳到的二级菜单")
    @TableField(value = "is_direct", jdbcType = JdbcType.TINYINT)
    private Short isDirect;
    @Schema(description = "0目录1页面2外部链接")
    @TableField(value = "menu_type", jdbcType = JdbcType.NUMERIC)
    private Short menuType;
    @Schema(description = "0否1是")
    @TableField(value = "is_custom", jdbcType = JdbcType.NUMERIC)
    private Short isCustom;
    @Schema(description = "1PC 2手机 3全部")
    @TableField(value = "front_type", jdbcType = JdbcType.NUMERIC)
    private Short frontType;
    @Schema(description = "修改次数")
    @TableField(value = "seq", jdbcType = JdbcType.NUMERIC)
    private Integer seq;

    @JsonCreator
    public Menu() {
    }

    @Override
    public String getPkId() {
        return menuId;
    }

    @Override
    public void setPkId(String pkId) {
        this.menuId = pkId;
    }
}