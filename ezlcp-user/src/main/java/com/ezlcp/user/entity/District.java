package com.ezlcp.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ezlcp.commons.base.entity.BaseExtEntity;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

/**
 * <pre>
 *
 * 描述：s_district实体类定义
 * 表:s_district
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-01-04 15:13:51
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "s_district")
@Schema(description ="县或区")
public class District extends BaseExtEntity<java.lang.String> {
    @Schema(description = "district")
    @TableId(value = "district", type = IdType.INPUT)
	private String district;
    @Schema(description = "name_")
    @TableField(value = "name_", jdbcType = JdbcType.VARCHAR)
    private String name;
    @Schema(description = "en_name")
    @TableField(value = "en_name", jdbcType = JdbcType.VARCHAR)
    private String enName;
    @Schema(description = "city")
    @TableField(value = "city", jdbcType = JdbcType.VARCHAR)
    private char city;

    @JsonCreator
    public District() {
    }

    @Override
    public String getPkId() {
        return district;
    }

    @Override
    public void setPkId(String pkId) {
        this.district = pkId;
    }
}