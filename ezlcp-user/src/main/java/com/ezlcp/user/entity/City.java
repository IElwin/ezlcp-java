package com.ezlcp.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ezlcp.commons.base.entity.BaseExtEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

/**
 * <pre>
 *
 * 描述：s_city实体类定义
 * 表:s_city
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-01-04 13:13:45
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "s_city")
@Schema(description = "城市")
public class City extends BaseExtEntity<java.lang.String> {
    @Schema(description = "city")
    @TableId(value = "city", type = IdType.INPUT)
	private String city;
    @Schema(description = "code")
    @TableField(value = "code", jdbcType = JdbcType.VARCHAR)
    private String code;
    @Schema(description = "name_")
    @TableField(value = "name_", jdbcType = JdbcType.VARCHAR)
    private String name;
    @Schema(description = "en_name")
    @TableField(value = "en_name", jdbcType = JdbcType.VARCHAR)
    private String enName;
    @Schema(description = "province")
    @TableField(value = "province", jdbcType = JdbcType.VARCHAR)
    private char province;

    @JsonCreator
    public City() {
    }

    @Override
    public String getPkId() {
        return city;
    }

    @Override
    public void setPkId(String pkId) {
        this.city = pkId;
    }
}