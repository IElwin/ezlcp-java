package com.ezlcp.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ezlcp.commons.base.entity.BaseExtEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

/**
 * <pre>
 *
 * 描述：IP黑名单或白名单控制实体类定义
 * 表:s_ip_strategy
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-01-16 10:41:51
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "s_ip_strategy")
@Schema(description = "IP地址安全策略")
public class IpStrategy extends BaseExtEntity<java.lang.String> {
    @Schema(description = "主键")
    @TableId(value = "id", type = IdType.INPUT)
	private String id;
    @Schema(description = "IP地址")
    @TableField(value = "ip_addr", jdbcType = JdbcType.VARCHAR)
    private String ipAddr;
    @Schema(description = "备注")
    @TableField(value = "remark", jdbcType = JdbcType.VARCHAR)
    private String remark;
    @Schema(description = "0未启用1启用")
    @TableField(value = "status", jdbcType = JdbcType.NUMERIC)
    private Short status;
    @Schema(description = "1黑名单2白名单")
    @TableField(value = "type", jdbcType = JdbcType.NUMERIC)
    private Short type;
    @Schema(description = "公司ID")
    @TableField(value = "tenant_id", jdbcType = JdbcType.VARCHAR)
    private String tenantId;
    @JsonCreator
    public IpStrategy() {
    }

    @Override
    public String getPkId() {
        return id;
    }

    @Override
    public void setPkId(String pkId) {
        this.id = pkId;
    }
}