package com.ezlcp.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ezlcp.commons.base.entity.BaseExtEntity;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

/**
 * <pre>
 *
 * 描述：用户与角色关联表，多对多实体类定义
 * 表:s_user_group
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-01-04 15:21:21
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "s_user_group")
@Schema(description ="用户关联用户组")
public class UserGroup extends BaseExtEntity<java.lang.String> {
    @Schema(description = "记录ID")
    @TableId(value = "id", type = IdType.INPUT)
	private String id;
    @Schema(description = "用户ID")
    @TableField(value = "user_id", jdbcType = JdbcType.VARCHAR)
    private String userId;
    @Schema(description = "群组ID")
    @TableField(value = "group_id", jdbcType = JdbcType.VARCHAR)
    private String groupId;
    @Schema(description = "公司ID")
    @TableField(value = "tenant_id", jdbcType = JdbcType.VARCHAR)
    private String tenantId;
    @JsonCreator
    public UserGroup() {
    }

    @Override
    public String getPkId() {
        return id;
    }

    @Override
    public void setPkId(String pkId) {
        this.id = pkId;
    }
}