package com.ezlcp.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ezlcp.commons.base.entity.BaseExtEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

/**
 * <pre>
 *
 * 描述：租户配置实体类定义
 * 表:s_settings
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-01-29 10:31:19
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "s_settings")
@Schema(description = "公司设置")
public class Settings extends BaseExtEntity<java.lang.String> {
    @Schema(description = "设置id")
    @TableId(value = "id", type = IdType.INPUT)
    private String id;
    @Schema(description = "公司id")
    @TableField(value = "tenant_id", jdbcType = JdbcType.VARCHAR)
	private String tenantId;
    @Schema(description = "默认语言")
    @TableField(value = "default_lang", jdbcType = JdbcType.VARCHAR)
    private String defaultLang;
    @Schema(description = "默认主题")
    @TableField(value = "default_theme", jdbcType = JdbcType.VARCHAR)
    private String defaultTheme;
    @Schema(description = "启用谷歌身份验证器")
    @TableField(value = "google_auth", jdbcType = JdbcType.NUMERIC)
    private Short googleAuth;
    @Schema(description = "空闲几分钟自动登出")
    @TableField(value = "idle_minutes", jdbcType = JdbcType.NUMERIC)
    private Short idleMinutes;
    @Schema(description = "1黑名单2白名单0未配置")
    @TableField(value = "ip_strategy", jdbcType = JdbcType.NUMERIC)
    private Short ipStrategy;
    @Schema(description = "锁定时长（分钟）")
    @TableField(value = "lock_minutes", jdbcType = JdbcType.NUMERIC)
    private Short lockMinutes;
    @Schema(description = "登录失败几次锁定用户")
    @TableField(value = "login_fail_times", jdbcType = JdbcType.NUMERIC)
    private Short loginFailTimes;
    @Schema(description = "用户密码长度")
    @TableField(value = "pw_length", jdbcType = JdbcType.NUMERIC)
    private Short pwLength;
    @Schema(description = "字母数字特殊符号有几种")
    @TableField(value = "pw_strength", jdbcType = JdbcType.NUMERIC)
    private Short pwStrength;
    @Schema(description = "修改次数")
    @TableField(value = "seq", jdbcType = JdbcType.NUMERIC)
    private Integer seq;
    @JsonCreator
    public Settings() {
    }

    @Override
    public String getPkId() {
        return tenantId;
    }

    @Override
    public void setPkId(String pkId) {
        this.tenantId = pkId;
    }
}