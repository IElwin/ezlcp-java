package com.ezlcp.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ezlcp.commons.constant.GroupTypeEnum;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ezlcp.commons.base.entity.BaseExtEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

/**
 * <pre>
 *
 * 描述：角色、群组、部门、分公司等人员集合实体类定义
 * 表:s_group
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-02-06 17:51:28
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "s_group")
@Schema(description = "用户组")
public class Group extends BaseExtEntity<java.lang.String> {
    @Schema(description = "用户组ID")
    @TableId(value = "group_id", type = IdType.INPUT)
	private String groupId;
    @Schema(description = "名称")
    @TableField(value = "group_name", jdbcType = JdbcType.VARCHAR)
    private String groupName;
    @Schema(description = "繁体中文名称")
    @TableField(value = "hk_name", jdbcType = JdbcType.VARCHAR)
    private String hkName;
    @Schema(description = "英文名称")
    @TableField(value = "en_name", jdbcType = JdbcType.VARCHAR)
    private String enName;
    @Schema(description = "备注")
    @TableField(value = "remark", jdbcType = JdbcType.VARCHAR)
    private String remark;
    @Schema(description = "0用户组1分公司2部门4岗位8角色")
    @TableField(value = "group_type", jdbcType = JdbcType.NUMERIC)
    private Short groupType;
    @Schema(description = "0停用1启用4删除")
    @TableField(value = "status", jdbcType = JdbcType.NUMERIC)
    private Short status;
    @Schema(description = "排序")
    @TableField(value = "sort", jdbcType = JdbcType.NUMERIC)
    private Integer sort;
    @Schema(description = "父ID")
    @TableField(value = "parent_id", jdbcType = JdbcType.VARCHAR)
    private String parentId;
    @Schema(description = "路径")
    @TableField(value = "path", jdbcType = JdbcType.VARCHAR)
    private String path;
    @Schema(description = "等级")
    @TableField(value = "level", jdbcType = JdbcType.NUMERIC)
    private Short level;
    @Schema(description = "修改次数")
    @TableField(value = "seq", jdbcType = JdbcType.NUMERIC)
    private Integer seq;
    @Schema(description = "公司ID")
    @TableField(value = "tenant_id", jdbcType = JdbcType.VARCHAR)
    private String tenantId;
    @Schema(description = "上级名称")
    @TableField(exist = false)
    private String parentName;
    @Schema(description = "上级英文名称")
    @TableField(exist = false)
    private String parentEnName;
    @Schema(description = "上级繁体名称")
    @TableField(exist = false)
    private String parentHkName;
    @JsonCreator
    public Group() {
    }

    public String getTypeName(){
        return GroupTypeEnum.valueOf(groupType).getName();
    }
    public String getTypeEnName(){
        return GroupTypeEnum.valueOf(groupType).getEnName();
    }
    public String getTypeHkName(){
        return GroupTypeEnum.valueOf(groupType).getHkName();
    }
    @Override
    public String getPkId() {
        return groupId;
    }

    @Override
    public void setPkId(String pkId) {
        this.groupId = pkId;
    }
}