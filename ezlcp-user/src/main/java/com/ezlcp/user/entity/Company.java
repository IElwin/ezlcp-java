package com.ezlcp.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ezlcp.commons.constant.StatusEnum;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ezlcp.commons.base.entity.BaseExtEntity;
 
 
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

/**
 * <pre>
 *
 * 描述：即租户信息实体类定义
 * 表:s_company
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-01-04 15:06:28
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "s_company")
@Schema(description ="公司或租户")
public class Company extends BaseExtEntity<java.lang.String> {
    @Schema(description = "公司id")
    @TableId(value = "tenant_id", type = IdType.INPUT)
	private String tenantId;
    @Schema(description = "公司管理员")
    @TableField(value = "user_id", jdbcType = JdbcType.VARCHAR)
    private String userId;
    @Schema(description = "英文名称")
    @TableField(value = "en_name", jdbcType = JdbcType.VARCHAR)
    private String enName;
    @Schema(description = "中文名称")
    @TableField(value = "name", jdbcType = JdbcType.VARCHAR)
    private String name;
    @Schema(description = "牌照号码")
    @TableField(value = "license_no", jdbcType = JdbcType.VARCHAR)
    private String licenseNo;
    @Schema(description = "中文地址")
    @TableField(value = "address", jdbcType = JdbcType.VARCHAR)
    private String address;
    @Schema(description = "英文地址")
    @TableField(value = "en_addr", jdbcType = JdbcType.VARCHAR)
    private String enAddr;
    @Schema(description = "网址")
    @TableField(value = "web_site", jdbcType = JdbcType.VARCHAR)
    private String webSite;
    @Schema(description = "公司电话")
    @TableField(value = "tel_no", jdbcType = JdbcType.VARCHAR)
    private String telNo;
    @Schema(description = "电邮地址")
    @TableField(value = "email", jdbcType = JdbcType.VARCHAR)
    private String email;
    @Schema(description = "狀態")
    @TableField(value = "status", jdbcType = JdbcType.VARCHAR)
    private String status;
    @Schema(description = "公司LOGO")
    @TableField(value = "url", jdbcType = JdbcType.VARCHAR)
    private String url;
    @Schema(description = "账户数量限制")
    @TableField(value = "account_limit", jdbcType = JdbcType.NUMERIC)
    private Integer accountLimit;
    @Schema(description = "使用期限")
    @TableField(value = "time_limit", jdbcType = JdbcType.DATE)
    private java.util.Date timeLimit;
    @Schema(description = "修改次数")
    @TableField(value = "seq", jdbcType = JdbcType.NUMERIC)
    private Integer seq;
    @TableField(exist = false)
    @Schema(description = "登录密码")
    private String password;
    @TableField(exist = false)
    @Schema(description = "账户名称")
    private String userName;

    @JsonCreator
    public Company() {
    }

    @Override
    public String getPkId() {
        return tenantId;
    }

    @Override
    public void setPkId(String pkId) {
        this.tenantId = pkId;
    }
}