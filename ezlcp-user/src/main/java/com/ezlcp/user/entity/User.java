package com.ezlcp.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ezlcp.commons.tool.StringUtils;
import com.ezlcp.commons.constant.StatusEnum;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ezlcp.commons.base.entity.BaseExtEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

/**
 * <pre>
 *
 * 描述：用户表实体类定义
 * 表:s_user
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-01-29 10:34:09
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "s_user")
@Schema(description = "系统用户")
public class User extends BaseExtEntity<java.lang.String> {
    @Schema(description = "用户ID")
    @TableId(value = "user_id", type = IdType.INPUT)
	private String userId;
    @Schema(description = "住宅地址")
    @TableField(value = "address", jdbcType = JdbcType.VARCHAR)
    private String address;
    @Schema(description = "电邮地址")
    @TableField(value = "email", jdbcType = JdbcType.VARCHAR)
    private String email;
    @Schema(description = "英文名")
    @TableField(value = "en_name", jdbcType = JdbcType.VARCHAR)
    private String enName;
    @Schema(description = "入职时间")
    @TableField(value = "entry_date", jdbcType = JdbcType.DATE)
    private java.util.Date entryDate;
    @Schema(description = "性别")
    @TableField(value = "gender", jdbcType = JdbcType.NUMERIC)
    private Short gender;
    @Schema(description = "是否已绑定Google验证")
    @TableField(value = "is_bind", jdbcType = JdbcType.NUMERIC)
    private Short isBind;
    @Schema(description = "是否锁定")
    @TableField(value = "is_lock", jdbcType = JdbcType.NUMERIC)
    private Short isLock;
    @Schema(description = "预设界面语言")
    @TableField(value = "language", jdbcType = JdbcType.VARCHAR)
    private String language;
    @Schema(description = "锁定开始时间")
    @TableField(value = "lock_time", jdbcType = JdbcType.TIMESTAMP)
    private java.util.Date lockTime;
    @Schema(description = "登录失败次数")
    @TableField(value = "login_fail_times", jdbcType = JdbcType.NUMERIC)
    private Short loginFailTimes;
    @Schema(description = "手机号码")
    @TableField(value = "mobile_no", jdbcType = JdbcType.VARCHAR)
    private String mobileNo;
    @Schema(description = "姓名")
    @TableField(value = "name", jdbcType = JdbcType.VARCHAR)
    private String name;
    @Schema(description = "身份证号码")
    @TableField(value = "passport_no", jdbcType = JdbcType.VARCHAR)
    private String passportNo;
    @Schema(description = "登录密码")
    @TableField(value = "password", jdbcType = JdbcType.VARCHAR)
    private String password;
    @Schema(description = "备注")
    @TableField(value = "remark", jdbcType = JdbcType.VARCHAR)
    private String remark;
    @Schema(description = "Google验证密钥")
    @TableField(value = "secret_key", jdbcType = JdbcType.VARCHAR)
    private String secretKey;
    @Schema(description = "修改次数")
    @TableField(value = "seq", jdbcType = JdbcType.NUMERIC)
    private Integer seq;
    @Schema(description = "状态")
    @TableField(value = "status", jdbcType = JdbcType.VARCHAR)
    private String status;
    @Schema(description = "用户编号")
    @TableField(value = "user_code", jdbcType = JdbcType.VARCHAR)
    private String userCode;
    @Schema(description = "账户名称")
    @TableField(value = "user_name", jdbcType = JdbcType.VARCHAR)
    private String userName;
    @Schema(description = "1管理员0普通用户")
    @TableField(value = "user_type", jdbcType = JdbcType.NUMERIC)
    private Short userType;
    @Schema(description = "公司ID")
    @TableField(value = "tenant_id", jdbcType = JdbcType.VARCHAR)
    private String tenantId;
    @TableField(exist = false)
    @Schema(description = "账号归属的群组")
    private String[] groupIds;
    @TableField(exist = false)
    @Schema(description = "最近登录ip")
    private String ipAddr;
    @TableField(exist = false)
    @Schema(description = "最近登录时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private java.util.Date loginTime;
    @JsonCreator
    public User() {
    }
    /***
    * @description 是否为管理员类型账户
    */
    public boolean isAdmin(){
        return userType==1;
    }
    /***
    * @description  是否平台用户
    */
    public boolean isPlatformUser(){
        return StringUtils.isEmpty(tenantId);
    }
    /***
     * @description  是否公司/租户用户
     */
    public boolean isTenantUser(){
        return !isPlatformUser();
    }
    /***
     * @description  是否公司的普通员工
     */
    public boolean isStaff(){
        return (isPlatformUser() && !isAdmin());
    }
    /***
     * @description  是否平台超级管理员
     */
    public boolean isSuperAdmin(){
        return (isAdmin() && isPlatformUser());
    }

    @Override
    public String getPkId() {
        return userId;
    }

    @Override
    public void setPkId(String pkId) {
        this.userId = pkId;
    }
}