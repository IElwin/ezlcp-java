package com.ezlcp.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.ezlcp.commons.base.entity.BaseExtEntity;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

/**
 * <pre>
 *
 * 描述：頁面功能表实体类定义
 * 表:s_page_element
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-01-04 15:15:33
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "s_page_element")
@Schema(description ="页面元素")
public class PageElement extends BaseExtEntity<java.lang.String> {
    @Schema(description = "主键")
    @TableId(value = "id", type = IdType.INPUT)
	private String id;
    @Schema(description = "菜單ID")
    @TableField(value = "menu_id", jdbcType = JdbcType.VARCHAR)
    private String menuId;
    @Schema(description = "元素KEY")
    @TableField(value = "element_key", jdbcType = JdbcType.VARCHAR)
    private String elementKey;
    @Schema(description = "元素名称")
    @TableField(value = "element_name", jdbcType = JdbcType.VARCHAR)
    private String elementName;
    @Schema(description = "英文名称")
    @TableField(value = "en_name", jdbcType = JdbcType.VARCHAR)
    private String enName;
    @Schema(description = "繁体中文名")
    @TableField(value = "hk_name", jdbcType = JdbcType.VARCHAR)
    private String hkName;
    @Schema(description = "类型,一般为按钮")
    @TableField(value = "type", jdbcType = JdbcType.VARCHAR)
    private String type;
    @Schema(description = "备注")
    @TableField(value = "remark", jdbcType = JdbcType.VARCHAR)
    private String remark;
    @Schema(description = "修改次数")
    @TableField(value = "seq", jdbcType = JdbcType.NUMERIC)
    private Integer seq;
    @Schema(description = "公司ID")
    @TableField(value = "tenant_id", jdbcType = JdbcType.VARCHAR)
    private String tenantId;

    @JsonCreator
    public PageElement() {
    }

    @Override
    public String getPkId() {
        return id;
    }

    @Override
    public void setPkId(String pkId) {
        this.id = pkId;
    }
}