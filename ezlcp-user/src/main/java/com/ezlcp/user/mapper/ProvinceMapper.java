package com.ezlcp.user.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.user.entity.Province;
import org.apache.ibatis.annotations.Mapper;

/**
 * s_province数据库访问层
 */
@Mapper
public interface ProvinceMapper extends BaseDao<Province> {
}