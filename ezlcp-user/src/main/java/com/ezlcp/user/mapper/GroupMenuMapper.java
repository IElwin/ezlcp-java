package com.ezlcp.user.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.user.entity.GroupMenu;
import org.apache.ibatis.annotations.Mapper;

/**
 * 群组关联菜单及元素，多对多数据库访问层
 */
@Mapper
public interface GroupMenuMapper extends BaseDao<GroupMenu> {
}