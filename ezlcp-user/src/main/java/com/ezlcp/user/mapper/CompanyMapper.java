package com.ezlcp.user.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.user.entity.Company;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 即租户信息数据库访问层
 */
@Mapper
public interface CompanyMapper extends BaseDao<Company> {
    Company selectByTenantId(@Param("tenantId") String tenantId);
}