package com.ezlcp.user.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.user.entity.GroupMenu;
import com.ezlcp.user.entity.Menu;
import com.ezlcp.user.entity.TenantMenu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 系统菜单表数据库访问层
 */
@Mapper
public interface MenuMapper extends BaseDao<Menu> {

    /***
     * @description: 获取平台用户的授权菜单
     * @param userId 用户ID
     * @return java.util.List<com.ezlcp.user.entity.Menu>
     * @author Elwin ZHANG
     * @date 2022/5/11 13:39
     */
    List<TenantMenu> getPlatUserMenus(@Param("userId")String userId);

    /**
     * @description: 获取公司用户的授权菜单
     * @param userId 用户ID
     * @param tenantId 公司ID
     * @return java.util.List<com.ezlcp.user.entity.Menu>
     * @author Elwin ZHANG
     * @date 2022/6/9 9:53
     */
    List<TenantMenu> getCommonUserMenus(@Param("userId")String userId, @Param("tenantId")String tenantId);


    /***
     * @description: 查询用户的某个菜单页面权限
     * @param userId 用户ID
     * @param menuId 菜单ID
     * @return java.util.List<java.util.HashMap>
     * @author Elwin ZHANG
     * @date 2022/5/11 14:30
     */
    List<GroupMenu> getUserPageAuth(@Param("userId")String userId, @Param("menuId")String menuId);

    /***
    * @description  初始化租户菜单
    * @param tenantId 租户ID
    * @author Elwin ZHANG
    * @date 2023/3/27 17:24
    */
    int initTenantMenu1(@Param("tenantId")String tenantId);
    int initTenantMenu2(@Param("tenantId")String tenantId);
}