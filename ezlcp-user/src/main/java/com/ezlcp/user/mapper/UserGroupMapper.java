package com.ezlcp.user.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.user.entity.UserGroup;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户与角色关联表，多对多数据库访问层
 */
@Mapper
public interface UserGroupMapper extends BaseDao<UserGroup> {
}