package com.ezlcp.user.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.user.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户表数据库访问层
 */
@Mapper
public interface UserMapper extends BaseDao<User> {
}