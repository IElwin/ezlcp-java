package com.ezlcp.user.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.user.entity.IpStrategy;
import org.apache.ibatis.annotations.Mapper;

/**
 * IP黑名单或白名单控制数据库访问层
 */
@Mapper
public interface IpStrategyMapper extends BaseDao<IpStrategy> {
}