package com.ezlcp.user.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.user.entity.TenantMenu;
import org.apache.ibatis.annotations.Mapper;

/**
 * 公司关联菜单数据库访问层
 */
@Mapper
public interface TenantMenuMapper extends BaseDao<TenantMenu> {
}