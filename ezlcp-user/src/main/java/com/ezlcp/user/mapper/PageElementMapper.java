package com.ezlcp.user.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.user.entity.PageElement;
import org.apache.ibatis.annotations.Mapper;

/**
 * 頁面功能表数据库访问层
 */
@Mapper
public interface PageElementMapper extends BaseDao<PageElement> {
}