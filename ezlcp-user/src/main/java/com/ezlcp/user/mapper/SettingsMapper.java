package com.ezlcp.user.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.user.entity.Settings;
import org.apache.ibatis.annotations.Mapper;

/**
 * 租户配置数据库访问层
 */
@Mapper
public interface SettingsMapper extends BaseDao<Settings> {
}