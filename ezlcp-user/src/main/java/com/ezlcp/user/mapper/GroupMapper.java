package com.ezlcp.user.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.user.entity.Group;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 角色、群组、部门、分公司等人员集合数据库访问层
 */
@Mapper
public interface GroupMapper extends BaseDao<Group> {
    /***
     * @description: 查询某用户的所有用户组
     * @param userId 用户ID
     * @author Elwin ZHANG
     * @date 2022/5/10 14:18
     */
    List<Group> getByUserId(@Param("userId")String userId);
}