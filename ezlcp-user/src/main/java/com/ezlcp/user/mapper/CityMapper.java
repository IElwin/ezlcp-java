package com.ezlcp.user.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.user.entity.City;
import org.apache.ibatis.annotations.Mapper;

/**
 * s_city数据库访问层
 */
@Mapper
public interface CityMapper extends BaseDao<City> {
}