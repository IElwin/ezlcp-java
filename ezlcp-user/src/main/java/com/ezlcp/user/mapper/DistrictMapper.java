package com.ezlcp.user.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.user.entity.District;
import org.apache.ibatis.annotations.Mapper;

/**
 * s_district数据库访问层
 */
@Mapper
public interface DistrictMapper extends BaseDao<District> {
}