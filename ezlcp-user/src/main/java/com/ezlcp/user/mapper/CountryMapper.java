package com.ezlcp.user.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.user.entity.Country;
import org.apache.ibatis.annotations.Mapper;

/**
 * s_country数据库访问层
 */
@Mapper
public interface CountryMapper extends BaseDao<Country> {
}