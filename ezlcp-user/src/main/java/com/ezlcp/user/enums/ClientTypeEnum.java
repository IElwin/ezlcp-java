package com.ezlcp.user.enums;

/**
 * @author Elwin ZHANG
 * @description: 客户端类型<br />
 * @date 2023/1/19 10:33
 */
public enum ClientTypeEnum {
    // description = "客户端类型：0.PC 1.H5 2.安卓 4.IOS"
    PC(0), HTML5(1), Android(2), IOS(4);
    private final int id;

    ClientTypeEnum(int id) {
        this.id = id;
    }

    public int getValue() {
        return id;
    }
}
