package com.ezlcp.user.enums;

/**
 * @author Elwin ZHANG
 * @description: IP安全策略<br/>
 * @date 2023/1/29 16:24
 */
public enum IpStrategyEnum {

    // 0.未设置 1.黑名单控制 2.白名单控制
    None(0), BlackList(1), WhiteList(2);
    private final int id;

    IpStrategyEnum(int id) {
        this.id = id;
    }

    public int getValue() {
        return id;
    }
}
