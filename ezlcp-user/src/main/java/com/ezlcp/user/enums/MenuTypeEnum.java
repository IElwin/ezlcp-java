package com.ezlcp.user.enums;

/**
 * @author Elwin ZHANG
 * @description: 菜单类型枚举<br/>
 * @date 2023/4/3 11:37
 */
public enum MenuTypeEnum {
    // 0.目录 1.Vue组件 2.外部链接
    Directory(0), VuePage(1), LinkUrl(2);
    private final int id;

    MenuTypeEnum(int id) {
        this.id = id;
    }

    public int getValue() {
        return id;
    }
}
