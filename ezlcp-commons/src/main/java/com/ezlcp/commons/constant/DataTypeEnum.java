package com.ezlcp.commons.constant;

import com.alibaba.fastjson2.JSONObject;

/**
 * @author Elwin ZHANG
 * @description: 数据库字段类型<br />
 * @date 2023/6/19 17:31
 */
public enum DataTypeEnum {

    SmallInt("smallint", 1),
    Int("int", 1),
    BigInt("bigint", 1),
    Decimal("decimal", 1),
    VarChar("varchar", 2),
    Text("text", 2),
    LongText("longtext", 2),
    //Blob("blob",2),
    Date("date", 3),
    Datetime("datetime", 3);

    /***
     * 大的类型：数值、字符、日期
     */
    private final int type;
    /***
     * 保存的值
     */
    private final String value;

    DataTypeEnum(String value, int type) {
        this.value = value;
        this.type = type;
    }

    /***
    * 根据数据类型字符串值得到对应的枚举类型
    * @param strDataType 类型字符串值
    */
    public static DataTypeEnum get(String strDataType) {
        if ("smallint".equals(strDataType)) {
            return DataTypeEnum.SmallInt;
        } else if ("int".equals(strDataType)) {
            return DataTypeEnum.Int;
        } else if ("bigint".equals(strDataType)) {
            return DataTypeEnum.BigInt;
        } else if ("decimal".equals(strDataType)) {
            return DataTypeEnum.Decimal;
        } else if ("text".equals(strDataType)) {
            return DataTypeEnum.Text;
        } else if ("longtext".equals(strDataType)) {
            return DataTypeEnum.LongText;
        } else if ("date".equals(strDataType)) {
            return DataTypeEnum.Date;
        } else if ("datetime".equals(strDataType)) {
            return DataTypeEnum.Datetime;
        }
        return DataTypeEnum.VarChar;
    }

    public int getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    /***
     * @description 转换成JSON对象
     * @return com.alibaba.fastjson2.JSONObject
     * @author Elwin ZHANG
     * @date 2023/6/29 14:41
     */
    public JSONObject toJsonObject() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("value", value);
        jsonObject.put("name", this.name());
        jsonObject.put("type", type);
        return jsonObject;
    }
}
