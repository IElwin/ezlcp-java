package com.ezlcp.commons.constant;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;

/**
 * @author Elwin ZHANG
 * @description: 用户组类型枚举<br />
 * 0用户组1分公司2部门4岗位8角色
 * @date 2023/3/16 11:48
 */
public enum GroupTypeEnum {
    /**
     * 用户组
     */
    UserGroup(0, "用户组", "用戶組", "User Group"),
    /**
     * 分公司
     */
    Branch(1, "分公司", "分公司", "Branch"),
    /**
     * 部门
     */
    Department(2, "部门", "部門", "Department"),
    /**
     * 岗位
     */
    Position(4, "岗位", "崗位", "Position"),
    /**
     * 角色
     */
    Role(8, "角色", "角色", "Role");
    /**
     * 枚举值
     */
    private final int id;
    /**
     * 简体中文名
     */
    private final String name;
    /**
     * 英文名
     */
    private final String enName;
    /**
     * 繁体中文名
     */
    private final String hkName;

    GroupTypeEnum(int id, String name, String hkName, String enName) {
        this.id = id;
        this.name = name;
        this.enName = enName;
        this.hkName = hkName;
    }

    /***
    * 根据ID值返回对应的枚举类型
    */
    public static GroupTypeEnum valueOf(int id) {
        if (id == 1) {
            return Branch;
        } else if (id == 2) {
            return Department;
        } else if (id == 4) {
            return Position;
        } else if (id == 8) {
            return Role;
        } else {
            return UserGroup;
        }
    }
    /**
     * 枚举值
     */
    public int getId() {
        return id;
    }
    /**
     * 简体中文名
     */
    public String getName() {
        return name;
    }
    /**
     * 英文名
     */
    public String getEnName() {
        return enName;
    }
    /**
     * 繁体中文名
     */
    public String getHkName() {
        return hkName;
    }

    /***
    * @description  转换成JSON对象
    * @return com.alibaba.fastjson2.JSONObject
    * @author Elwin ZHANG
    * @date 2023/3/16 14:41
    */
    public JSONObject toJsonObject(){
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("id", id);
        jsonObject.put("name",name);
        jsonObject.put("enName",enName);
        jsonObject.put("hkName",hkName);
        return jsonObject;
    }
}
