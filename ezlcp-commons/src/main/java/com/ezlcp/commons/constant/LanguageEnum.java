package com.ezlcp.commons.constant;

/**
 * @author Elwin ZHANG
 * @description: 语言类型<br/>
 * @date 2023/1/18 17:44
 */
public enum LanguageEnum
{
    zh_CN,//简体中文(中国)
    zh_HK,//繁体中文
    en;//英语
}
