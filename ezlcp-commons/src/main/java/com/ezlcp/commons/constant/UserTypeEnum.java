package com.ezlcp.commons.constant;

/**
 * @author Elwin ZHANG
 * @description: 用户类型枚举型<br/>
 * @date 2023/2/8 14:23
 */
public enum UserTypeEnum {
    // 1管理员0普通用户
    User(0), Admin(1);
    private final int id;

    UserTypeEnum(int id) {
        this.id = id;
    }

    public static UserTypeEnum valueOf(int id){
        if(id==1){
            return Admin;
        }
        return User;
    }

    public int getValue() {
        return id;
    }
}
