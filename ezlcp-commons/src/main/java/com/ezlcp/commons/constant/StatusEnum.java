package com.ezlcp.commons.constant;

/**
 * 主表状态字段
 */
public enum StatusEnum {
    /**
     * 启用 就职
     */
    enable(1),
    /**
     * 禁用 离职
     */
    disable(0),
    /**
     * 驳回
     */
    reject(2),
    /**
     * 删除
     */
    deleted(4);

    private int value;

    StatusEnum(int value) {
        this.value = value;
    }

    /***
     * @description 获取整型的值
     */
    public int getValue() {
        return value;
    }

    /***
    * @description 获取字符串类型的值
    */
    public String getCode(){
        return value + "";
    }
}
