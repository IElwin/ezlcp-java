package com.ezlcp.commons.constant;

import com.alibaba.fastjson2.JSONObject;

/**
 * @author Elwin ZHANG
 * 自定义列表视图类型：  <br/>
 * 0表格1看板2相册3主从表4树表5组合表6图表7甘特图8日历
 */
public enum ListTypeEnum {
    /**
     * 表格
     */
    Table(0, "表格", "表格", "Table"),
    /**
     * 看板
     */
    Billboard(1, "看板", "看板", "Billboard"),
    /**
     * 相册
     */
    Album(2, "相册", "相冊", "Album"),
    /**
     * 主从表
     */
    MasterSlave(3, "主从表", "主從表", "Master Slave"),
    /**
     * 树表
     */
    Tree(4, "树表", "樹表", "Tree"),
    /***
     * 组合表
     */
    Combo(5, "组合表", "组合表", "Combo"),
    /***
     * 图表
     */
    Chart(6, "图表", "圖表", "Chart"),
    /***
     * 甘特图
     */
    Gantt(7, "甘特图", "甘特圖", "Gantt"),
    /**
     * 日历
     */
    Calendar(8, "日历", "日歷", "Calendar");
    /**
     * 枚举值
     */
    private final int id;
    /**
     * 简体中文名
     */
    private final String name;
    /**
     * 英文名
     */
    private final String enName;
    /**
     * 繁体中文名
     */
    private final String hkName;

    ListTypeEnum(int id, String name, String hkName, String enName) {
        this.id = id;
        this.name = name;
        this.enName = enName;
        this.hkName = hkName;
    }

    /***
     * 根据ID值返回对应的枚举类型
     * @param id 值
     * @return com.ezlcp.commons.constant.ListTypeEnum
     * 0表格1看板2相册3主从表4树表5组合表6图表7甘特图8日历
     */
    public static ListTypeEnum valueOf(int id) {
        switch (id) {
            case 1 -> {
                return Billboard;
            }
            case 2 -> {
                return Album;
            }
            case 3 -> {
                return MasterSlave;
            }
            case 4 -> {
                return Tree;
            }
            case 5 -> {
                return Combo;
            }
            case 6 -> {
                return Chart;
            }
            case 7 -> {
                return Gantt;
            }
            case 8 -> {
                return Calendar;
            }
            default -> {
                return Table;
            }
        }
    }

    /**
     * 枚举值
     */
    public int getId() {
        return id;
    }

    /**
     * 简体中文名
     */
    public String getName() {
        return name;
    }

    /**
     * 英文名
     */
    public String getEnName() {
        return enName;
    }

    /**
     * 繁体中文名
     */
    public String getHkName() {
        return hkName;
    }

    /***
     * @description 转换成JSON对象
     * @return com.alibaba.fastjson2.JSONObject
     * @author Elwin ZHANG
     * @date 2023/3/16 14:41
     */
    public JSONObject toJsonObject() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", id);
        jsonObject.put("name", name);
        jsonObject.put("enName", enName);
        jsonObject.put("hkName", hkName);
        return jsonObject;
    }
}
