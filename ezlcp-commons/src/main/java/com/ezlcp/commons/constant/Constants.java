package com.ezlcp.commons.constant;

public interface Constants {

    short SHORT0 = 0;
    short SHORT1 = 1;

    String TOKEN = "token";
    String Authorization = "Authorization";
    String CAPTCHA_KEY = "vcode";
    String COL_ID="id";
    String COL_TENANT_ID = "tenant_id";
    String COL_GROUP_TYPE = "group_type";
    String COL_GROUP_ID = "group_id";
    String COL_STATUS = "status";
    String COL_USER_ID = "user_id";
    String COL_MENU_ID = "menu_id";
    String COL_SEQ = "seq";
    String COL_PARENT_ID = "parent_id";
    String COL_MODULE_ID = "module_id";
    String COL_CREATE_BY= "create_by";
    String COL_CREATE_TIME = "create_time";
    String COL_UPDATE_BY= "update_by";
    String COL_UPDATE_TIME = "update_time";
    String COL_IS_PUBLIC = "is_public";
    String COL_DS_ID = "ds_id";
    String COL_DS_NAME = "ds_name";
}
