package com.ezlcp.commons.constant;

/**
 * 控件类型枚举
 *
 * @author Elwin ZHANG
 * @date 2024/6/21 13:25
 */
public enum ControlTypeEnum {

    /*
     *单行文本
     */
    input,
    textarea,
    /*
     *数字输入
     */
    number,
    switchButton,
    date,
    datetime,
    time,
    select,
    /*
     *评分
     */
    rate,
    radio,
    checkbox,
    /*
     *文件上传
     */
    upload,
    /*
     *颜色选择
     */
    color,
    /*
     *人员选择
     */
    person,
    /*
     *用户组选择
     */
    userGroup,
    /*
     *引用实体
     */
    refEntity,
    /*
     *单据编号
     */
    orderNo,
    /*
     *数据字典
     */
    dictionary;

    /**
    * 获取枚举对象的值
    */
    public String getValue() {
        String name=this.name();
        if("switchButton".equals(name)){
            return "switch";
        }else {
            return name;
        }
    }

    ControlTypeEnum() {

    }
}
