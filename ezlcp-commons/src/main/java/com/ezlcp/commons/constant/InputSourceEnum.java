package com.ezlcp.commons.constant;

/**
 * @author Elwin ZHANG
 * @description: 控件输入来源。当前用户Id,这些属于默认值
 * @date 2023/10/24 13:43
 */
public enum InputSourceEnum {
    /***
     * 手工输入
     */
    input,
    /***
     * 数据字典
     */
    dict,
    /***
     * 计算公式
     */
    expression,
    /***
     * 自动生成，像内置字段,序列号
     */
    auto;


    InputSourceEnum() {

    }


}
