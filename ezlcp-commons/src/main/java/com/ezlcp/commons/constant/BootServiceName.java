package com.ezlcp.commons.constant;

/**
 * @author Elwin ZHANG
 * @description: 微服务名称常量<br/>
 * @date 2023/1/16 16:13
 */
public interface BootServiceName {
    /***
    * @description 系统及用户服务
    */
    String USER_SERVICE="EzLcp-User";

    /***
     * @description 日志服务
     */
    String LOG_SERVICE="EzLcp-Log";

    /***
     * @description  表单、列表服务
     */
    String FORM_SERVICE="EzLcp-Form";

    /***
     * @description 流程服务
     */
    String FLOW_SERVICE="EzLcp-Flow";
}
