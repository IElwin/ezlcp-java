package com.ezlcp.commons.model;

import com.ezlcp.commons.tool.StringUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ezlcp.commons.base.entity.IUser;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * 平台的用户实体
 */

@Setter
@Getter
public class SysUser implements  IUser {

    private static final long serialVersionUID = 1L;

    public SysUser(){

    }

    /**
     * 用户id
     */
    private String userId;
    /**
     * 账号
     */
    private String account;

    /**
    * 姓名
    */
    private String fullName;

    /**
     * 租户ID
     */
    private String tenantId;

    /**
     * 密码
     */
    private String password;
    /**
     * 邮箱
     */
    private String email;

    /**
     * 手机
     */
    private String mobile;

    /**
     * 拥有角色
     */
    private List<String> roles;

    private String rootPath;

    /**
     * 状态
     */
    private String status;
    /**
     * 头像
     */
    private String photo;
    /**
     * 用户类型
     */
    private String userType="";
    /**
     * 语言
     */
    private String language;
    /**
     * 是否锁定，1为锁定
     */
    private Short isLock;
    /**
     * 性别，1为男性
     */
    private Short gender;
    /**
     * 用户编号
     */
    private String userCode;
    /**
     * IP地址
     */
    private String ipAddr;

    /***
    * @description  登录时request.getHeader("User-Agent")值
    */
    private String userAgent;
    /**
    * token缓存多少分钟
    */
    private int cacheSeconds;

    /***
    * 缓存的时刻：System.currentTimeMillis()
    */
    private long timeMillis;

    /***
     * 登录日志ID
     */
    private String englishName;

    /***
    * @description  客户端类型，默认为0。 0.PC 1.H5 2.安卓 4.IOS
    */
    private int clientType;

    /***
     * @description 是否为管理员类型账户
     */
    public boolean isAdmin(){
        return "Admin".equals(userType);
    }
    /***
     * @description  是否平台用户
     */
    public boolean isPlatformUser(){
        return StringUtils.isEmpty(tenantId);
    }
    /***
     * @description  是否公司/租户用户
     */
    public boolean isTenantUser(){
        return !isPlatformUser();
    }
    /***
     * @description  是否公司的普通员工
     */
    public boolean isStaff(){
        return (isTenantUser() && !isAdmin());
    }
    /***
     * @description  是否平台超级管理员
     */
    public boolean isSuperAdmin(){
        return (isAdmin() && isPlatformUser());
    }

    public String getUsername() {
        return this.account;
    }

    public boolean isEnabled() {
        if(IUser.STATUS_ENABLED.equals(getStatus())){
            return true;
        }else {
            return false;
        }
    }

}
