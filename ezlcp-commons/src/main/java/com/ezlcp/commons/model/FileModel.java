package com.ezlcp.commons.model;

/**
 * @author Elwin ZHANG
 * @description: 上传文件对象模型<br />
 * @date 2022/5/16 9:23
 */
public class FileModel {

    private String relPath = "";

    private String fileName = "";

    private String extName = "";

    private byte[] bytes;

    public String getRelPath() {
        return relPath;
    }

    public void setRelPath(String relPath) {
        this.relPath = relPath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getExtName() {
        return extName;
    }

    public void setExtName(String extName) {
        this.extName = extName;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

}