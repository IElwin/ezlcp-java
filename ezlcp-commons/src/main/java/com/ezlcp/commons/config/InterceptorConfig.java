package com.ezlcp.commons.config;

import com.alibaba.fastjson2.JSONReader;
import com.alibaba.fastjson2.JSONWriter;
import com.alibaba.fastjson2.support.config.FastJsonConfig;
import com.alibaba.fastjson2.support.spring6.http.converter.FastJsonHttpMessageConverter;
import com.ezlcp.commons.filter.WebContextFilter;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

    /***
     * @description: 添加Spring MVC 拦截器
     * @author Elwin ZHANG
     * @date 2022/5/7 10:49
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new WebContextFilter()).addPathPatterns("/**");
    }

    //保证StringHttpMessageConverter在FastJsonHttpMessageConverter前被调用
    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.clear();
        StringHttpMessageConverter converter = new StringHttpMessageConverter(
                Charset.forName("UTF-8"));
        converters.add(converter);
        converters.add(getFastJsonHttpMessageConverter());
    }

    public FastJsonHttpMessageConverter getFastJsonHttpMessageConverter() {
        FastJsonHttpMessageConverter converter = new FastJsonHttpMessageConverter();
        FastJsonConfig config = new FastJsonConfig();
        //WriteMapNullValue——–是否输出值为null的字段,默认为false
        //WriteNullNumberAsZero—-数值字段如果为null,输出为0,而非null
        //WriteNullListAsEmpty—–List字段如果为null,输出为[],而非null
        //WriteNullStringAsEmpty—字符类型字段如果为null,输出为”“,而非null
        config.setWriterFeatures(
                JSONWriter.Feature.WriteNullListAsEmpty,
                JSONWriter.Feature.WriteNullBooleanAsFalse,
                JSONWriter.Feature.WriteNullNumberAsZero,
                JSONWriter.Feature.BrowserCompatible,
                JSONWriter.Feature.BrowserSecure
        );
        config.setReaderFeatures(
                JSONReader.Feature.AllowUnQuotedFieldNames,
                JSONReader.Feature.NonZeroNumberCastToBooleanAsTrue,
                JSONReader.Feature.TrimString
        );
        converter.setFastJsonConfig(config);
        List<MediaType> mediaTypes = new ArrayList<>(8);
        mediaTypes.add(MediaType.APPLICATION_JSON_UTF8);
        mediaTypes.add(MediaType.MULTIPART_FORM_DATA);
        mediaTypes.add(MediaType.APPLICATION_FORM_URLENCODED);
        mediaTypes.add(MediaType.APPLICATION_JSON);
        mediaTypes.add(MediaType.APPLICATION_OCTET_STREAM);
        converter.setSupportedMediaTypes(mediaTypes);
        return converter;
    }
}