package com.ezlcp.commons.tool;

import java.math.RoundingMode;
import java.text.NumberFormat;

/**
 * @author Elwin ZHANG
 * @description: 数值类型工具类 <br/>
 * @date 2022/7/4 17:15
 */
public class NumberUtil {
    /***
    * @description  格式化数值（加千分位，）
    * @param number 浮点数或金额
    * @param precision 保留几位小数
    * @return java.lang.String
    * @author Elwin ZHANG
    * @date 2022/7/4 17:22
    */
    public static String format(Object number,int precision){
        NumberFormat format=NumberFormat.getNumberInstance();
        format.setRoundingMode(RoundingMode.HALF_UP);
        format.setMaximumFractionDigits(precision);
        format.setMinimumFractionDigits(precision);
        return format.format(number);
    }
}
