package com.ezlcp.commons.tool;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;

/**
 * 用于Response的返回类型
 */
@Data
@AllArgsConstructor
public class Result<T> extends HashMap<String, Object> implements Serializable {


    public static <T> Result<T> succeed(String msg) {
        return succeedWith(null, com.ezlcp.commons.tool.CodeEnum.SUCCESS.getCode(), msg);
    }

    public static <T> Result<T> succeed(T model, String msg) {
        return succeedWith(model, com.ezlcp.commons.tool.CodeEnum.SUCCESS.getCode(), msg);
    }

    public static <T> Result<T> succeed(T model) {
        return succeedWith(model, com.ezlcp.commons.tool.CodeEnum.SUCCESS.getCode(), "");
    }

    public static <T> Result<T> succeedWith(T data, Integer code, String msg) {
        Result<T> r = new Result<T>();
        r.put("code", code);
        r.put("msg", msg);
        r.put("data", data);
        return r;
    }

    public static <T> Result<T> failed(String msg) {
        return failedWith(null, com.ezlcp.commons.tool.CodeEnum.ERROR.getCode(), msg);
    }

    public static <T> Result<T> failed(T model, String msg) {
        return failedWith(model, com.ezlcp.commons.tool.CodeEnum.ERROR.getCode(), msg);
    }

    public static <T> Result<T> failedWith(T data, Integer code, String msg) {
        Result<T> r = new Result<T>();
        r.put("code", code);
        r.put("msg", msg);
        r.put("data", data);
        return r;
    }

    public String getCode(){
        return this.get("code").toString();
    }

    @Override
    public Result put(String key, Object value) {
        super.put(key, value);
        return this;
    }

}
