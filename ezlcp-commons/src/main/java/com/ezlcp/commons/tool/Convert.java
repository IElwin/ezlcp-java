package com.ezlcp.commons.tool;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * 数据转换工具类，来自网络
 */
public class Convert {

    /**
     * 功能：判断的输入字符串是否是空
     *<br> @param str 输入的字符串
     *<br> @return
     *<br> 创建时间：2018年10月8日 下午2:03:06
     * @author Elwin ZHANG
     * @version 1.0
     */
    public static boolean isEmpty(String str) {
        if (str==null || str.length()==0) {
            return true;
        }
        return false;
    }

    /**
     * 功能：简单对象转成字符串
     *<br> @param object 对象
     *<br> @return 字符串
     *<br> 创建时间：2018年10月15日 下午2:48:03
     * @author Elwin ZHANG
     * @version 1.0
     */
    public static String object2String(Object object) {
        //空
        if(object==null) {
            return "";
        }
        //日期类型
        if(object instanceof Date) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return sdf.format(new Date());
        }
        return object.toString();
    }
    /**
     * long转int
     *
     * @param lnum
     * @param defaultValue
     * @author fangjian
     * @return
     */
    public static int LongToInt(long lnum, int defaultValue) {
        int Result = defaultValue;
        try {
            Result = new Long(lnum).intValue();
        } catch (Exception localException) {

        }
        return Result;
    }


    /**
     * 字符串转int
     *
     * @param str
     * @param defaultValue
     * @return
     */
    public static int strToInt(String str, int defaultValue) {
        int Result = defaultValue;
        try {
            Result = Integer.parseInt(str);
        } catch (Exception localException) {
        }
        return Result;
    }

    /**
     * 字符串转long类型
     *
     * @param str
     * @param defaultValue
     * @return
     */
    public static long strToLong(String str, long defaultValue) {
        long Result = defaultValue;
        try {
            Result = Long.parseLong(str);
        } catch (Exception localException) {
        }
        return Result;
    }

    /**
     * 字符串转float类型
     *
     * @param str
     * @param defaultValue
     * @return
     */
    public static float strToFloat(String str, float defaultValue) {
        float Result = defaultValue;
        try {
            Result = Float.parseFloat(str);
        } catch (Exception localException) {
        }
        return Result;
    }

    /**
     * 字符串转double类型
     *
     * @param str
     * @param defaultValue
     * @return
     */
    public static double strToDouble(String str, double defaultValue) {
        double Result = defaultValue;
        try {
            Result = Double.parseDouble(str);
        } catch (Exception localException) {
        }
        return Result;
    }

    /**
     * 字符串转boolean类型
     *
     * @param str
     * @param defaultValue
     * @return
     */
    public static boolean strToBoolean(String str, boolean defaultValue) {
        boolean Result = defaultValue;
        try {
            Result = Boolean.parseBoolean(str);
        } catch (Exception localException) {
        }
        return Result;
    }

    /**
     * 字符串转日期类型yyyy-MM-dd HH:mm:ss
     *
     * @param str
     * @param defaultValue
     * @return
     */
    public static Date strToDate(String str, Date defaultValue) {
        return strToDate(str, "yyyy-MM-dd HH:mm:ss", defaultValue);
    }

    /**
     * 字符串根据格式转日期
     *
     * @param str
     * @param format
     * @param defaultValue
     * @return
     */
    public static Date strToDate(String str, String format, Date defaultValue) {
        Date Result = defaultValue;
        SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.ENGLISH);
        try {
            Result = formatter.parse(str);
        } catch (Exception localException) {
        }
        return Result;
    }

    /**
     * 日期长时间格式转字符串
     *
     * @param date
     * @param defaultValue
     * @return
     */
    public static String dateToStr(Date date, String defaultValue) {
        return dateToStr(date, "yyyy-MM-dd HH:mm:ss", defaultValue);
    }

    /**
     * 日期根据格式转字符串
     *
     * @param date
     * @param format
     * @param defaultValue
     * @return
     */
    public static String dateToStr(Date date, String format, String defaultValue) {
        String Result = defaultValue;
        SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.CHINA);
        try {
            Result = formatter.format(date);
        } catch (Exception localException) {
        }
        return Result;
    }

    /**
     * 判断是否为空，给默认值
     *
     * @param str
     * @param defaultValue
     * @return
     */
    public static String strToStr(String str, String defaultValue) {
        String Result = defaultValue;

        if ((str != null) && (!(str.isEmpty()))) {
            Result = str;
        }

        return Result;
    }

    public static java.sql.Date dateToSqlDate(Date date) {
        return new java.sql.Date(date.getTime());
    }

    public static Date sqlDateToDate(java.sql.Date date) {
        return new Date(date.getTime());
    }

    public static Timestamp dateToSqlTimestamp(Date date) {
        return new Timestamp(date.getTime());
    }

    public static Date qlTimestampToDate(Timestamp date) {
        return new Date(date.getTime());
    }

    public static int strtoAsc(String st) {
        byte[] gc = st.getBytes();
        int asnum = gc[0];
        return asnum;
    }

    public static char intToChar(int backnum) {
        char stchar = (char) backnum;
        return stchar;
    }
}