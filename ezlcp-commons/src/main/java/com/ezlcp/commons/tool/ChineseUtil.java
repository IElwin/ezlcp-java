package com.ezlcp.commons.tool;

import com.github.stuxuhai.jpinyin.ChineseHelper;
import com.github.stuxuhai.jpinyin.PinyinException;
import com.github.stuxuhai.jpinyin.PinyinFormat;
import com.github.stuxuhai.jpinyin.PinyinHelper;

/**
 * @author Elwin ZHANG
 * @description: 中文工具<br />
 * @date 2023/3/20 16:10
 */
public class ChineseUtil {
    /***
     * @description 转换汉字为拼音不带声调
     * @param text 汉字
     * @param split 拼音之间的分隔符，默认为空格
     * @return java.lang.String
     * @author Elwin ZHANG
     * @date 2023/3/20 16:33
     */
    public static String toPinyinNoTone(String text, String split) {
        if (StringUtils.isEmpty(split)) {
            split = " ";
        }
        try {
            return PinyinHelper.convertToPinyinString(text, split, PinyinFormat.WITHOUT_TONE);
        } catch (PinyinException e) {
            return "";
        }
    }

    /***
     * @description 转换汉字为拼音带声调
     * @param text 汉字
     * @param split 拼音之间的分隔符，默认为空格
     * @return java.lang.String
     * @author Elwin ZHANG
     * @date 2023/3/20 16:33
     */
    public static String toPinyinWithToneMark(String text, String split) {
        if (StringUtils.isEmpty(split)) {
            split = " ";
        }
        try {
            return PinyinHelper.convertToPinyinString(text, split, PinyinFormat.WITH_TONE_MARK);
        } catch (PinyinException e) {
            return "";
        }
    }

    /***
     * @description 转换汉字为拼音用数字表示声调
     * @param text 汉字
     * @param split 拼音之间的分隔符，默认为空格
     * @return java.lang.String
     * @author Elwin ZHANG
     * @date 2023/3/20 16:33
     */
    public static String toPinyinWithToneNo(String text, String split) {
        if (StringUtils.isEmpty(split)) {
            split = " ";
        }
        try {
            return PinyinHelper.convertToPinyinString(text, split, PinyinFormat.WITH_TONE_NUMBER);
        } catch (PinyinException e) {
            return "";
        }
    }

    /***
     * @description 获取中文的拼音首字母
     * @param text 中文
     * @return java.lang.String
     * @author Elwin ZHANG
     * @date 2023/3/20 16:33
     */
    public static String toFirstLetters(String text) {
        try {
            return PinyinHelper.getShortPinyin(text);
        } catch (PinyinException e) {
            return "";
        }
    }

    /***
     * @description 转换中为为简体中文
     * @param text 原中文
     * @return java.lang.String
     * @author Elwin ZHANG
     * @date 2023/3/20 16:50
     */
    public static String toSimplified(String text) {
        return ChineseHelper.convertToSimplifiedChinese(text);
    }

    /***
     * @description 转换中为为繁体中文
     * @param text 原中文
     * @return java.lang.String
     * @author Elwin ZHANG
     * @date 2023/3/20 16:50
     */
    public static String toTraditional(String text) {
        return ChineseHelper.convertToTraditionalChinese(text);
    }

    public static void main(String[] args) {
        String text = "我是中国人so2我爱五星红旗！";

        System.out.println("1." + toFirstLetters(text));
        System.out.println("2." + toPinyinNoTone(text, null));
        System.out.println("3." + toPinyinWithToneMark(text, ""));
        System.out.println("4." + toPinyinWithToneNo(text, ","));
        System.out.println("a." + toTraditional(text));
        String text2 = "傳說中的神異動物，身長，有鱗爪，ds3能興雲降雨。";
        System.out.println("b." + toSimplified(text2));
    }
}
