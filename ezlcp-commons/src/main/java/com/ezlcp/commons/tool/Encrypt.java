package com.ezlcp.commons.tool;

import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.symmetric.AES;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 * 功能：加密、解密相关的操作 <br>
 * 创建时间：2018年9月26日 下午4:19:01
 *
 * @author Elwin ZHANG
 * @version 1.0
 */
public class Encrypt {

    public static final String ALG_HS256 = "HmacSHA256";

    /**
     * token参数名
     */
    public static final String TOKEN_PARAM_NAME = "token";

    /**
     * signature参数名
     */
    public static final String SIGN_PARAM_NAME = "signature";

    /***
     * @description AES加密
     * @param source 原字符串（明文）
     * @param key 密钥（长度要为16/20/24/28/32字节）
     * @return java.lang.String 加密后的 十六进制字符串
     * @author Elwin ZHANG
     * @date 2023/6/5 17:31
     */
    public static String aesEncode(String source, byte[] key) {
        try {
            //构建
            AES aes = SecureUtil.aes(key);
            //加密为16进制表示
            return aes.encryptHex(source);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return "";
        }
    }

    /***
     * @description AES解密
     * @param text 密文
     * @param key 密钥（长度要为16/20/24/28/32字节）
     * @return java.lang.String 原文
     * @author Elwin ZHANG
     * @date 2023/6/5 17:31
     */
    public static String aesDecode(String text, byte[] key) {
        try {
            //构建
            AES aes = SecureUtil.aes(key);
            //加密16进制字符串
            return aes.decryptStr(text);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return "";
        }
    }

    /**
     * 功能：用HmacSHA256算法计算签名 <br>
     * 创建时间：2018年9月26日 下午5:36:40
     *
     * @param secretKey 密钥 <br>
     * @param content   加密内容 <br>
     * @return 签名字符串 <br>
     * @author Elwin ZHANG
     * @version 1.0
     */
    public static String getHS256Signature(String secretKey, String content) {
        Mac sha256_HMAC;
        try {
            sha256_HMAC = Mac.getInstance(ALG_HS256);

            SecretKeySpec secret_key = new SecretKeySpec(secretKey.getBytes("utf-8"), ALG_HS256);
            sha256_HMAC.init(secret_key);
            Base64.Encoder encoder = Base64.getEncoder();

            String hash = encoder.encodeToString(sha256_HMAC.doFinal(content.getBytes("ascii")));
            return hash.replaceAll("[\\s*\t\n\r=]", "");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * 功能：计算Map中的已有参数计算签名并将Token和签名添加到Map，注意用TreeMap是为了自动排序，先不要将token的值加入到map中 <br>
     *
     * @param token 令牌 <br>
     * @param map   原有的参数Map <br>
     * @return 增加了token和签名的Map <br>
     * 创建时间：2018年10月10日 下午5:21:52
     * @author Elwin ZHANG
     * @version 1.0
     */
    public static TreeMap<String, String> addSignature2Map(String token, TreeMap<String, String> map) {
        if (Convert.isEmpty(token) || map == null) {
            return null;
        }
        // 将参数值按顺序拼接起来构成要签名的内容
        String content = "";
        Iterator<Entry<String, String>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Entry<String, String> entry = it.next();
            content += entry.getValue();
        }
        // 计算签名
        String sign = Encrypt.getHS256Signature(token, content);

        //增加令牌和签名到MAP
        map.put(TOKEN_PARAM_NAME, token);
        map.put(SIGN_PARAM_NAME, sign);
        return map;
    }

    /**
     * 功能：获得后台用户的密码，升级为防复制粘贴密码<br>
     * 创建时间：2017年11月23日 上午11:48:13
     *
     * @param id       记录ID<br>
     * @param password 前端传入的密码<br>
     * @return 加密后的密码<br>
     * @author Elwin ZHANG <br>
     */
    public static String getMd5Password(String id, String password) {
        // 输入无效则返回NULL
        if (password == null || password.length() == 0) {
            return null;
        }
        if (com.ezlcp.commons.tool.StringUtils.isEmpty(id)) {
            return null;
        }
        // 用户ID作为盐加入
        String plainText = id + "@Ez-" + password.toLowerCase();
        String result = MD5(plainText);
        // 密文再做一下变换
        return result.substring(5) + result.substring(0, 5);
    }

    public static String MD5(String input) {
        return MD5(input, Charset.defaultCharset());
    }

    public static String MD5(String input, String charset) {
        return MD5(input, Charset.forName(charset));
    }

    public static String MD5(String input, Charset charset) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        md.update(input.getBytes(charset));

        char[] hexDigits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        byte[] tmp = md.digest();
        char[] str = new char[32];

        int k = 0;
        for (int i = 0; i < 16; ++i) {
            byte byte0 = tmp[i];
            str[(k++)] = hexDigits[(byte0 >>> 4 & 0xF)];
            str[(k++)] = hexDigits[(byte0 & 0xF)];
        }

        String result = new String(str);

        return result;
    }

    /**
     * 功能：通过明文密码客户端第一次加密的密码 <br>
     *
     * @param passwd 明文密码 <br>
     * @return 客户端的密文 <br>
     * 创建时间：2018年10月9日 下午6:06:37
     * @author Elwin ZHANG
     * @version 1.0
     */
    public static String getFirstPassword(String passwd) {
        // 输入无效则返回NULL
        if (passwd == null || passwd.length() == 0) {
            return null;
        }
        String pw = passwd + "$1#.@Ez";
        return MD5(pw).toLowerCase();
    }

    /**
     * 功能：获取BASE64字符串 <br>
     *
     * @param content 明文 <br>
     * @return 密文 <br>
     * 创建时间：2018年9月30日 上午10:24:17
     * @author Elwin ZHANG
     * @version 1.0
     */
    public static String getBase64String(String content) {
        try {
            Base64.Encoder encoder = Base64.getEncoder();
            String hash = encoder.encodeToString(content.getBytes("ascii"));
            return hash.replaceAll("[\\s*\t\n\r=]", "");
        } catch (Exception e) {
            return content;
        }
    }

    /***
     * @description: 测试密码
     */
    public static void main(String[] args) {
        String password = "123456";
        String id = "1";
        String first = getFirstPassword(password);
        String result = getMd5Password(id, first);
        System.out.println("==============[" + password + "] 前端传入的密文为：" + first);
        System.out.println("==============[" + password + "] 加密后的密文为：" + result);
    }
}
