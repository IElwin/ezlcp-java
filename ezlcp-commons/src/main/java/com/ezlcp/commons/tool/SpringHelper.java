package com.ezlcp.commons.tool;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @author Elwin ZHANG
 * 创建时间：2016年4月14日 上午9:12:13
 * 功能：Spring 工具类，用于获取Spring管理的Bean,用于不方便注入的类调用
 */
@Component
@Slf4j
public class SpringHelper implements ApplicationContextAware {

    // 当前的Spring上下文
    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext arg0)
            throws BeansException {
        applicationContext = arg0;
    }

    /**
     * @param beanName bean Id
     * @return  如果获取失败，则返回Null
     * @author Elwin ZHANG
     * 创建时间：2016年4月14日 上午9:52:55
     * 功能：通过BeanId获取Spring管理的对象
     */
    public  Object getObject(String beanName) {
        Object object = null;
        try {
            object = applicationContext.getBean(beanName);
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
        return object;
    }


    /**
     * @return
     * @author Elwin ZHANG
     * 创建时间：2017年3月7日 下午3:44:38
     * 功能：获取Spring的ApplicationContext
     */
    public ApplicationContext  getContext() {
        return applicationContext;
    }

    /**
     * @param clazz 要获取的Bean类
     * @return  如果获取失败，则返回Null
     * @author Elwin ZHANG
     * 创建时间：2016年4月14日 上午10:05:27
     * 功能：通过类获取Spring管理的对象
     */
    public  <T> T getObject(Class<T> clazz) {
        try {
            return applicationContext.getBean(clazz);
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
        return null;
    }


}
