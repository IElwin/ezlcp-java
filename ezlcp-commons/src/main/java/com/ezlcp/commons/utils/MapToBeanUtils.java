package com.ezlcp.commons.utils;

import com.alibaba.fastjson2.JSON;

import java.util.Map;

public class MapToBeanUtils {
    /**
     * map转实体类
     *
     * @param map
     * @param entityClass
     * @return
     */
    public static <T> T mapToBean(Map<String, Object> map, Class<T> entityClass) {
        return JSON.parseObject(JSON.toJSONString(map), entityClass);
    }
}
