package com.ezlcp.commons.utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * 图片处理工具类。
 *
 */
public class ImageUtil {
    /**
     * 将图片生成缩略图。
     * @param src
     * @param w
     * @param h
     * @param format
     * @return
     */
    public static byte[] thumbnailImage(byte[] src, int w, int h,String format){
        try {
            ByteArrayInputStream stream=new ByteArrayInputStream(src);
            Image img = ImageIO.read(stream);

            int width = img.getWidth(null);
            int height = img.getHeight(null);

            if(width>w || height >h){
                if(width>height){
                    h= Integer.parseInt(new java.text.DecimalFormat("0").format( (w*1.0) * (height*1.0 / width* 1.0)));
                }
                else{
                    w= Integer.parseInt(new java.text.DecimalFormat("0").format( (h*1.0) * (width*1.0 / height* 1.0)));
                }
            }
            else{
                stream.close();
                return null;
            }


            BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
            Graphics g = bi.getGraphics();
            g.drawImage(img, 0, 0, w, h, Color.LIGHT_GRAY, null);
            g.dispose();

            ByteArrayOutputStream out=new ByteArrayOutputStream();
            ImageIO.write(bi, format, out);

            stream.close();

            byte[] rtn= out.toByteArray();
            return rtn;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }
}
