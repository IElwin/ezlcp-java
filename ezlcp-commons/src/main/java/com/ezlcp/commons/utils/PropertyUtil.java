package com.ezlcp.commons.utils;

import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @author Weixuan LONG
 * @description: 借助spring读取Properties文件
 * @date 2022/8/11 17:26
 */
public class PropertyUtil {
    /***
     * @description: 读取properties文件
     * @param fileName properties文件名及所在路径
     * @return java.util.Map<java.lang.String, java.lang.String>
     * @author Weixuan LONG
     * 1.传递的参数不是properties类型文件，不会报错，返回的是空Map；
     * 2.传递的参数是根本不存在的properties文件，也不会报错，返回的是空Map；
     * 3.传递的参数可以带路径，可以正常解析到
     * @date 2022/8/11 17:26
     */
    public static Map<String, String> readProperties(String fileName) {
        Map<String, String> resultMap = new HashMap<>();
        try {
            Properties props = PropertiesLoaderUtils.loadAllProperties(fileName);
            for (Object key : props.keySet()) {
                resultMap.put(key.toString(), props.get(key).toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resultMap;
    }
}