package com.ezlcp.commons.utils;

import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.config.ConfigService;
import com.ezlcp.commons.tool.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Properties;

/**
 * @author Elwin ZHANG
 * @description: Nacos工具类<br />
 * @date 2022/12/22 10:08
 */
@Component
@Slf4j
public class NacosUtils {

    /***
     * Nacos中自定义数据源的配置KEY
     */
    public static final String KEY_OF_DATASOURCE = "DataSources.json";

    /***
     * Nacos读超时时间（毫秒）
     */
    public static final long READ_TIME_OUT_MS = 1000 * 60 * 5;

    @Value("${spring.cloud.nacos.config.server-addr:localhost}")
    private String serverAddr;

    @Value("${spring.cloud.nacos.config.namespace:default}")
    private String nameSpace;

    /***
     * @description 关闭Nacos的配置服务
     * @param service 服务对象
     * @author Elwin ZHANG
     * @date 2022/12/22 10:57
     */
    public void closeService(ConfigService service) {
        try {
            service.shutDown();
        } catch (Exception e) {
            log.info("========关闭Nacos的配置服务时出错：");
            log.error(e.getMessage(), e);
        }
    }

    /***
     * @description 获取Nacos的配置服务
     * @return com.alibaba.nacos.api.config.ConfigService
     * @author Elwin ZHANG
     * @date 2022/12/22 10:27
     */
    public ConfigService getConfigService(String prefix) {
        Properties properties = new Properties();
        properties.put("serverAddr", serverAddr);
        properties.put("namespace", nameSpace);
        if (StringUtils.isNotEmpty(prefix)) {
            properties.put("prefix", prefix);
        }
        try {
            return NacosFactory.createConfigService(properties);
        } catch (Exception e) {
            log.info("========获取Nacos的配置服务时出错：");
            log.error(e.getMessage(), e);
        }
        return null;
    }
}
