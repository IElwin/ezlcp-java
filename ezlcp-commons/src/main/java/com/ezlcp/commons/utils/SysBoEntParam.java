package com.ezlcp.commons.utils;

/**
 * SysBo相关配置参数
 *
 */
public class SysBoEntParam {
    public final static String SQL_FK="REF_ID_";

    public final static String SQL_PATH="PATH_";

    public final static String SQL_FK_STATEMENT="#{fk}";

    public final static String SQL_PK="id";

    public final static String COMPLEX_NAME="_NAME";

    public static  final String SUB_PRE="SUB_";


    public static  final String FIELD_INST="INST_ID_";

    public static  final String FIELD_INST_STATUS_="INST_STATUS_";

    public static  final String FIELD_CREATE_BY="create_by";

    public static  final String FIELD_CREATE_USER="CREATE_USER_NAME_";

    public static  final String FIELD_CREATE_TIME="create_time";

    public static  final String FIELD_CREATE_DATE="create_date";

    public static  final String FIELD_UPDATE_BY="update_by";

    public static  final String FIELD_UPDATE_USER="UPDATE_USER_NAME_";

    public static  final String FIELD_UPDATE_TIME="update_time";

    public static  final String FIELD_UPDATE_DATE="update_date";

    public static  final String FIELD_GROUP="GROUP_ID_";

    public static  final String FIELD_GROUP_NAME="GROUP_NAME_";

    public static  final String FIELD_PARENT_ID="parent_id";
}
