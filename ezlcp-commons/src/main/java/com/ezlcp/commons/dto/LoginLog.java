package com.ezlcp.commons.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ezlcp.commons.base.entity.BaseExtEntity;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;


/**
 * <pre>
 *
 * 描述：用户登录日志实体类定义
 * 表:login_log
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-01-04 15:33:18
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "login_log")
@Schema(description ="登录日志")
public class LoginLog extends BaseExtEntity<String> {
    @Schema(description = "记录ID")
    @TableId(value = "log_id", type = IdType.INPUT)
	private String logId;
    @Schema(description = "登录名")
    @TableField(value = "login_name", jdbcType = JdbcType.VARCHAR)
    private String loginName;
    @Schema(description = "登录时间")
    @TableField(value = "login_time", jdbcType = JdbcType.TIMESTAMP)
    private java.util.Date loginTime;
    @Schema(description = "IP地址")
    @TableField(value = "ip_addr", jdbcType = JdbcType.VARCHAR)
    private String ipAddr;
    @Schema(description = "用户ID")
    @TableField(value = "user_id", jdbcType = JdbcType.VARCHAR)
    private String userId;
    @Schema(description = "0成功,1失败")
    @TableField(value = "is_fail", jdbcType = JdbcType.NUMERIC)
    private Short isFail;
    @Schema(description = "失败原因")
    @TableField(value = "fail_reason", jdbcType = JdbcType.VARCHAR)
    private String failReason;
    @Schema(description = "登出时间")
    @TableField(value = "logout_time", jdbcType = JdbcType.TIMESTAMP)
    private java.util.Date logoutTime;
    @Schema(description = "客户端类型：0.PC 1.H5 2.安卓 4.IOS")
    @TableField(value = "client_type", jdbcType = JdbcType.VARCHAR)
    private Short clientType;
    @Schema(description = "公司ID")
    @TableField(value = "tenant_id", jdbcType = JdbcType.VARCHAR)
    private String tenantId;

    @JsonCreator
    public LoginLog() {
    }

    @Override
    public String getPkId() {
        return logId;
    }

    @Override
    public void setPkId(String pkId) {
        this.logId = pkId;
    }
}