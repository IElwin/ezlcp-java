package com.ezlcp.commons.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ezlcp.commons.base.entity.BaseExtEntity;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

/**
 * <pre>
 *
 * 描述：自定义的数据库连接实体类定义
 * 表:f_datasource
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-03-28 10:42:47
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "f_datasource")
@Schema(description = "数据源")
public class Datasource extends BaseExtEntity<java.lang.String> {
    @Schema(description = "数据源ID")
    @TableId(value = "ds_id", type = IdType.INPUT)
    private String dsId;
    @Schema(description = "数据库类型")
    @TableField(value = "db_type", jdbcType = JdbcType.VARCHAR)
    private String dbType;
    @Schema(description = "定义")
    @TableField(value = "detail", jdbcType = JdbcType.VARCHAR)
    private String detail;
    @Schema(description = "名称")
    @TableField(value = "ds_name", jdbcType = JdbcType.VARCHAR)
    private String dsName;
    @Schema(description = "标识符")
    @TableField(value = "identifier", jdbcType = JdbcType.VARCHAR)
    private String identifier;
    @Schema(description = "所属模块")
    @TableField(value = "module_id", jdbcType = JdbcType.VARCHAR)
    private String moduleId;
    @Schema(description = "是否公开")
    @TableField(value = "is_public", jdbcType = JdbcType.NUMERIC)
    private Short isPublic;
    @Schema(description = "备注")
    @TableField(value = "remark", jdbcType = JdbcType.VARCHAR)
    private String remark;
    @Schema(description = "修改次数")
    @TableField(value = "seq", jdbcType = JdbcType.NUMERIC)
    private Integer seq;
    @Schema(description = "公司ID")
    @TableField(value = "tenant_id", jdbcType = JdbcType.VARCHAR)
    private String tenantId;
    @Schema(description = "0停用1启用4删除")
    @TableField(value = "status", jdbcType = JdbcType.NUMERIC)
    private Short status;
    @Schema(description = "模块名")
    @TableField(exist = false)
    private String moduleName;
    @Schema(description = "模块繁体名")
    @TableField(exist = false)
    private String hkModuleName;
    @TableField(exist = false)
    @Schema(description = "模块英文名")
    private String enModuleName;

    @JsonCreator
    public Datasource() {
    }

    @Override
    public String getPkId() {
        return dsId;
    }

    @Override
    public void setPkId(String pkId) {
        this.dsId = pkId;
    }
}