package com.ezlcp.commons.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ezlcp.commons.base.entity.BaseExtEntity;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

/**
 * <pre>
 *
 * 描述：系统操作日志实体类定义
 * 表:system_log
 * 作者：Elwin ZHANG
 * 邮箱: elwin2018@foxmail.com
 * 日期:2023-01-04 15:34:38
 * 版权：
 * </pre>
 */
@Setter
@Getter
@Accessors(chain = true)
@TableName(value = "system_log")
@Schema(description ="系统日志")
public class SystemLog extends BaseExtEntity<String> {
    @Schema(description = "记录ID")
    @TableId(value = "log_id", type = IdType.INPUT)
	private String logId;
    @Schema(description = "用户ID")
    @TableField(value = "user_id", jdbcType = JdbcType.VARCHAR)
    private String userId;
    @Schema(description = "用户名")
    @TableField(value = "user_name", jdbcType = JdbcType.VARCHAR)
    private String userName;
    @Schema(description = "模块名")
    @TableField(value = "module", jdbcType = JdbcType.VARCHAR)
    private String module;
    @Schema(description = "操作")
    @TableField(value = "action", jdbcType = JdbcType.VARCHAR)
    private String action;
    @Schema(description = "操作记录ID")
    @TableField(value = "record_id", jdbcType = JdbcType.VARCHAR)
    private String recordId;
    @Schema(description = "操作详情(新旧记录JSON)")
    @TableField(value = "detail", jdbcType = JdbcType.VARCHAR)
    private String detail;
    @Schema(description = "访问页面")
    @TableField(value = "ref_page", jdbcType = JdbcType.VARCHAR)
    private String refPage;
    @Schema(description = "菜单名称")
    @TableField(value = "menu_name", jdbcType = JdbcType.VARCHAR)
    private String menuName;
    @Schema(description = "IP地址")
    @TableField(value = "ip_addr", jdbcType = JdbcType.VARCHAR)
    private String ipAddr;
    @Schema(description = "批次号")
    @TableField(value = "batch_no", jdbcType = JdbcType.VARCHAR)
    private String batchNo;
    @Schema(description = "公司ID")
    @TableField(value = "tenant_id", jdbcType = JdbcType.VARCHAR)
    private String tenantId;

    @JsonCreator
    public SystemLog() {
    }

    @Override
    public String getPkId() {
        return logId;
    }

    @Override
    public void setPkId(String pkId) {
        this.logId = pkId;
    }
}