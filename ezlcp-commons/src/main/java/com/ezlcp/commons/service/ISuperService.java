package com.ezlcp.commons.service;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * service接口父类
 *

 */
public interface ISuperService<T> extends IService<T> {

}
