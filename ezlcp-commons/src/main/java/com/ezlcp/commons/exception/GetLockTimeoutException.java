package com.ezlcp.commons.exception;

/**
 * @author Elwin ZHANG
 * @description: 获取锁超时异常<br/>
 * @date 2022/11/8 14:33
 */
public class GetLockTimeoutException extends  RuntimeException{
    public GetLockTimeoutException(String message){
        super(message);
    }
}
