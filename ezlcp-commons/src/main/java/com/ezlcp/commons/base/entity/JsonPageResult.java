package com.ezlcp.commons.base.entity;

import com.baomidou.mybatisplus.core.metadata.IPage;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 包名：com.ezlcp.commons.base.entity
 * 功能描述：用于页面分页大小
 */
@Data
@Schema(description ="返回前端的分页数据对象")
public class JsonPageResult extends com.ezlcp.commons.base.entity.JsonResult {

    @Schema(description = "分页数据对象")
    com.ezlcp.commons.base.entity.JsonPage result;

    public JsonPageResult() {

    }

    public JsonPageResult(IPage iPage){
        this.result=new com.ezlcp.commons.base.entity.JsonPage(iPage);
    }

    public void setPageData(IPage ipage){
        result=new com.ezlcp.commons.base.entity.JsonPage(ipage);
    }

    /**
     * 生成成功的状态
     * @param msg
     * @return
     */
    public static  JsonPageResult getSuccess(String msg){
        JsonPageResult jsonPageResult=new JsonPageResult();
        jsonPageResult.setSuccess(true);
        jsonPageResult.setCode(SUCESS_CODE);
        jsonPageResult.setMessage(msg);
        jsonPageResult.setShow(false);
        return jsonPageResult;
    }
    /**
     * 生成失败的状态
     * @param msg
     * @return
     */
    public static JsonPageResult getFail(String msg){
        JsonPageResult jsonPageResult=new JsonPageResult();
        jsonPageResult.setSuccess(false);
        jsonPageResult.setMessage(msg);
        jsonPageResult.setCode(FAIL_CODE);
        jsonPageResult.setShow(false);
        return jsonPageResult;
    }
}
