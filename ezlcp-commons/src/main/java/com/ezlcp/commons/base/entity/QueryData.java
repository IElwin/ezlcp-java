package com.ezlcp.commons.base.entity;

 
 
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * 列表查询参数对象
 */
@Data
@Schema(description ="分页查询传入参数")
public class QueryData {
    /**
     * 分页序号
     */
    @Schema(description = "页码/第几页")
    private Integer pageNo;
    /**
     * 分页大小
     */
    @Schema(description = "每页记录数")
    private Integer pageSize;
    /**
     * 排序字段
     */
    @Schema(description = "排序字段名")
    private String sortField;
    /**
     * 排序
     */
    @Schema(description = "升序或降序，asc/desc")
    private String sortOrder;

    /**
     * 映射参数，格式如：ORDER_ID_,Q_ORDER_ID_S_EQ 键值的映射
     */
    @Schema(description = "查询参数(多个)")
    private Map<String,String> params=new HashMap<>();
    /**
     * 模糊查询字段名
     */
    @Schema(description = "模糊查询字段名")
    private String key;

    /**
     * 自定义的数据列表Id
     */
    @Schema(description = "自定义的数据列表Id")
    private String listId;

    /**
     * 前端的语言环境
     */
    @Schema(description = "当前语言")
    private String language;
}
