package com.ezlcp.commons.base.search;

import com.ezlcp.commons.base.entity.SqlModel;

import java.util.List;

/**
 * SQL构建工具类
 *
 */
public class SqlBuilder {
    /**
     * 根据FieldLogic构建SQLModel
     * @param logic
     * @return
     */
    public  static SqlModel build(com.ezlcp.commons.base.search.FieldLogic logic){
        List<WhereParam> whereParams=logic.getWhereParams();

        SqlModel oModel=new SqlModel();

        StringBuilder sb=new StringBuilder();
        int i=0;
        for(WhereParam param:whereParams){
            if(i>0){
                sb.append(" " +logic.getLogic() +" ");
            }
            if(param instanceof  QueryParam){
                QueryParam queryParam=(QueryParam)param;
                sb.append(queryParam.getSql());
                oModel.getParams().put(queryParam.getFieldName() ,queryParam.getValue());
            }
            else {
                com.ezlcp.commons.base.search.FieldLogic fieldLogic=(com.ezlcp.commons.base.search.FieldLogic)param;
                SqlModel model= build(fieldLogic);

                sb.append("("+ model.getSql() +")");
                oModel.getParams().putAll(model.getParams());
            }
            i++;
        }
        oModel.setSql(sb.toString());

        return oModel;

    }

    /**
     * 测试用例
     * @param args
     */
    public static void main(String[] args) {
        QueryParam queryParam1=new  QueryParam("A","=","1");
        QueryParam queryParam2=new  QueryParam("B","=","2");
        QueryParam queryParam3=new  QueryParam("C","=","3");
        QueryParam queryParam4=new  QueryParam("D","=","4");

        QueryParam queryParam5=new  QueryParam("E","=","5");
        QueryParam queryParam6=new  QueryParam("F","=","6");

        com.ezlcp.commons.base.search.FieldLogic logic=new com.ezlcp.commons.base.search.FieldLogic("and");

        com.ezlcp.commons.base.search.FieldLogic logic1=new com.ezlcp.commons.base.search.FieldLogic("or");
        logic1.addParams(queryParam1);
        logic1.addParams(queryParam2);

        com.ezlcp.commons.base.search.FieldLogic logic2=new com.ezlcp.commons.base.search.FieldLogic("or");
        logic2.addParams(queryParam3);
        logic2.addParams(queryParam4);

        com.ezlcp.commons.base.search.FieldLogic logic3=new com.ezlcp.commons.base.search.FieldLogic("AND");
        logic3.addParams(queryParam5);
        logic3.addParams(queryParam6);

        logic2.addParams(logic3);


        logic.addParams(logic1);
        logic.addParams(logic2);

        SqlModel sqlModel= SqlBuilder.build(logic);
        System.out.println(sqlModel.getSql());

    }


}
