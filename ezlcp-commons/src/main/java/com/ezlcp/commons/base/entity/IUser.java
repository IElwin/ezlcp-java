package com.ezlcp.commons.base.entity;

import com.ezlcp.commons.tool.StringUtils;

import java.util.List;

/**
 * 用户接口
 */
public interface IUser {
    /**
     * 平台租户ID
     */
    String ROOT_TENANT_ID = "1";
    /**
     * 是否启用
     */
    String STATUS_ENABLED = "1";
    /**
     * 禁用
     */
    String STATUS_DISABLED = "0";


    /**
     * 用户id
     */
    String getUserId();

    /**
     * 用户id
     */
    void setUserId(String userId);

    /**
     * 账号
     */
    String getAccount();

    /**
     * 账号
     */
    void setAccount(String account);

    /***
    * @description 姓名
    */
    String getFullName();

    void setFullName(String fullName);

    /***
     * @description 英文名
     */
    String getEnglishName();

    void setEnglishName(String englishName);

    String getEmail();

    void setEmail(String email);

    String getMobile();

    void setMobile(String mobile);

    /**
     * 租户ID
     */
    String getTenantId();

    /**
     * 租户ID
     */
    void setTenantId(String tenantId);


    String getPassword();

    void setPassword(String password);

    String getStatus();

    void setStatus(String status);

    /**
     * 默认语言
     */
    String getLanguage();

    /**
     * 默认语言
     */
    void setLanguage(String language);

    /**
     * 是否锁定
     */
    Short getIsLock();

    /**
     * 是否锁定
     */
    void setIsLock(Short isLock);

    /**
     * 账号类型
     */
    String getUserType();

    /**
     * 账号类型
     */
    void setUserType(String userType);


    /**
     * IP
     */
    String getIpAddr();

    /**
     * IP
     */
    void setIpAddr(String IpAddr);

    /***
     * @description 是否为管理员类型账户
     */
    public boolean isAdmin();
    /***
     * @description  是否平台用户
     */
    public boolean isPlatformUser();
    /***
     * @description  是否公司/租户用户
     */
    public boolean isTenantUser();
    /***
     * @description  是否公司的普通员工
     */
    public boolean isStaff();
    /***
     * @description  是否平台超级管理员
     */
    public boolean isSuperAdmin();
    /***
    * @description  缓存request.getHeader("User-Agent")值
    */
    String getUserAgent();
    void setUserAgent(String userAgent);

    List<String> getRoles();

    void setRoles(List<String> roles);
}
