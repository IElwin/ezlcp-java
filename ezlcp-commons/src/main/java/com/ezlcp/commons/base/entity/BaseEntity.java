package com.ezlcp.commons.base.entity;

import java.io.Serializable;

public interface BaseEntity<T>  extends Serializable {

    /**
     * 主键
     * @return
     */
    T  getPkId() ;

    /**
     * 设置主键。
     * @param pkId
     */
    void setPkId(T pkId);
}
