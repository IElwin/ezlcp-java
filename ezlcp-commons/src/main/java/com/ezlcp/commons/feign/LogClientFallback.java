package com.ezlcp.commons.feign;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.ezlcp.commons.dto.LoginLog;
import com.ezlcp.commons.dto.SystemLog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author Elwin ZHANG
 * @description: 日志服务降级<br/>
 * @date 2023/2/13 13:22
 */
@Component
public class LogClientFallback implements LogClient {

    private Logger logger = LoggerFactory.getLogger(LogClientFallback.class);

    /***
     * @description 输出系统日志
     * @param log 系统日志对象
     * @return java.lang.Integer 1为成功，0为失败
     * @author Elwin ZHANG
     * @date 2023/1/17 16:46
     */
    @Override
    public Integer saveSystemLog(SystemLog log) {
        writeLog(log);
        return -1;
    }

    private void writeLog(Object obj){
        logger.warn("=============日志服务调用失败！要记录的日志内容如下：");
        logger.warn(JSON.toJSONString(obj));
    }

    /***
     * @description 写登录日志
     * @param log 登录日志对象
     * @return java.lang.Integer  1为成功，0为失败
     * @author Elwin ZHANG
     * @date 2023/1/17 16:47
     */
    @Override
    public Integer saveLoginLog(LoginLog log) {
        writeLog(log);
        return -1;
    }

    /***
     * @description 写登录日志的登录出时间
     * @param userId 用户ID
     * @param clientType 客户端类型：0.PC 1.H5 2.安卓 4.IOS
     * @return java.lang.Boolean
     * @author Elwin ZHANG
     * @date 2023/1/17 16:47
     */
    @Override
    public boolean logOut(String userId, Short clientType) {
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("userId",userId);
        jsonObject.put("clientType",clientType);
        writeLog(jsonObject);
        return false;
    }
}
