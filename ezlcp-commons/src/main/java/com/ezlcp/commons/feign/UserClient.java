package com.ezlcp.commons.feign;

import com.ezlcp.commons.constant.BootServiceName;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Elwin ZHANG
 * @description: 系统及用户服务<br />
 * @date 2023/3/21 15:59
 */
@FeignClient(name = BootServiceName.USER_SERVICE, fallback = UserClientFallback.class)
public interface UserClient {
    /***
     * @param path 前端的路由
     * @return java.lang.String
     * @description: 根据前端的路由取对应的菜单名
     * @author Elwin ZHANG
     * @date 2023/3/21 16:07
     */
    @GetMapping("/ezlcp/user/menu/getMenuName")
    String getMenuName(@RequestParam(value = "path") String path);
}
