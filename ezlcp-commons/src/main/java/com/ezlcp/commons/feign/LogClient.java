package com.ezlcp.commons.feign;

import com.ezlcp.commons.constant.BootServiceName;
import com.ezlcp.commons.dto.LoginLog;
import com.ezlcp.commons.dto.SystemLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Elwin ZHANG
 * @description: Log微服务调用<br />
 * @date 2023/1/16 16:54
 */
@FeignClient(name = BootServiceName.LOG_SERVICE,fallback = LogClientFallback.class)
public interface LogClient {
    /***
     * @description 输出系统日志
     * @param log 系统日志对象
     * @return java.lang.Integer 1为成功，0为失败
     * @author Elwin ZHANG
     * @date 2023/1/17 16:46
     */
    @RequestMapping("/ezlcp/log/systemLog/insert")
    Integer saveSystemLog(@RequestBody SystemLog log);

    /***
     * @description 写登录日志
     * @param log 登录日志对象
     * @return java.lang.Integer  1为成功，0为失败
     * @author Elwin ZHANG
     * @date 2023/1/17 16:47
     */
    @RequestMapping("/ezlcp/log/loginLog/insert")
    Integer saveLoginLog(@RequestBody LoginLog log);

    /***
     * @description 写登录日志的登录出时间
     * @param userId 用户ID
     * @param clientType 客户端类型：0.PC 1.H5 2.安卓 4.IOS
     * @return java.lang.Boolean
     * @author Elwin ZHANG
     * @date 2023/1/17 16:47
     */
    @GetMapping("/ezlcp/log/loginLog/logout")
    boolean logOut(@RequestParam(value = "userId") String userId, @RequestParam(value = "clientType") Short clientType);
}
