package com.ezlcp.commons.feign;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author Elwin ZHANG
 * @description: 系统及用户服务降级<br />
 * @date 2023/3/21 16:09
 */
@Component
public class UserClientFallback implements UserClient {
    private Logger logger = LoggerFactory.getLogger(UserClientFallback.class);

    /***
     * @param path 前端的路由
     * @return java.lang.String
     * @description: 根据前端的路由取对应的菜单名
     * @author Elwin ZHANG
     * @date 2023/3/21 16:07
     */
    @Override
    public String getMenuName(String path) {
        logger.warn("=============获取菜单名微服务调用失败！参数为：path=" + path);
        return "";
    }
}
