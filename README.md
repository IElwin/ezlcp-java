# <center>易睿低代码平台 EZLCP 后端
### 一. 介绍
Easy Low-Code Platform 低代码开发平台的Java后端，基于Spring boot 3.x 技术(java17+)，使用Spring Cloud 微服务架构开发。除工作流引擎模块外，其他模块已基本上完成开发。

使用以下技术：
```
Spring Boot 3.3.2
Spring Cloud Gateway 
Spring Cloud OpenFeign 
Spring Cloud Alibaba Nacos
mybatis plus 3.5.3.1
knife4j 4.5.0
resilience4j ...
camunda 7.22.0
```
- 产品介绍：[https://gitee.com/IElwin/ezlcp-pc/blob/master/doc/intro/index.md](https://gitee.com/IElwin/ezlcp-pc/blob/master/doc/intro/index.md)

- PC前端项目代码：[https://gitee.com/IElwin/ezlcp-pc](https://gitee.com/IElwin/ezlcp-pc)

### 二、目录
#### 2.1 ezlcp-user 用户及系统相关模块
#### 2.2 ezlcp-log 日志模块
#### 2.3 ezlcp-form 表单模块
#### 2.4 ezlcp-flow 工作流程模块
#### 2.5 ezlcp-gateway 网关模块
#### 2.6 init 数据库初始化脚本、nacos配置导出包


### 三、安全控制
#### 3.1 密码加密
在`s_settings`表中为各租户配置: `用户密码长度，密码强度`。前端的密码在传到后端接口时要加密（md5加盐，与getFirstPassword方法中的盐保持一致），后端接收到前端传来的密文，再调用getMd5Password方法再次加密后再保存到数据库；数据库中的密码是二次加密的，可以防止数据库记录复制密码粘贴到另一条记录（当时放弃使用Spring Security框架除了太重之外，另一个原因就是不能解决密文复制的问题）。

加密方法在com.ezlcp.commons.tool.Encrypt类中，如果重置数据库数据库中某用户的密码，修改main方法中的用户ID和初始密码，运行main方法就可以得到前端要传入的密文，以及保存在数据库中的密文。

#### 3.2 登录控制
在s_settings表中为各租户配置: `登录失败几次锁定账号，锁定时间，空闲多久自动登出，是否启用谷歌身份验证器，以及是否启用IP地址黑名单/白名单`，来增强安全控制，防止非法入侵。

#### 3.3 token
前后端分离，使用token验证客户端，不使用session和cookie。前端调用登录接口后获得token值（后台存在redis中），调用后台的其他接口时，需要将token值加到`Authorization`或`token`请求头参数中。相关微服务中配置拦截器(参考：`com.ezlcp.commons.config.InterceptorConfig`），校验token并将token对应的用户信息缓存在当前线程中，控制器或其他类中可以使用ContextUtil.getCurrentUser()方法，获取当前登录用户的信息。

#### 3.4 网关过滤器
`TokenRequestFilter` 统一拦截前端调用各微服务接口时请求头中是否带`Authorization`或`token`参数；
`XssRequestGlobalFilter` 统一处理请求中的XSS攻击；因为ORM层使用的是Mybatis框架，SQL注入的可能性很小，所以只做XSS字符过滤。
以上两个过滤器都可以在`application.yml`中配置白名单。

### 四、自定义数据源
可以在不同的数据库中创建实体、表单，默认支持`MySQL`/`MariaDB`/`PostgreSQL`/`SQL Server`/`Oracle`这5种数据库。如果想支持其他类型关系型数据库，需要在ezlcp-form等微服务的pom.xml文件中引入相应的jdbc驱动程序，然后在定义数据源时指定`driverClassName`属性值即可。系统使用的是Druid数据源，默认会根据url自动误别`driverClassName`，但有些小众的数据库是无法识别的，比如：达梦，就需要指定jdbc驱动程序类名。

### 五、QQ交流群
群号：259395201