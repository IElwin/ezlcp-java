package com.ezlcp.log.config;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.github.pagehelper.PageInterceptor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

/**
 * @author Elwin ZHANG
 * @description: <br/>
 * @date 2022/6/6 13:58
 */
@Configuration
public class MybatisPlusConfig {
    //private final boolean showLog = true;
    private final String GLOBAL_DATASOURCE = "dataSource";

    /**
     * 新多租户插件配置,一缓和二缓遵循mybatis的规则,需要设置 MybatisConfiguration#useDeprecatedExecutor = false 避免缓存万一出现问题
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();

        // 如果用了分页插件注意先 add TenantLineInnerInterceptor 再 add PaginationInnerInterceptor
        // 用了分页插件必须设置 MybatisConfiguration#useDeprecatedExecutor = false
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor());
        return interceptor;
    }

    @Bean
    public PageInterceptor pageInterceptor() {
        return new PageInterceptor();
    }

    @Bean(value = "localJdbcTemplate")
    public JdbcTemplate registerJdbcTemplate(@Qualifier(GLOBAL_DATASOURCE) DataSource dataSource) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate();
        jdbcTemplate.setDataSource(dataSource);
        return jdbcTemplate;
    }

    /**
     * 解决使用seata 后分页的问题。
     *
     * @Bean public SqlSessionFactory sqlSessionFactory(@Qualifier(GLOBAL_DATASOURCE) DataSource dataSource,
     * @Qualifier("mybatisPlusInterceptor") MybatisPlusInterceptor mybatisPlusInterceptor) throws Exception {
     * MybatisSqlSessionFactoryBean factoryBean = new MybatisSqlSessionFactoryBean();
     * <p>
     * factoryBean.setPlugins(mybatisPlusInterceptor);
     * factoryBean.setDataSource(dataSource);
     * <p>
     * MybatisConfiguration configuration = new MybatisConfiguration();
     * configuration.setCallSettersOnNulls(true);
     * configuration.setJdbcTypeForNull(JdbcType.NULL);
     * if (showLog) {
     * configuration.setLogImpl(StdOutImpl.class);
     * } else {
     * configuration.setLogImpl(NoLoggingImpl.class);
     * }
     * factoryBean.setConfiguration(configuration);
     * <p>
     * Resource[] resources = new PathMatchingResourcePatternResolver().getResources(myBatisConfig.getMapperLocations());
     * <p>
     * factoryBean.setMapperLocations(resources);
     * <p>
     * //扩展字段设置。
     * GlobalConfig globalConfig = new GlobalConfig();
     * <p>
     * <p>
     * globalConfig.setMetaObjectHandler(new DateMetaObjectHandler());
     * <p>
     * factoryBean.setGlobalConfig(globalConfig);
     * <p>
     * //设置databaseIdProvider
     * //factoryBean.setDatabaseIdProvider(databaseIdProvider());
     * <p>
     * return factoryBean.getObject();
     * <p>
     * <p>
     * }
     */

}
