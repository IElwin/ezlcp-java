package com.ezlcp.log.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.commons.dto.SystemLog;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统操作日志数据库访问层
 */
@Mapper
public interface SystemLogMapper extends BaseDao<SystemLog> {
}