package com.ezlcp.log.mapper;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.commons.dto.LoginLog;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户登录日志数据库访问层
 */
@Mapper
public interface LoginLogMapper extends BaseDao<LoginLog> {
}