package com.ezlcp.log.controller;

import com.alibaba.fastjson2.JSON;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.base.entity.JsonResult;
import com.ezlcp.commons.base.search.QueryFilter;
import com.ezlcp.commons.dto.LoginLog;
import com.ezlcp.commons.utils.ContextUtil;
import com.ezlcp.log.service.LoginLogServiceImpl;
import com.ezlcp.log.web.BaseController;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/ezlcp/log/loginLog")
@RefreshScope
@Tag(name = "用户登录日志")
public class LoginLogController extends BaseController<LoginLog> {
    @Autowired
    LoginLogServiceImpl loginLogService;

    @Operation(summary = "记录登录日志")
    @PostMapping("insert")
    public Integer insert(@Parameter(description = "日志对象") @RequestBody LoginLog log) {
        logger.info("登录日志:" + JSON.toJSONString(log));
        return loginLogService.insert(log);
    }

    @Operation(summary = "退出登录", description = "记录登出时间")
    @GetMapping("logout")
    public boolean logout(@Parameter(description = "用户ID") @RequestParam(value = "userId") String userId, @Parameter(description = "客户端类型：0.PC 1.H5 2.安卓 4.IOS") @RequestParam(value = "clientType") Short clientType) {
        return loginLogService.logOut(userId, clientType);
    }

    @Override
    public JsonResult del(String ids) {
        return JsonResult.Fail("common.apiClose");
    }

    @Override
    public JsonResult save(LoginLog entity, BindingResult validResult) throws Exception {
        return JsonResult.Fail("common.apiClose");
    }

    @Override
    public BaseService getBaseService() {
        return loginLogService;
    }

    @Override
    public String getComment() {
        return "用户登录日志";
    }
}