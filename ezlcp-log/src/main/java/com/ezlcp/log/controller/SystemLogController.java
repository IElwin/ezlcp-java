package com.ezlcp.log.controller;

import com.alibaba.fastjson2.JSON;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.base.entity.JsonResult;
import com.ezlcp.commons.base.search.QueryFilter;
import com.ezlcp.commons.dto.SystemLog;
import com.ezlcp.commons.utils.ContextUtil;
import com.ezlcp.log.service.SystemLogServiceImpl;
import com.ezlcp.log.web.BaseController;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/ezlcp/log/systemLog")
@Tag(name = "系统操作日志")
public class SystemLogController extends BaseController<SystemLog> {
    @Autowired
    SystemLogServiceImpl systemLogService;

    @Value("${ezlcp.log.to.mysql:true}")
    private boolean log2Mysql;

    @Operation(summary = "记录系统日志")
    @PostMapping("/insert")
    public Integer insert(@Parameter(description = "日志对象") @RequestBody SystemLog log) {
        logger.info("系统日志:" + JSON.toJSONString(log));
        if (!log2Mysql) {
            return 1;
        }
        return systemLogService.insert(log);
    }

    @Override
    public BaseService getBaseService() {
        return systemLogService;
    }

    @Override
    public JsonResult del(String ids) {
        return JsonResult.Fail("common.apiClose");
    }

    @Override
    public JsonResult save(SystemLog entity, BindingResult validResult) throws Exception {
        return JsonResult.Fail("common.apiClose");
    }

    @Override
    public String getComment() {
        return "系统操作日志";
    }

}