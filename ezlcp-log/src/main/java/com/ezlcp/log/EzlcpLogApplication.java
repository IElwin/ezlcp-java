package com.ezlcp.log;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author Elwin ZHANG
 * @description: 日志模块启动 <br/>
 * @date 2022/12/30 10:06
 */
@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = {"com.ezlcp"})
public class EzlcpLogApplication {
    public static void main(String[] args) {
        SpringApplication.run(EzlcpLogApplication.class, args);
    }
}
