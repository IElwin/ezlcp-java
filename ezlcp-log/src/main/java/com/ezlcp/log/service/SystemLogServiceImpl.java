package com.ezlcp.log.service;

import com.ezlcp.commons.base.db.BaseDao;
import com.ezlcp.commons.base.db.BaseService;
import com.ezlcp.commons.dto.SystemLog;
import com.ezlcp.commons.service.impl.SuperServiceImpl;
import com.ezlcp.log.mapper.SystemLogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * [系统操作日志]业务服务类
 */
@Service
public class SystemLogServiceImpl extends SuperServiceImpl<SystemLogMapper, SystemLog> implements BaseService<SystemLog> {
    @Autowired
    private SystemLogMapper systemLogMapper;

    @Override
    public BaseDao<SystemLog> getRepository() {
        return systemLogMapper;
    }
}