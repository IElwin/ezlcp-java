/*
SQLyog Ultimate v12.08 (64 bit)
MySQL - 8.0.28 : Database - ezlcp_log
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ezlcp_log` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

/*Table structure for table `login_log` */

DROP TABLE IF EXISTS `login_log`;

CREATE TABLE `login_log` (
                             `log_id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '记录ID',
                             `login_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '登录名',
                             `login_time` datetime DEFAULT NULL COMMENT '登录时间',
                             `ip_addr` varchar(40) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'IP地址',
                             `user_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户ID',
                             `client_type` smallint DEFAULT NULL COMMENT '0.PC 1.H5 2.安卓4.IOS',
                             `is_fail` smallint DEFAULT NULL COMMENT '0成功,1失败',
                             `fail_reason` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '失败原因',
                             `logout_time` datetime DEFAULT NULL COMMENT '登出时间',
                             `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司ID',
                             `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                             `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                             `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最近修改人',
                             `update_time` datetime DEFAULT NULL COMMENT '最近修改人',
                             PRIMARY KEY (`log_id`),
                             KEY `idx_login_log_user_create_time` (`user_id`,`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='用户登录日志';

/*Table structure for table `system_log` */

DROP TABLE IF EXISTS `system_log`;

CREATE TABLE `system_log` (
                              `log_id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '记录ID',
                              `user_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户ID',
                              `user_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户名',
                              `module` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '模块名',
                              `action` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '操作',
                              `record_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '操作记录ID',
                              `detail` longtext COLLATE utf8mb4_general_ci COMMENT '操作详情(新旧记录JSON)',
                              `ref_page` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '访问页面',
                              `menu_name` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '菜单名称',
                              `ip_addr` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'IP地址',
                              `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司ID',
                              `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                              `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                              `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最近修改人',
                              `update_time` datetime DEFAULT NULL COMMENT '最近修改人',
                              `batch_no` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '批次号',
                              PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='系统操作日志';

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
