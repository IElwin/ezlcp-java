-- 2024-4-17 表单控件表增加字段
ALTER TABLE `ezlcp_form`.`f_form_control`
    CHANGE `input_source` `input_source` VARCHAR(20) CHARSET utf8mb4 COLLATE utf8mb4_general_ci NULL   COMMENT 'input/curUserId/curDate/now/calculate/auto/dict',
    ADD COLUMN `source_filter` TEXT NULL   COMMENT '输入来源过滤条件' AFTER `input_source`;

-- 2024-6-19 实体列表增加字段
ALTER TABLE `ezlcp_form`.`f_entity_col`
    ADD COLUMN `control_type` VARCHAR(20) NULL   COMMENT '组件类型' AFTER `is_pk`,
  ADD COLUMN `dict_id` VARCHAR(64) NULL   COMMENT '数据字典ID' AFTER `ref_serial_no`;

/*  2024-7-18 默认的数据字典项 */
insert into `f_module` (`module_id`, `module_name`, `hk_name`, `en_name`, `parent_id`, `level`, `remark`, `status`, `sort`, `tenant_id`, `create_by`, `create_time`, `update_by`, `update_time`, `seq`)
values('1','系统内置','係統內置','Built-in','','1','','1','1',NULL,'ez','2024-07-18 16:26:43',NULL,NULL,'1');

insert into `f_dictionary` (`dic_id`, `dic_key`, `dic_name`, `hk_name`, `en_name`, `module_id`, `sort_no`, `is_fixed`, `status`, `tenant_id`, `create_by`, `create_time`, `update_by`, `update_time`, `seq`)
values('1','status','状态','狀態','Status','1','1','1','1',NULL,'ez','2024-07-18 15:32:52',NULL,NULL,'1');

insert into `f_dic_value` (`val_id`, `dic_id`, `dic_key`, `save_val`, `show_text`, `hk_text`, `en_text`, `sort_no`, `is_fixed`, `status`, `tenant_id`, `create_by`, `create_time`, `update_by`, `update_time`, `seq`)
values('101','1','status','0','草稿','草稿','Draft','1','1','1',NULL,'ez','2024-07-18 15:32:52',NULL,NULL,'1');
insert into `f_dic_value` (`val_id`, `dic_id`, `dic_key`, `save_val`, `show_text`, `hk_text`, `en_text`, `sort_no`, `is_fixed`, `status`, `tenant_id`, `create_by`, `create_time`, `update_by`, `update_time`, `seq`)
values('102','1','status','1','已发布','已發布','Published','2','1','1',NULL,'ez','2024-07-18 15:32:52',NULL,NULL,'1');
insert into `f_dic_value` (`val_id`, `dic_id`, `dic_key`, `save_val`, `show_text`, `hk_text`, `en_text`, `sort_no`, `is_fixed`, `status`, `tenant_id`, `create_by`, `create_time`, `update_by`, `update_time`, `seq`)
values('103','1','status','4','已作废','已作廢','Canceled','3','1','1',NULL,'ez','2024-07-18 15:32:52',NULL,NULL,'1');

-- 2024-7-23 列表列增加字段
ALTER TABLE `ezlcp_form`.`f_list_col`
    ADD COLUMN `dict_id` VARCHAR(64) NULL   COMMENT '数据字典ID' AFTER `is_mobile`;
