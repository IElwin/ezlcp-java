/*
SQLyog Ultimate v12.08 (64 bit)
MySQL - 8.0.28 : Database - ezlcp_form
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
USE `ezlcp_form`;

/*Table structure for table `f_data_list` */

CREATE TABLE `f_data_list` (
                               `id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT 'ID',
                               `module_id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '所属模块',
                               `ent_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '实体ID',
                               `name` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '列表名称',
                               `hk_name` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '繁体中文名称',
                               `en_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '英文名称',
                               `list_type` smallint DEFAULT NULL COMMENT '0表格1看板2相册3主从表4树表5组合表6图表7甘特图8日历',
                               `end_type` smallint DEFAULT NULL COMMENT '1PC2移动',
                               `pc_form_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'PC表单',
                               `pc_form_read_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'PC表单-只读',
                               `mb_form_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '移动表单',
                               `mb_form_read_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '移动表单-只读',
                               `can_select_more` tinyint NOT NULL DEFAULT '1' COMMENT '允许多选',
                               `copy_from` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '来源ID',
                               `is_build_in` tinyint NOT NULL DEFAULT '0' COMMENT '系统内置',
                               `show_serial_no` tinyint DEFAULT NULL COMMENT '显示序号列',
                               `sql_join` text COLLATE utf8mb4_general_ci COMMENT '关联子句',
                               `sql_where` text COLLATE utf8mb4_general_ci COMMENT '固定查询条件',
                               `sql_oder_by` text COLLATE utf8mb4_general_ci COMMENT '排序子句',
                               `is_paging` tinyint NOT NULL DEFAULT '1' COMMENT '是否分页',
                               `page_size`  smallint NOT NULL DEFAULT '10' COMMENT '每页行数',
                               `is_lazy` tinyint NOT NULL DEFAULT '0' COMMENT '是否懒加载',
                               `line_height` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '行高',
                               `icon` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '图标',
                               `show_high_filter` tinyint NOT NULL DEFAULT '0' COMMENT '显示高级查询',
                               `show_query_plan` tinyint NOT NULL DEFAULT '0' COMMENT '显示查询方案',
                               `settings` longtext COLLATE utf8mb4_general_ci COMMENT '更多配置',
                               `status` smallint NOT NULL DEFAULT '0' COMMENT '0草稿1发布4作废',
                               `remark` text COLLATE utf8mb4_general_ci COMMENT '备注',
                                before_sql  text COLLATE utf8mb4_general_ci  COMMENT '前置SQL',
                               `sql_before_del` text COLLATE utf8mb4_general_ci COMMENT '删除前校验SQL',
                               `sql_after_del` text COLLATE utf8mb4_general_ci COMMENT '删除后执行SQL',
                               `click_event` text COLLATE utf8mb4_general_ci COMMENT '单击行事件',
                               `dbl_click_event` text COLLATE utf8mb4_general_ci COMMENT '双击行事件',
                               `ver` int DEFAULT NULL COMMENT '版本',
                               `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司ID',
                               `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                               `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                               `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最近修改人',
                               `update_time` datetime DEFAULT NULL COMMENT '最近修改人',
                               `seq` int NOT NULL DEFAULT '0' COMMENT '修改次数',
                               PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='查询列表定义';

/*Table structure for table `f_datasource` */

CREATE TABLE `f_datasource` (
                                `ds_id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '数据源ID',
                                `ds_name` varchar(30) COLLATE utf8mb4_general_ci NOT NULL COMMENT '数据源名称',
                                `identifier` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '标识符',
                                `db_type` varchar(20) COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'mysql' COMMENT '数据库类型',
                                `module_id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '所属模块',
                                `is_public` smallint NOT NULL DEFAULT '0' COMMENT '是否公开',
                                `detail` text COLLATE utf8mb4_general_ci COMMENT '定义',
                                `remark` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
                                `status` smallint NOT NULL DEFAULT '0' COMMENT '0停用1启用4删除',
                                `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司ID',
                                `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                                `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                                `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最近修改人',
                                `update_time` datetime DEFAULT NULL COMMENT '最近修改人',
                                `seq` int NOT NULL DEFAULT '0' COMMENT '修改次数',
                                PRIMARY KEY (`ds_id`),
                                UNIQUE KEY `idx_datasource_identifier` (`tenant_id`,`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='自定义的数据库连接';

/*Table structure for table `f_dic_value` */

CREATE TABLE `f_dic_value` (
                               `val_id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '记录ID',
                               `dic_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '字典ID',
                               `dic_key` varchar(50) COLLATE utf8mb4_general_ci NOT NULL COMMENT '关键字',
                               `save_val` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '保存值',
                               `show_text` varchar(50) COLLATE utf8mb4_general_ci NOT NULL COMMENT '显示文本',
                               `hk_text` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '繁体中文',
                               `en_text` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '英文',
                               `sort_no` int DEFAULT '0' COMMENT '显示顺序',
                               `is_fixed` smallint NOT NULL DEFAULT '0' COMMENT '系统内置',
                               `status` smallint DEFAULT NULL COMMENT '0停用1启用4删除',
                               `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司ID',
                               `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                               `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                               `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改人',
                               `update_time` datetime DEFAULT NULL COMMENT '修改时间',
                               `seq` int NOT NULL DEFAULT '0' COMMENT '修改次数',
                               PRIMARY KEY (`val_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='字典值';

/*Table structure for table `f_dictionary` */

CREATE TABLE `f_dictionary` (
                                `dic_id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典ID',
                                `dic_key` varchar(50) COLLATE utf8mb4_general_ci NOT NULL COMMENT '关键字',
                                `dic_name` varchar(50) COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
                                `hk_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '繁体中文名',
                                `en_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '英文名称',
                                `module_id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '所属模块',
                                `sort_no` int DEFAULT '0' COMMENT '显示顺序',
                                `is_fixed` smallint NOT NULL DEFAULT '0' COMMENT '系统内置',
                                `status` smallint DEFAULT NULL COMMENT '0停用1启用4删除',
                                `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司ID',
                                `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                                `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                                `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改人',
                                `update_time` datetime DEFAULT NULL COMMENT '修改时间',
                                `seq` int NOT NULL DEFAULT '0' COMMENT '修改次数',
                                PRIMARY KEY (`dic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='数据字典';

/*Table structure for table `f_entity` */

CREATE TABLE `f_entity` (
                            `ent_id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '实体ID',
                            `ent_name` varchar(30) COLLATE utf8mb4_general_ci NOT NULL COMMENT '实体名',
                            `hk_name` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '繁体中文名称',
                            `en_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '英文名称',
                            `module_id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '所属模块',
                            `table_name` varchar(50) COLLATE utf8mb4_general_ci NOT NULL COMMENT '物理表名',
                            `ds_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '数据源ID',
                            `remark` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
                            `status` smallint NOT NULL DEFAULT '0' COMMENT '0草稿1发布4作废',
                            `is_build_in` tinyint NOT NULL DEFAULT '0' COMMENT '1系统内置',
                            `is_gen_db` tinyint NOT NULL DEFAULT '1' COMMENT '是否生成表',
                            `is_logic_del` tinyint NOT NULL DEFAULT '0' COMMENT '仅逻辑删除',
                            `gen_log` smallint NOT NULL DEFAULT '0' COMMENT '日志生成：1新增2修改4删除',
                            `is_from_db` tinyint NOT NULL DEFAULT '0' COMMENT '从数据库反向生成',
                            `others` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '其他信息',
                            `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司ID',
                            `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                            `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                            `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最近修改人',
                            `update_time` datetime DEFAULT NULL COMMENT '最近修改人',
                            `seq` int NOT NULL DEFAULT '0' COMMENT '修改次数',
                            PRIMARY KEY (`ent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='数据库中表的定义';

/*Table structure for table `f_entity_col` */
CREATE TABLE `f_entity_col` (
                                `col_id` varchar(64) NOT NULL COMMENT '列ID',
                                `ent_id` varchar(64) NOT NULL COMMENT '所属实体',
                                `field_name` varchar(64) DEFAULT NULL COMMENT '数据库字段名',
                                `show_name` varchar(30) NOT NULL COMMENT '列名（显示）',
                                `hk_name` varchar(30) DEFAULT NULL COMMENT '繁体中文名称',
                                `en_name` varchar(50) DEFAULT NULL COMMENT '英文名称',
                                `is_build_in` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1系统内置',
                                `col_type` varchar(20) NOT NULL DEFAULT 'varchar' COMMENT '字段类型',
                                `col_length` smallint(6) NOT NULL DEFAULT '30' COMMENT '字段长度',
                                `col_prec` tinyint(4) DEFAULT NULL COMMENT '小数位长度',
                                `is_unique` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否唯一',
                                `is_required` tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否必填',
                                `col_order` smallint(6) NOT NULL DEFAULT '1' COMMENT '显示顺序',
                                `is_hide` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否隐藏',
                                `is_pk` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否主键',
                                `control_type` varchar(20) DEFAULT NULL COMMENT '组件类型',
                                `ref_ent_id` varchar(64) DEFAULT NULL COMMENT '引用实体ID',
                                `ref_serial_no` varchar(64) DEFAULT NULL COMMENT '单据编号ID',
                                `dict_id` varchar(64) DEFAULT NULL COMMENT '数据字典ID',
                                `main_col` tinyint(4) NOT NULL DEFAULT '0' COMMENT '重要列',
                                `can_search` tinyint(4) NOT NULL DEFAULT '1' COMMENT '可搜索',
                                `default_val` text COMMENT '默认值或表达式',
                                `others` text COMMENT '附加属性',
                                `remark` varchar(50) DEFAULT NULL COMMENT '备注',
                                `tenant_id` varchar(64) DEFAULT NULL COMMENT '公司ID',
                                `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                                `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
                                `update_by` varchar(64) DEFAULT NULL COMMENT '最近修改人',
                                `update_time` datetime DEFAULT NULL COMMENT '最近修改人',
                                `seq` int(11) NOT NULL DEFAULT '0' COMMENT '修改次数',
                                PRIMARY KEY (`col_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='表的列定义';

/*Table structure for table `f_entity_index` */

CREATE TABLE `f_entity_index` (
                                  `id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT 'ID',
                                  `index_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '索引名（数据库对象）',
                                  `ent_id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '所属实体',
                                  `is_unique` tinyint NOT NULL DEFAULT '0' COMMENT '是否唯一',
                                  `fields` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '一个或多个字段',
                                  `status` smallint DEFAULT NULL COMMENT '0草稿1已创建4删除',
                                  `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司ID',
                                  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                                  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                                  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最近修改人',
                                  `update_time` datetime DEFAULT NULL COMMENT '最近修改人',
                                  `seq` int NOT NULL DEFAULT '0' COMMENT '修改次数',
                                  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='物理表的索引定义';

/*Table structure for table `f_file` */

CREATE TABLE `f_file` (
                          `id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT 'ID',
                          `file_name` varchar(100) COLLATE utf8mb4_general_ci NOT NULL COMMENT '原文件名',
                          `new_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '新文件名',
                          `ext_name` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '文件扩展名',
                          `total_bytes` bigint DEFAULT NULL COMMENT '文件大小',
                          `media_type` varchar(80) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '媒体类型',
                          `path` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '保存路径',
                          `thumbnail` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '缩略图路径',
                          `status` smallint DEFAULT NULL COMMENT '状态',
                          `remark` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
                          `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司ID',
                          `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                          `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                          `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最近修改人',
                          `update_time` datetime DEFAULT NULL COMMENT '最近修改人',
                          PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='上传的文件统一保存在这张表中';

/*Table structure for table `f_form` */

CREATE TABLE `f_form` (
                          `fm_id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '表单ID',
                          `fm_name` varchar(30) COLLATE utf8mb4_general_ci NOT NULL COMMENT '表单名（标题）',
                          `hk_name` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '繁体中文名称',
                          `en_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '英文名称',
                          `module_id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '所属模块',
                          `ent_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '关联实体ID',
                          `fm_type` smallint DEFAULT NULL COMMENT '1PC2移动',
                          `icon` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '图标',
                          `width` smallint DEFAULT NULL COMMENT '页宽',
                          `height` smallint DEFAULT NULL COMMENT '高度',
                          `fm_cols` tinyint DEFAULT NULL COMMENT '每行字段数',
                          `row_gutter` smallint DEFAULT NULL COMMENT '字段间隙',
                          `font_size` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '字体大小',
                          `label_style` smallint DEFAULT NULL COMMENT '0不显示1左对齐2右对齐4换行',
                          `label_width` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '标签宽度',
                          `content` longtext COLLATE utf8mb4_general_ci COMMENT '页面内容',
                          `edit_limit_minutes` int DEFAULT NULL COMMENT '修改时限',
                          `content_modified` tinyint DEFAULT NULL COMMENT '内容手工修改过',
                          `sub_tables` tinyint NOT NULL DEFAULT '0' COMMENT '子表数量',
                          temp_content longtext comment '页面临时内容（针对已发布的表单重新发布后生效）',
                          `ver` int NOT NULL DEFAULT '0' COMMENT '版本',
                          `buttons` text COLLATE utf8mb4_general_ci COMMENT '按钮定义',
                          `copy_from` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '来源ID',
                          `is_build_in` tinyint NOT NULL DEFAULT '0' COMMENT '1系统内置',
                          `is_readonly` tinyint NOT NULL DEFAULT '0' COMMENT '是否只读',
                          `status` smallint NOT NULL DEFAULT '0' COMMENT '0草稿1发布4作废',
                          `remark` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
                          `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司ID',
                          `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                          `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                          `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最近修改人',
                          `update_time` datetime DEFAULT NULL COMMENT '最近修改时间',
                          `seq` int NOT NULL DEFAULT '0' COMMENT '修改次数',
                          PRIMARY KEY (`fm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='表单（主表）定义';

/*Table structure for table `f_form_button` */

CREATE TABLE `f_form_button` (
                                 `id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT 'ID',
                                 `fm_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '表单ID',
                                 `btn_name` varchar(30) COLLATE utf8mb4_general_ci NOT NULL COMMENT '按钮名称',
                                 `hk_name` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '繁体中文名称',
                                 `en_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '英文名称',
                                 `method_name` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '方法名称',
                                 `use_at` int DEFAULT NULL COMMENT '1只读2新增4修改8审批后',
                                 `show_cond` text COLLATE utf8mb4_general_ci COMMENT '显示条件',
                                 `bg_color` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '底色',
                                 `icon` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '图标',
                                 `title` text COLLATE utf8mb4_general_ci COMMENT '提示',
                                 `show_order` smallint DEFAULT NULL COMMENT '顺序',
                                 `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司ID',
                                 `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                                 `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                                 `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最近修改人',
                                 `update_time` datetime DEFAULT NULL COMMENT '最近修改人',
                                 `seq` int NOT NULL DEFAULT '0' COMMENT '修改次数',
                                 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='表单上的按钮';

/*Table structure for table `f_form_comment` */

CREATE TABLE `f_form_comment` (
                                  `id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT 'ID',
                                  `form_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '所属表单',
                                  `content` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '内容',
                                  `reply_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '被回复的ID',
                                  `attachments` text COLLATE utf8mb4_general_ci COMMENT '附件内容',
                                  `status` smallint DEFAULT NULL COMMENT '0停用1启用4删除',
                                  `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司ID',
                                  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                                  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                                  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最近修改人',
                                  `update_time` datetime DEFAULT NULL COMMENT '最近修改人',
                                  `seq` int NOT NULL DEFAULT '0' COMMENT '修改次数',
                                  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='表单评论';

/*Table structure for table `f_form_control` */

CREATE TABLE `f_form_control` (
                                  `control_id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '控件ID',
                                  `form_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '所属表单',
                                  `col_id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '关联实体列',
                                  `label_text` varchar(30) COLLATE utf8mb4_general_ci NOT NULL COMMENT '标签文字',
                                  `label_hk` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '繁体标签',
                                  `label_en` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '英文标签',
                                  `control_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '控件名称',
                                  `control_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '控件类型',
                                  `col_spans` tinyint DEFAULT NULL COMMENT '所占列数',
                                  `col_order` smallint NOT NULL DEFAULT '1' COMMENT '显示顺序',
                                  `sub_table` tinyint DEFAULT NULL COMMENT '是否子表',
                                  `is_required` tinyint NOT NULL DEFAULT '0' COMMENT '是否必填',
                                  `required_cond` text COLLATE utf8mb4_general_ci COMMENT '必填条件',
                                  `is_readonly` tinyint NOT NULL DEFAULT '0' COMMENT '是否只读',
                                  `readonly_cond` text COLLATE utf8mb4_general_ci COMMENT '只读条件',
                                  `is_hide` tinyint NOT NULL DEFAULT '0' COMMENT '是否隐藏',
                                  `hide_cond` text COLLATE utf8mb4_general_ci COMMENT '隐藏条件',
                                  `default_val` text COLLATE utf8mb4_general_ci COMMENT '默认值或表达式',
                                  `sql_expression` text COLLATE utf8mb4_general_ci COMMENT '计算字段（SQL表达式）',
                                  `edit_after_approve` tinyint DEFAULT NULL COMMENT '审核后可修改',
                                  `input_source` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'input/dict/expression/auto',
                                  `source_filter` text COLLATE utf8mb4_general_ci COMMENT '输入来源过滤条件',
                                  `style` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '样式',
                                  `is_export` tinyint NOT NULL DEFAULT '1' COMMENT '是否导出',
                                  `placeholder` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '占位符',
                                  `title` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '浮动提示',
                                  `ext_html` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '扩展代码',
                                  `ext_settings` longtext COLLATE utf8mb4_general_ci COMMENT '其他设置',
                                  `max_length` int DEFAULT NULL COMMENT '最大输入长度',
                                  `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司ID',
                                  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                                  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                                  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最近修改人',
                                  `update_time` datetime DEFAULT NULL COMMENT '最近修改人',
                                  PRIMARY KEY (`control_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='表单的控件';

/*Table structure for table `f_form_event` */

CREATE TABLE `f_form_event` (
                                `id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT 'ID',
                                `fm_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '表单ID',
                                `control_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '控件ID',
                                `is_sub_table` tinyint NOT NULL DEFAULT '0' COMMENT '是否子表控件',
                                `event_name` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '事件性名',
                                `call_func` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '调用方法',
                                `parameters` varchar(500) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '参数列表（,分开）',
                                `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司ID',
                                `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                                `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                                `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最近修改人',
                                `update_time` datetime DEFAULT NULL COMMENT '最近修改人',
                                `seq` int NOT NULL DEFAULT '0' COMMENT '修改次数',
                                PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='表单字段控件事件';

/*Table structure for table `f_form_validate` */

CREATE TABLE `f_form_validate` (
                                   `id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT 'ID',
                                   `form_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '所属表单',
                                   `use_at` smallint DEFAULT NULL COMMENT '场景（1保存前2加载时4审核8反审',
                                   `end_type` tinyint DEFAULT NULL COMMENT '1前端2后端',
                                   `table_name` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '表名或子表名',
                                   `pre_cond` text COLLATE utf8mb4_general_ci COMMENT '前置条件',
                                   `left_expr` text COLLATE utf8mb4_general_ci COMMENT '左表达式',
                                   `operator` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '运算符',
                                   `right_expr` text COLLATE utf8mb4_general_ci COMMENT '右表达式',
                                   `message` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '提示信息',
                                   `hk_message` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '繁体中文信息',
                                   `en_message` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '英文信息',
                                   `is_break` tinyint NOT NULL DEFAULT '1' COMMENT '失败时是否中断',
                                   `show_order` smallint DEFAULT NULL COMMENT '顺序',
                                   `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司ID',
                                   `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                                   `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                                   `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最近修改人',
                                   `update_time` datetime DEFAULT NULL COMMENT '最近修改人',
                                   `seq` int NOT NULL DEFAULT '0' COMMENT '修改次数',
                                   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='表单的自定义校验';

/*Table structure for table `f_list_button` */

CREATE TABLE `f_list_button` (
                                 `id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT 'ID',
                                 `list_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '列表',
                                 `btn_name` varchar(30) COLLATE utf8mb4_general_ci NOT NULL COMMENT '按钮名称',
                                 `hk_name` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '繁体中文名称',
                                 `en_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '英文名称',
                                 `method_name` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '方法名称',
                                 `is_build_in` tinyint DEFAULT NULL COMMENT '系统内置',
                                 `show_cond` text COLLATE utf8mb4_general_ci COMMENT '显示条件',
                                 `bg_color` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '底色',
                                 `icon` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '图标',
                                 `title` text COLLATE utf8mb4_general_ci COMMENT '提示',
                                 `show_order` smallint DEFAULT NULL COMMENT '顺序',
                                 `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司ID',
                                 `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                                 `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                                 `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最近修改人',
                                 `update_time` datetime DEFAULT NULL COMMENT '最近修改人',
                                 `seq` int NOT NULL DEFAULT '0' COMMENT '修改次数',
                                 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='数据列表上的按钮';

/*Table structure for table `f_list_col` */
CREATE TABLE `f_list_col` (
                              `id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT 'ID',
                              `list_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '列表ID',
                              `table_alias` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '表名/别名',
                              `field_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '物理字段名',
                              `field_alias` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '字段别名',
                              `header_text` varchar(30) COLLATE utf8mb4_general_ci NOT NULL COMMENT '列标题',
                              `header_hk` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '繁体列标题',
                              `header_en` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '英文列标题',
                              `parent_header_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '父列头',
                              `col_order` smallint NOT NULL DEFAULT '1' COMMENT '显示顺序',
                              `is_filter` tinyint NOT NULL DEFAULT '0' COMMENT '是否过滤',
                              `is_fk` tinyint NOT NULL DEFAULT '0' COMMENT '是否关联字段',
                              `is_fixed` tinyint NOT NULL DEFAULT '0' COMMENT '是否冻结',
                              `is_sort` tinyint NOT NULL DEFAULT '0' COMMENT '可排序',
                              `stat_func` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '统计函数',
                              `entity_col_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '实体列ID',
                              `col_type` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '列类型',
                              `col_width` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '列宽',
                              `font_size` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '字体大小',
                              `font_color` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '字体颜色',
                              `bg_color` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '背景色',
                              `is_export` tinyint NOT NULL DEFAULT '1' COMMENT '是否导出',
                              `data_format` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '数据格式',
                              `text_align` tinyint NOT NULL DEFAULT '0' COMMENT '0左，1中，2右',
                              `is_link` tinyint NOT NULL DEFAULT '0' COMMENT '显示为链接',
                              `link_url` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '链接地址',
                              `is_mobile` tinyint NOT NULL DEFAULT '1' COMMENT '是否显示在移动端',
                              `dict_id` VARCHAR(64)  COLLATE utf8mb4_general_ci DEFAULT  NULL  COMMENT '数据字典ID',
                              `others` text COLLATE utf8mb4_general_ci COMMENT '其它配置',
                              `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司ID',
                              `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                              `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                              `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最近修改人',
                              `update_time` datetime DEFAULT NULL COMMENT '最近修改人',
                              `seq` int NOT NULL DEFAULT '0' COMMENT '修改次数',
                              PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='数据列表的列定义';

/*Table structure for table `f_module` */

CREATE TABLE `f_module` (
                            `module_id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '模块ID',
                            `module_name` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '名称',
                            `hk_name` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '繁体中文名称',
                            `en_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '英文名称',
                            `parent_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '父模块ID',
                            `level` smallint DEFAULT NULL COMMENT '等级',
                            `remark` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
                            `status` smallint DEFAULT NULL COMMENT '0停用1启用4删除',
                            `sort` int DEFAULT '1' COMMENT '排序',
                            `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司ID',
                            `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                            `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                            `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最近修改人',
                            `update_time` datetime DEFAULT NULL COMMENT '最近修改人',
                            `seq` int NOT NULL DEFAULT '0' COMMENT '修改次数',
                            PRIMARY KEY (`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='对创建的实体，表单，流程进行分组';

/*Table structure for table `f_multiple_header` */

CREATE TABLE `f_multiple_header` (
                                     `id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT 'ID',
                                     `module_id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '所属模块',
                                     `relate_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '列表或子表ID',
                                     `is_sub_table` tinyint NOT NULL DEFAULT '0' COMMENT '是否子表',
                                     `head_name` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '名称',
                                     `hk_name` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '繁体中文名称',
                                     `en_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '英文名称',
                                     `parent_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '父表头',
                                     `level` smallint DEFAULT NULL COMMENT '层级',
                                     `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司ID',
                                     `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                                     `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                                     `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最近修改人',
                                     `update_time` datetime DEFAULT NULL COMMENT '最近修改人',
                                     `seq` int NOT NULL DEFAULT '0' COMMENT '修改次数',
                                     PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='列表或表单子表的多层表头配置';

/*Table structure for table `f_query_plan` */

CREATE TABLE `f_query_plan` (
                                `id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT 'ID',
                                `list_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '列表',
                                `plan_name` varchar(30) COLLATE utf8mb4_general_ci NOT NULL COMMENT '方案名称',
                                `hk_name` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '繁体中文名称',
                                `en_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '英文名称',
                                `is_default` tinyint NOT NULL COMMENT '是否默认',
                                `is_build_in` tinyint NOT NULL COMMENT '系统内置',
                                `conditions` text COLLATE utf8mb4_general_ci COMMENT '条件配置',
                                `show_order` smallint DEFAULT NULL COMMENT '顺序',
                                `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司ID',
                                `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                                `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                                `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最近修改人',
                                `update_time` datetime DEFAULT NULL COMMENT '最近修改人',
                                `seq` int NOT NULL DEFAULT '0' COMMENT '修改次数',
                                PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='数据列表的查询方案';

/*Table structure for table `f_report` */

CREATE TABLE `f_report` (
                            `id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT 'ID',
                            `report_name` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '名称',
                            `hk_name` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '繁体中文名称',
                            `en_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '英文名称',
                            `report_type` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '报表类型',
                            `content` longtext COLLATE utf8mb4_general_ci COMMENT '报表内容',
                            `form_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '关联表单（用于打印）',
                            `remark` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
                            `status` smallint DEFAULT NULL COMMENT '0停用1启用4删除',
                            `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司ID',
                            `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                            `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                            `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最近修改人',
                            `update_time` datetime DEFAULT NULL COMMENT '最近修改人',
                            `seq` int NOT NULL DEFAULT '0' COMMENT '修改次数',
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='报表或打印模板';

/*Table structure for table `f_serial_no` */

CREATE TABLE `f_serial_no` (
                               `id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
                               `identifier` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '标识符',
                               `order_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '单据名称',
                               `module_id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '所属模块',
                               `en_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '英文名称',
                               `hk_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '繁体中文名',
                               `rules` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '规则表达式',
                               `init_val` smallint DEFAULT NULL COMMENT '初始值',
                               `step` smallint DEFAULT NULL COMMENT '步长',
                               `current_val` bigint DEFAULT NULL COMMENT '当前值',
                               `no_len` smallint DEFAULT NULL COMMENT '数值长度',
                               `reset_type` smallint NOT NULL DEFAULT '0' COMMENT '0不重置1每天2每月4每年',
                               `remark` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
                               `status` smallint DEFAULT NULL COMMENT '0停用1启用4删除',
                               `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司ID',
                               `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                               `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                               `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最近修改人',
                               `update_time` datetime DEFAULT NULL COMMENT '最近修改人',
                               `seq` int NOT NULL DEFAULT '0' COMMENT '修改次数',
                               PRIMARY KEY (`id`),
                               UNIQUE KEY `idx_serial_no_identifier` (`tenant_id`,`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='单据自定义编号';

/*Table structure for table `f_sub_table_col` */

CREATE TABLE `f_sub_table_col` (
                                   `id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT 'ID',
                                   `form_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '所属表单',
                                   `sub_table_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '子表ID',
                                   `col_id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '关联实体列',
                                   `header_text` varchar(30) COLLATE utf8mb4_general_ci NOT NULL COMMENT '标签文字',
                                   `header_hk` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '繁体列标题',
                                   `header_en` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '英文列标题',
                                   `parent_header_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '父列头',
                                   `is_sum` tinyint DEFAULT NULL COMMENT '是否汇总',
                                   `control_name` varchar(50) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '30' COMMENT '控件名称',
                                   `col_order` smallint NOT NULL DEFAULT '1' COMMENT '显示顺序',
                                   `is_required` tinyint NOT NULL DEFAULT '0' COMMENT '是否必填',
                                   `required_cond` text COLLATE utf8mb4_general_ci COMMENT '必填条件',
                                   `is_readonly` tinyint NOT NULL DEFAULT '0' COMMENT '是否只读',
                                   `readonly_cond` text COLLATE utf8mb4_general_ci COMMENT '只读条件',
                                   `is_hide` tinyint NOT NULL DEFAULT '0' COMMENT '是否隐藏',
                                   `hide_cond` text COLLATE utf8mb4_general_ci COMMENT '隐藏条件',
                                   `default_val` text COLLATE utf8mb4_general_ci COMMENT '默认值或表达式',
                                   `sql_expression` text COLLATE utf8mb4_general_ci COMMENT '计算字段（SQL表达式）',
                                   `edit_after_approve` tinyint DEFAULT NULL COMMENT '审核后可修改',
                                   `input_source` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'input/curUserId/curDate/now/calculate',
                                   `ext_html` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '扩展代码',
                                   `ext_settings` longtext COLLATE utf8mb4_general_ci COMMENT '其他设置',
                                   `font_size` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '字体大小',
                                   `font_color` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '字体颜色',
                                   `bg_color` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '背景色',
                                   `is_export` tinyint NOT NULL DEFAULT '1' COMMENT '是否导出',
                                   `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司ID',
                                   `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                                   `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                                   `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最近修改人',
                                   `update_time` datetime DEFAULT NULL COMMENT '最近修改人',
                                   `seq` int NOT NULL DEFAULT '0' COMMENT '修改次数',
                                   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='表单子表列设置';

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
