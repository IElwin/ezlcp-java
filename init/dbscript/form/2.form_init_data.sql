/*默认的数据字典项 */
insert into `f_module` (`module_id`, `module_name`, `hk_name`, `en_name`, `parent_id`, `level`, `remark`, `status`, `sort`, `tenant_id`, `create_by`, `create_time`, `update_by`, `update_time`, `seq`)
values('1','系统内置','係統內置','Built-in','','1','','1','1',NULL,'ez','2024-07-18 16:26:43',NULL,NULL,'1');

insert into `f_dictionary` (`dic_id`, `dic_key`, `dic_name`, `hk_name`, `en_name`, `module_id`, `sort_no`, `is_fixed`, `status`, `tenant_id`, `create_by`, `create_time`, `update_by`, `update_time`, `seq`)
values('1','status','状态','狀態','Status','1','1','1','1',NULL,'ez','2024-07-18 15:32:52',NULL,NULL,'1');

insert into `f_dic_value` (`val_id`, `dic_id`, `dic_key`, `save_val`, `show_text`, `hk_text`, `en_text`, `sort_no`, `is_fixed`, `status`, `tenant_id`, `create_by`, `create_time`, `update_by`, `update_time`, `seq`)
values('101','1','status','0','草稿','草稿','Draft','1','1','1',NULL,'ez','2024-07-18 15:32:52',NULL,NULL,'1');
insert into `f_dic_value` (`val_id`, `dic_id`, `dic_key`, `save_val`, `show_text`, `hk_text`, `en_text`, `sort_no`, `is_fixed`, `status`, `tenant_id`, `create_by`, `create_time`, `update_by`, `update_time`, `seq`)
values('102','1','status','1','已发布','已發布','Published','2','1','1',NULL,'ez','2024-07-18 15:32:52',NULL,NULL,'1');
insert into `f_dic_value` (`val_id`, `dic_id`, `dic_key`, `save_val`, `show_text`, `hk_text`, `en_text`, `sort_no`, `is_fixed`, `status`, `tenant_id`, `create_by`, `create_time`, `update_by`, `update_time`, `seq`)
values('103','1','status','4','已作废','已作廢','Canceled','3','1','1',NULL,'ez','2024-07-18 15:32:52',NULL,NULL,'1');
