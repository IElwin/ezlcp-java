/*
SQLyog Ultimate v12.08 (64 bit)
MySQL - 8.0.28 : Database - ezlcp_user
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ezlcp_user` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `ezlcp_user`;

/*Table structure for table `s_city` */

CREATE TABLE `s_city` (
                          `city` char(4) COLLATE utf8mb4_general_ci NOT NULL,
                          `code` char(6) COLLATE utf8mb4_general_ci DEFAULT NULL,
                          `name_` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL,
                          `en_name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
                          `province` char(2) COLLATE utf8mb4_general_ci DEFAULT NULL,
                          `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                          `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                          `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最近修改人',
                          `update_time` datetime DEFAULT NULL COMMENT '最近修改人',
                          PRIMARY KEY (`city`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Table structure for table `s_company` */

CREATE TABLE `s_company` (
                             `tenant_id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '公司id',
                             `user_id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '公司管理员',
                             `en_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '英文名称',
                             `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '中文名称',
                             `license_no` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '牌照号码',
                             `address` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '中文地址',
                             `en_addr` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '英文地址',
                             `web_site` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '网址',
                             `tel_no` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司电话',
                             `email` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '电邮地址',
                             `status` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '狀態',
                             `url` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司LOGO',
                             `account_limit` int DEFAULT NULL COMMENT '账户数量限制',
                             `time_limit` date DEFAULT NULL COMMENT '使用期限',
                             `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                             `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                             `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改人',
                             `update_time` datetime DEFAULT NULL COMMENT '修改时间',
                             `seq` int DEFAULT '0' COMMENT '修改次数',
                             PRIMARY KEY (`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='即租户信息';

/*Table structure for table `s_country` */

CREATE TABLE `s_country` (
                             `id` int NOT NULL,
                             `name_` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
                             `en_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
                             `fullname` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL,
                             `alpha2` char(2) COLLATE utf8mb4_general_ci DEFAULT NULL,
                             `alpha3` char(3) COLLATE utf8mb4_general_ci DEFAULT NULL,
                             `no_` int DEFAULT NULL,
                             `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                             `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                             `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最近修改人',
                             `update_time` datetime DEFAULT NULL COMMENT '最近修改人',
                             PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Table structure for table `s_district` */

CREATE TABLE `s_district` (
                              `district` char(6) COLLATE utf8mb4_general_ci NOT NULL,
                              `name_` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL,
                              `en_name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
                              `city` char(4) COLLATE utf8mb4_general_ci DEFAULT NULL,
                              `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                              `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                              `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最近修改人',
                              `update_time` datetime DEFAULT NULL COMMENT '最近修改人',
                              PRIMARY KEY (`district`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Table structure for table `s_group` */

CREATE TABLE `s_group` (
                           `group_id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户组ID',
                           `group_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '名称',
                           `hk_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '繁体中文名称',
                           `en_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '英文名称',
                           `remark` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
                           `group_type` smallint DEFAULT NULL COMMENT '0用户组1分公司2部门4岗位8角色',
                           `status` smallint DEFAULT NULL COMMENT '0停用1启用4删除',
                           `sort` int DEFAULT '1' COMMENT '排序',
                           `parent_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '父ID',
                           `path` text COLLATE utf8mb4_general_ci COMMENT '路径',
                           `level` smallint DEFAULT NULL COMMENT '等级',
                           `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司ID',
                           `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                           `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                           `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最近修改人',
                           `update_time` datetime DEFAULT NULL COMMENT '最近修改人',
                           `seq` int NOT NULL DEFAULT '0' COMMENT '修改次数',
                           PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='角色、群组、部门、分公司等人员集合';

/*Table structure for table `s_group_menu` */

CREATE TABLE `s_group_menu` (
                                `id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '记录ID',
                                `group_id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户组ID',
                                `menu_id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单ID',
                                `has_element` smallint NOT NULL DEFAULT '0' COMMENT '是否有元素',
                                `elements` text COLLATE utf8mb4_general_ci COMMENT '元素权限,json',
                                `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司ID',
                                `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                                `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                                `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最近修改人',
                                `update_time` datetime DEFAULT NULL COMMENT '最近修改人',
                                PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='群组关联菜单及元素，多对多';

/*Table structure for table `s_ip_strategy` */

CREATE TABLE `s_ip_strategy` (
                                 `id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
                                 `ip_addr` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'IP地址',
                                 `remark` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
                                 `status` smallint DEFAULT NULL COMMENT '0未启用1启用',
                                 `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司ID',
                                 `type` smallint DEFAULT '1' COMMENT '1黑名单2白名单',
                                 `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                                 `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                                 `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改人',
                                 `update_time` datetime DEFAULT NULL COMMENT '修改时间',
                                 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='IP黑名单或白名单控制';

/*Table structure for table `s_menu` */

CREATE TABLE `s_menu` (
                          `menu_id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单ID',
                          `menu_name` varchar(30) COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单名称',
                          `hk_name` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '繁体中文名称',
                          `en_name` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '英文名称',
                          `parent_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '上级菜单id',
                          `url` varchar(50) COLLATE utf8mb4_general_ci NOT NULL COMMENT '访问路由',
                          `component` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'vue页面',
                          `params` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '参数',
                          `nav_url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '面包屑链接地址',
                          `sort` int DEFAULT '0' COMMENT '排序',
                          `icon` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '图标',
                          `page_level` smallint DEFAULT NULL COMMENT '页面等级',
                          `other` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '其他属性',
                          `hidden` smallint NOT NULL DEFAULT '0' COMMENT '纯路由,菜单不显示',
                          `status` smallint DEFAULT NULL COMMENT '1租户0平台4删除',
                          `parent_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '路径',
                          `is_direct` tinyint DEFAULT '0' COMMENT '一级菜单直接跳到的二级菜单',
                          `menu_type` smallint DEFAULT NULL COMMENT '0目录1页面2外部链接',
                          `is_custom` smallint DEFAULT NULL COMMENT '0否1是',
                          `front_type` smallint DEFAULT NULL COMMENT '1PC 2手机 3全部',
                          `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                          `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                          `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最近修改人',
                          `update_time` datetime DEFAULT NULL COMMENT '最近修改人',
                          `seq` int NOT NULL DEFAULT '0' COMMENT '修改次数',
                          PRIMARY KEY (`menu_id`),
                          UNIQUE KEY `idx_menu_url` (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='系统菜单表';

/*Table structure for table `s_page_element` */

CREATE TABLE `s_page_element` (
                                  `id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
                                  `menu_id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜單ID',
                                  `element_key` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '元素KEY',
                                  `element_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '元素名称',
                                  `en_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '英文名称',
                                  `hk_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '繁体中文名',
                                  `type` varchar(50) COLLATE utf8mb4_general_ci DEFAULT 'button' COMMENT '类型,一般为按钮',
                                  `remark` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
                                  `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司ID',
                                  `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                                  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                                  `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最近修改人',
                                  `update_time` datetime DEFAULT NULL COMMENT '最近修改人',
                                  `seq` int NOT NULL DEFAULT '0' COMMENT '修改次数',
                                  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='頁面功能表';

/*Table structure for table `s_province` */

CREATE TABLE `s_province` (
                              `code` char(6) COLLATE utf8mb4_general_ci NOT NULL,
                              `name_` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL,
                              `en_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL,
                              `province` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
                              `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                              `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                              `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最近修改人',
                              `update_time` datetime DEFAULT NULL COMMENT '最近修改人',
                              PRIMARY KEY (`province`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Table structure for table `s_settings` */

CREATE TABLE `s_settings` (
                              `id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '设置ID',
                              `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司id',
                              `default_lang` varchar(10) COLLATE utf8mb4_general_ci DEFAULT 'zh_CN' COMMENT '默认语言',
                              `default_theme` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '默认主题',
                              `pw_length` smallint DEFAULT NULL COMMENT '用户密码长度',
                              `pw_strength` smallint DEFAULT NULL COMMENT '字母数字特殊符号有几种',
                              `login_fail_times` smallint DEFAULT '5' COMMENT '登录失败几次锁定用户',
                              `lock_minutes` smallint DEFAULT '60' COMMENT '锁定时长（分钟）',
                              `google_auth` smallint DEFAULT '0' COMMENT '启用谷歌身份验证器',
                              `idle_minutes` smallint DEFAULT NULL COMMENT '空闲几分钟自动登出',
                              `ip_strategy` smallint DEFAULT '0' COMMENT '1黑名单2白名单0未配置',
                              `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                              `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                              `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改人',
                              `update_time` datetime DEFAULT NULL COMMENT '修改时间',
                              `seq` int DEFAULT '0' COMMENT '修改次数',
                              PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='租户配置';

/*Table structure for table `s_tenant_menu` */

CREATE TABLE `s_tenant_menu` (
                                 `id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '记录ID',
                                 `tenant_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司ID',
                                 `menu_id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单ID',
                                 `menu_name` varchar(30) COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单名称',
                                 `hk_name` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '繁体中文名称',
                                 `en_name` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '英文名称',
                                 `parent_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '上级菜单id',
                                 `url` varchar(50) COLLATE utf8mb4_general_ci NOT NULL COMMENT '访问路由',
                                 `component` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'vue页面',
                                 `params` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '参数',
                                 `nav_url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '面包屑链接地址',
                                 `sort` int DEFAULT '0' COMMENT '排序',
                                 `icon` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '图标',
                                 `page_level` smallint DEFAULT NULL COMMENT '页面等级',
                                 `other` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '其他属性',
                                 `hidden` smallint NOT NULL DEFAULT '0' COMMENT '纯路由,菜单不显示',
                                 `status` smallint DEFAULT NULL COMMENT '1租户0平台4删除',
                                 `parent_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '路径',
                                 `is_direct` tinyint DEFAULT '0' COMMENT '一级菜单直接跳到的二级菜单',
                                 `menu_type` smallint DEFAULT NULL COMMENT '0目录1页面2外部链接',
                                 `is_custom` smallint DEFAULT NULL COMMENT '0否1是',
                                 `front_type` smallint DEFAULT NULL COMMENT '1PC 2手机 3全部',
                                 `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                                 `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                                 `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最近修改人',
                                 `update_time` datetime DEFAULT NULL COMMENT '最近修改人',
                                 `seq` int NOT NULL DEFAULT '0' COMMENT '修改次数',
                                 PRIMARY KEY (`id`),
                                 UNIQUE KEY `idx_tenant_menu_url` (`tenant_id`,`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='公司关联菜单';

/*Table structure for table `s_user` */

CREATE TABLE `s_user` (
                          `user_id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户ID',
                          `user_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '账户名称',
                          `password` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '登录密码',
                          `user_type` smallint DEFAULT NULL COMMENT '1管理员0普通用户',
                          `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '所属公司ID',
                          `status` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '状态',
                          `entry_date` date DEFAULT NULL COMMENT '入职时间',
                          `user_code` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户编号',
                          `name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '姓名',
                          `en_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '英文名',
                          `gender` tinyint DEFAULT NULL COMMENT '性别',
                          `passport_no` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '身份证号码',
                          `mobile_no` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '手机号码',
                          `email` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '电邮地址',
                          `address` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '住宅地址',
                          `remark` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
                          `language` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '预设界面语言',
                          `login_fail_times` smallint DEFAULT '0' COMMENT '登录失败次数',
                          `is_lock` smallint DEFAULT '0' COMMENT '是否锁定',
                          `lock_time` datetime DEFAULT NULL COMMENT '锁定开始时间',
                          `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                          `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                          `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '修改人',
                          `update_time` datetime DEFAULT NULL COMMENT '修改时间',
                          `secret_key` char(16) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'Google验证密钥',
                          `is_bind` smallint DEFAULT NULL COMMENT '是否已绑定Google验证',
                          `seq` int DEFAULT '0' COMMENT '修改次数',
                          PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='用户表';

/*Table structure for table `s_user_group` */

CREATE TABLE `s_user_group` (
                                `id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '记录ID',
                                `user_id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户ID',
                                `group_id` varchar(64) COLLATE utf8mb4_general_ci NOT NULL COMMENT '群组ID',
                                `tenant_id` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公司ID',
                                `create_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
                                `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                                `update_by` varchar(64) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '最近修改人',
                                `update_time` datetime DEFAULT NULL COMMENT '最近修改人',
                                PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='用户与角色关联表，多对多';


/*!50106 set global event_scheduler = 1*/;

/* Event structure for event `event_daily` */

DELIMITER $$

/*!50106 CREATE  EVENT `event_daily` ON SCHEDULE EVERY 1 DAY STARTS '2023-01-01 00:00:00' ON COMPLETION NOT PRESERVE ENABLE COMMENT '每天0点的定时任务' DO BEGIN
		# 重置单据编号
		CALL up_reset_serial_no();

	END */$$
DELIMITER ;

/* Procedure structure for procedure `up_reset_serial_no` */

DELIMITER $$

/*!50003 CREATE PROCEDURE `up_reset_serial_no`()
    COMMENT '重置单据编号'
BEGIN
	DECLARE _month INT; #当前月份
	DECLARE _day INT;
	SET _month=MONTH(NOW());
	SET _day=DAY(NOW());
	#reset_type：0不重置1每天2每月4每年
	#每天重置的编号
	UPDATE s_serial_no SET  current_val=IFNULL(init_val,1)-IFNULL(step,1),seq=seq +1,update_by='1',update_time=NOW()
		WHERE reset_type=1;
	#每月重置的编号
	IF _day = 1 THEN
		UPDATE s_serial_no SET  current_val=IFNULL(init_val,1)-IFNULL(step,1),seq=seq +1,update_by='1',update_time=NOW()
			WHERE reset_type=2;
	END IF;
	#每年重置的编号
	IF _day = 1 AND _month=1 THEN
		UPDATE s_serial_no SET  current_val=IFNULL(init_val,1)-IFNULL(step,1),seq=seq +1,update_by='1',update_time=NOW()
			WHERE reset_type=4;
	END IF;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `up_update_tenant_menus` */

DELIMITER $$

/*!50003 CREATE   PROCEDURE `up_update_tenant_menus`()
    COMMENT '当系统菜单变化时，更新各公司的菜单'
BEGIN
    DECLARE cnt_ INT;  #总数量
    DECLARE i_ INT;		# 计数器
    DECLARE id_ VARCHAR(64); #当前公司ID
START TRANSACTION;
    # 创建临时表，用于循环
    DROP TABLE IF EXISTS tmp_tenant;
    CREATE TEMPORARY TABLE tmp_tenant(
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	tenant_id VARCHAR(64) NOT NULL,
	PRIMARY KEY (id)
    );
    # 先清空旧的记录
    delete from s_tenant_menu where menu_id in(select menu_id from s_menu);
    #临时表插入公司ID
    INSERT INTO tmp_tenant(tenant_id)
	SELECT tenant_id FROM s_company;
    # 记录数
    SET i_=1;
    SELECT COUNT(1) INTO cnt_ FROM tmp_tenant;
    # 循环处理临时表中每个公司的菜单
    WHILE i_ <= cnt_ DO
	SELECT tenant_id INTO id_ FROM tmp_tenant WHERE id = i_;
	INSERT INTO s_tenant_menu (id,tenant_id,menu_id,menu_name,hk_name,en_name,status,icon,create_by,create_time,update_time,seq,parent_id,url,component,params,
		nav_url,sort,page_level,other,hidden,parent_path,is_direct,menu_type,is_custom,front_type)
	    SELECT REPLACE(UUID(),'-',''),id_,menu_id,menu_name,hk_name,en_name,1,icon,'1',NOW(),NOW(),1,parent_id,url,component,params,
		nav_url,sort,page_level,other,hidden,parent_path,is_direct,menu_type,is_custom,front_type
	    FROM s_menu WHERE status=1;
	SET i_ = i_ + 1;
    END WHILE;
    # 插入平台的菜单（即租户ID为Null）
    INSERT INTO s_tenant_menu (id,tenant_id,menu_id,menu_name,hk_name,en_name,STATUS,icon,create_by,create_time,update_time,seq,parent_id,url,component,params,
		nav_url,sort,page_level,other,hidden,parent_path,is_direct,menu_type,is_custom,front_type)
	    SELECT REPLACE(UUID(),'-',''),null,menu_id,menu_name,hk_name,en_name,STATUS,icon,'1',NOW(),NOW(),1,parent_id,url,component,params,
		nav_url,sort,page_level,other,hidden,parent_path,is_direct,menu_type,is_custom,front_type
	    FROM s_menu WHERE STATUS<4;

    # 删除临时表
    DROP TABLE tmp_tenant;

    # 更新父节点值为当前表的ID
    UPDATE s_tenant_menu tm JOIN s_tenant_menu p ON (tm.parent_id=p.menu_id AND IFNULL(tm.tenant_id,0)=IFNULL(p.tenant_id,0))
	SET tm.parent_id=p.id;
COMMIT;
END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
