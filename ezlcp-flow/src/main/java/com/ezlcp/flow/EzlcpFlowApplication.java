package com.ezlcp.flow;

import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author Elwin ZHANG
 * @description: 流程模块启动类 <br/>
 * @date 2022/12/26 11:17
 */
@EnableDiscoveryClient
@EnableScheduling
@SpringBootApplication(scanBasePackages = {"com.ezlcp"})
@EnableFeignClients(basePackages = {"com.ezlcp"})
@EnableProcessApplication
public class EzlcpFlowApplication {
    public static void main(String[] args) {
        SpringApplication.run(EzlcpFlowApplication.class, args);
    }
}
