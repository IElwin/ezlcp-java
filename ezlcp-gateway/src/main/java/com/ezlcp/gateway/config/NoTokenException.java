package com.ezlcp.gateway.config;

/**
 * @author Elwin ZHANG
 * @description: 请求（头）无token参数异常<br/>
 * @date 2023/3/3 16:16
 */
public class NoTokenException extends RuntimeException {
    public NoTokenException(String message){
        super(message);
    }
}
