package com.ezlcp.gateway;

import com.ezlcp.gateway.config.MyErrorAttributes;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.HttpMessageConverter;

import java.util.stream.Collectors;

/**
 * @author Elwin ZHANG
 * @description: 网关启动程序<br />
 * @date 2022/12/26 10:35
 */
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"com.ezlcp.gateway"})
@SpringBootApplication
public class EzlcpGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(EzlcpGatewayApplication.class, args);
    }


    @Bean
    @ConditionalOnMissingBean
    public HttpMessageConverters messageConverters(ObjectProvider<HttpMessageConverter<?>> converters) {
        return new HttpMessageConverters(converters.orderedStream().collect(Collectors.toList()));
    }

    /***
    * @description 自定义错误返回信息
    */
    @Bean
   public MyErrorAttributes errorAttributes() {
       return new MyErrorAttributes();
   }

}

